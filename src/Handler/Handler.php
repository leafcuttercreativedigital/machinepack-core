<?php
namespace MachinePack\Core\Handler;

use MachinePack\Core\Event\Event;
use MachinePack\Core\Event\Broker;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Result\Collection as ResultCollection;

abstract class Handler
{
    protected $broker;
    protected $settings;
    protected $config;
    public function __construct(Broker $broker, array $settings)
    {
        $this->broker   = $broker;
        $this->settings = $settings;
        if (!empty($settings['env']) && !empty($settings[$settings['env']])) {
            $this->config = $this->settings[$this->settings['env']];
        }
    }

    /**
     * The most basic event handle method.
     *
     * @param Event $event
     * @return Result
     */
    abstract public function handleEvent(Event $event): Result;

    /**
     * Forward an event, to prevent nesting in single events.
     *
     * @param Event $event
     * @return Result|ResultCollection
     */
    protected function forward(string $path, array $data)
    {
        $output = $this->broker->send($path, $data);

        if (count($output) == 1 && $output[0] instanceof Result) {
            return $output[0];
        }

        return new ResultCollection($output);
    }
}
