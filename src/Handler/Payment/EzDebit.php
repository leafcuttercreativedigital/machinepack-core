<?php
namespace MachinePack\Core\Handler\Payment;

use MachinePack\Core\MachinePack;
use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Event\Events\Subscription;
use SoapClient;
use SoapHeader;

class EzDebit extends Handler
{

    const PCI_API_URL         = 'https://api.ezidebit.com.au/v3-5/pci?singleWsdl';
    const PCI_API_URL_SANDBOX = 'https://api.demo.ezidebit.com.au/v3-5/pci?singleWsdl';

    const NON_PCI_API_URL         = 'https://api.ezidebit.com.au/v3-5/nonpci?singleWsdl';
    const NON_PCI_API_URL_SANDBOX = 'https://api.demo.ezidebit.com.au/v3-5/nonpci?singleWsdl';

    private $_config;

    public function handleEvent(\MachinePack\Core\Event\Event $event): Result
    {
        if (! $event instanceof Payment) {
            return new Ignored;
        }

        //load config from settings
        $this->_config = $this->settings[$this->settings['env']];

        //validate configuration variables
        if (empty($this->_config['digitalKey']) && empty($event['Intangible/EzDebit.digitalKey'])
        ) {
            return new Failure(
                'Please add handler settings for EzDebit. Full config should be:
                    {
                        config: {
                            env: <someenv>,
                            <someenv>: {
                                apiUrl: "...",
                                digitalKey: "...",
                            }
                        }
                    }
                '
            );
        }


        if (isset($event['Intangible/EzDebit.action'])
            && $event['Intangible/EzDebit.action']  == 'get_customer_details'
        ) {
            return $this->_returnCustomerInformation($event);
        } else {
            return $this->_processPayment($event);
        }
    }

    /**
     * Handle a recurring payment request
     * @param  Payment $event    payment information
     * @return Success|Failure
     */
    /*  private function _processSubscription($event)
    {
        try {
            return new Success(
                [
                    'MoneyTransfer.identifier'=> '',
                    'Person.identifier'=> ''
                ]
            );
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }
    } */


    /**
     * Handle returning customer information
     * @param  Payment $event    payment information
     * @return Success|Failure
     */
    private function _returnCustomerInformation($event)
    {
        try {
            $account = $this->_getAccountDetails($event);
            if (!empty($account) && !$account->GetCustomerAccountDetailsResult->Error) {
                return new Success(
                    [
                        "Intangible/EzDebit.AccountHolderName" =>
                            $account->GetCustomerAccountDetailsResult->Data->AccountHolderName,
                        "Intangible/EzDebit.AccountNumber" =>
                            $account->GetCustomerAccountDetailsResult->Data->AccountNumber,
                        "Intangible/EzDebit.BSB" => $account->GetCustomerAccountDetailsResult->Data->BSB,
                        "Intangible/EzDebit.CardHolderName" =>
                            $account->GetCustomerAccountDetailsResult->Data->CardHolderName,
                        "Intangible/EzDebit.CreditCardNumber" =>
                            $account->GetCustomerAccountDetailsResult->Data->CreditCardNumber,
                        "Intangible/EzDebit.ExpiryMonth" =>
                            $account->GetCustomerAccountDetailsResult->Data->ExpiryMonth,
                        "Intangible/EzDebit.ExpiryYear" =>
                            $account->GetCustomerAccountDetailsResult->Data->ExpiryYear,
                        "Intangible/EzDebit.PaymentMethod" =>
                            $account->GetCustomerAccountDetailsResult->Data->PaymentMethod,
                    ]
                );
            } else {
                return new Failure(
                    $account->GetCustomerAccountDetailsResult->Error . '-'
                    . $account->GetCustomerAccountDetailsResult->ErrorMessage,
                );
            }
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }
    }

    /**
     * Handle a single payment request
     * @param  Payment $event    payment information
     * @return Success|Failure
     */

    private function _processPayment(Payment $event)
    {
        try {
            $token = $event['Intangible/EzDebit.Customer.id'] ?? "";
            if (!$token) {
                //Check if the customer exists
                $customer = $this->_getCustomerDetails($event);
                if ($customer->GetCustomerDetailsResult->Error != 0) {
                    //If the customer does not exist add the customer
                    $customer = $this->_addCustomer($event);
                    if ($customer->AddCustomerResult->Error != 0) {
                        return new Failure(
                            $customer->AddCustomerResult->Error . '' . $customer->AddCustomerResult->ErrorMessage
                        );
                    }
                    //Get the customer reference for future payments
                    $token = $customer->AddCustomerResult->Data->CustomerRef;
                } else {
                    $token = $customer->GetCustomerDetailsResult->Data->EzidebitCustomerID;
                }
            }

            //Add/update the card to the customer account
            $card_success = false;
            if (isset($event['Intangible/EzDebit.updateCreditCard'])
                && ($event['Intangible/EzDebit.updateCreditCard'] == true)
            ) {
                $card_success = $this->_editCustomerCreditCard($event, $token);
            }
            //Process the payment to the customers account using the token
            $payment = $this->_processRealtimeTokenPayment($event, $token);
            if ($payment->ProcessRealtimeTokenPaymentResult->Error != 0) {
                return new Failure(
                    // phpcs:ignore
                    $payment->ProcessRealtimeTokenPaymentResult->Error . '' . $payment->ProcessRealtimeTokenPaymentResult->ErrorMessage,
                    [
                        'Customer.identifier' => $token,
                        'MoneyTransfer.identifier' => $payment->ProcessRealtimeTokenPaymentResult->Data->BankReceiptID,// phpcs:ignore
                        'Intangible/EzDebit.ExchangePaymentID' => $payment->ProcessRealtimeTokenPaymentResult->Data->ExchangePaymentID,// phpcs:ignore
                        'Intangible/EzDebit.PaymentResult' => $payment->ProcessRealtimeTokenPaymentResult->Data->PaymentResult,// phpcs:ignore
                        'Intangible/EzDebit.PaymentResultCode' => $payment->ProcessRealtimeTokenPaymentResult->Data->PaymentResultCode,// phpcs:ignore
                        'Intangible/EzDebit.PaymentResultText' => $payment->ProcessRealtimeTokenPaymentResult->Data->PaymentResultText,// phpcs:ignore
                    ]
                );
            }

            return new Success(
                [
                    'Customer.identifier' => $token,
                    'MoneyTransfer.identifier' => $payment->ProcessRealtimeTokenPaymentResult->Data->BankReceiptID,// phpcs:ignore
                    'Intangible/EzDebit.ExchangePaymentID' => $payment->ProcessRealtimeTokenPaymentResult->Data->ExchangePaymentID,// phpcs:ignore
                    'Intangible/EzDebit.PaymentResult' => $payment->ProcessRealtimeTokenPaymentResult->Data->PaymentResult,// phpcs:ignore
                    'Intangible/EzDebit.PaymentResultCode' => $payment->ProcessRealtimeTokenPaymentResult->Data->PaymentResultCode,// phpcs:ignore
                    'Intangible/EzDebit.PaymentResultText' => $payment->ProcessRealtimeTokenPaymentResult->Data->PaymentResultText,// phpcs:ignore
                ]
            );
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }
    }

    /**
     * This method retrieves details about the given Customer.
     *
     * @param array $data
     */
    private function _getAccountDetails($event)
    {
        try {
            $api_url    = $this->_getPCIAPIURL($event);
            $soapclient = new SoapClient($api_url);

            $params = [
               'DigitalKey' => $this->_config['digitalKey'] ?? $event['Intangible/EzDebit.digitalKey'],
               'EziDebitCustomerID' => $event['Intangible/EzDebit.Customer.id'] ?? "",
               'YourSystemReference' => $event['Intangible/EzDebit.systemReference'] ?? "",
            ];
            return $soapclient->GetCustomerAccountDetails($params);
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }
    }

    /**
     * This method retrieves details about the given Customer.
     *
     * @param array $data
     */
    private function _getCustomerDetails($event)
    {
        try {
            $api_url    = $this->_getNonPCIAPIURL($event);
            $soapclient = new SoapClient($api_url);

            $params = [
               'DigitalKey' => $this->_config['digitalKey'] ?? $event['Intangible/EzDebit.digitalKey'],
               'YourSystemReference' => $event['Intangible/EzDebit.systemReference'] ?? "",
            ];

            return $soapclient->getCustomerDetails($params);
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }
    }

    /**
	 * This method creates a new customer record in the Ezidebit
	 * database from which you will be able to debit payments.
	 *
	 * @param array $event
     * @return addCustomer
	 */
	private function _addCustomer($event)
    {
        try {
            $api_url    = $this->_getNonPCIAPIURL($event);
            $soapclient = new SoapClient($api_url);

            $params = [
                'DigitalKey' => $this->_config['digitalKey'] ?? $event['Intangible/EzDebit.digitalKey'],
                'PaymentReference' => $event['Product.productId'] ?? "",

                'YourSystemReference' => $event['Intangible/EzDebit.systemReference'] ?? "",
                'YourGeneralReference' => $event['Intangible/EzDebit.generalReference'] ?? "",


                'LastName' =>  $event['Person.familyName'] ?? "",
                'FirstName' => $event['Person.givenName'] ?? "",
                'EmailAddress' => $event['Person.email'] ?? "",
                'MobilePhoneNumber' => $event['Person.telephone'] ?? "",
                'AddressLine1' => $event['PostalAddress.streetAddress'] ?? "",
                'AddressLine2' => $event['PostalAddress.streetAddress2'] ?? "",
                'AddressSuburb' => $event['PostalAddress.addressLocality'] ?? "",
                'AddressState' => $event['PostalAddress.addressRegion'] ?? "",
                'AddressPostCode' => $event['PostalAddress.postalCode'] ?? "",

                'ContractStartDate' => $event['startDate'] ?? date('Y-m-d'),
                'SmsPaymentReminder' => $event['Intangible/EzDebit.smsReminder'] ?? "YES",
                'SmsFailedNotification' => $event['Intangible/EzDebit.smsFailed'] ?? "YES",
                'SmsExpiredCard' => $event['Intangible/EzDebit.smsExpired'] ?? "YES",
            ];

            return $soapclient->AddCustomer($params);
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }
	}

    /**
	 * This method will Add or Edit the Credit Card detail on record for a Customer (Payer).
	 *
	 * @param array $event
     * @return editCustomerCreditCard
	 */
	private function _editCustomerCreditCard($event, $token)
    {
		try {
            $api_url    = $this->_getPCIAPIURL($event);
            $soapclient = new SoapClient($api_url);

            $params = [
                'DigitalKey' => $this->_config['digitalKey'] ?? $event['Intangible/EzDebit.digitalKey'],
				'EziDebitCustomerID' => $token,
                'CreditCardNumber' => $event['CreditCard/CardDetails.number'] ?? "",
                'CreditCardExpiryMonth' => $event['CreditCard/CardDetails.expiryMonth'] ?? "",
                'CreditCardExpiryYear' => $event['CreditCard/CardDetails.expiryYear'] ?? "",
                'NameOnCreditCard' => $event['CreditCard/CardDetails.name'] ?? "",
				'Reactivate' => 'YES',
            ];

            return $soapclient->editCustomerCreditCard($params);
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }
	}

    /**
	 * This method allows you to process a credit card payment in real time using a
	 * customer’s credit card number previously stored by Ezidebit. This requires a
	 * token to be passed in to identify the correct customer credit card.
	 *
	 * @param array $event
     * @return processRealtimeTokenPayment
	 */
	private function _processRealtimeTokenPayment($event, $token)
    {
		try {
            $api_url    = $this->_getPCIAPIURL($event);
            $soapclient = new SoapClient($api_url);

            $params = [
                'digitalKey' => $this->_config['digitalKey'] ?? $event['Intangible/EzDebit.digitalKey'],
				'token' => $token,
                'paymentAmountInCents' => floatval($event['MonetaryAmount.value'] * 100),
				'customerName' => $event['Person.givenName'] . ' ' . $event['Person.familyName'] ?? "",
				'paymentReference' => $event['Product.productId'] ?? ""
            ];

            return $soapclient->processRealtimeTokenPayment($params);
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }
	}

    /**
	 * This method is designed to accept requests that include both
	 * Customer (Payer) and Payment details in a single call. It will
	 * either create, update or maintain a Customer and schedule the
	 * payment to be debited from the account. A system utilising this
	 * method will provide a unique Payer Reference from the integrating
	 * system as well as a unique Payment Reference for each payment.
	 *
	 * @param array $event
     * @return Success|Failure
	 */
    private function _addCard(Payment $event)
    {
        try {
            $api_url    = $this->_getPCIAPIURL($event);
            $soapclient = new SoapClient($api_url);

            $params = [
                'DigitalKey' => $this->_config['digitalKey'] ?? $event['Intangible/EzDebit.digitalKey'],
                'PaymentReference' => $event['Product.productId'] ?? "",

				'YourSystemReference' => $event['Intangible/EzDebit.systemReference'] ?? "",
				'YourGeneralReference' => $event['Intangible/EzDebit.generalReference'] ?? "",

				'LastName' =>  $event['Person.familyName'] ?? "",
				'FirstName' => $event['Person.givenName'] ?? "",
				'EmailAddress' => $event['Person.email'] ?? "",
				'MobilePhoneNumber' => $event['Person.telephone'] ?? "",
                
				'SmsPaymentReminder' => $event['Intangible/EzDebit.smsReminder'] ?? "YES",
				'SmsFailedNotification' => $event['Intangible/EzDebit.smsFailed'] ?? "YES",
				'SmsExpiredCard' => $event['Intangible/EzDebit.smsExpired'] ?? "YES",

                'PaymentAmountInCents' => $event['MonetaryAmount.value'] * 100,
				'DebitDate' => $event['Intangible/EzDebit.debitDate'] ?? date('Y-m-d'),

                'CreditCardNumber' => $event['CreditCard/CardDetails.number'] ?? "",
                'CreditCardExpiryMonth' => $event['CreditCard/CardDetails.expiryMonth'] ?? "",
                'CreditCardExpiryYear' => $event['CreditCard/CardDetails.expiryYear'] ?? "",
                'CreditCardCCV' => $event['CreditCard/CardDetails.cvn'] ?? "",
                'NameOnCreditCard' => $event['CreditCard/CardDetails.name'] ?? "",
		    ];

            $response = $soapclient->addCardDebit($params);
            if (isset($response->AddCardDebitResult) && $response->AddCardDebitResult->Error != 0) {
                return new Failure(
                    $response->AddCardDebitResult->Error . ' ' .$response->AddCardDebitResult->ErrorMessage
                );
            } else {
                return new Success(
                    [
                        'MoneyTransfer.identifier' => $response->AddCardDebitResult->Data->CustomerRef,
                    ]
                );
            }
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }
    }

    private function _getPCIAPIURL($event)
    {
        if ((isset($this->_config['isProductionMode']) && $this->_config['isProductionMode'])
            || (isset($event['Intangible/EzDebit.isProductionMode']) && $event['Intangible/EzDebit.isProductionMode'])
        ) {
            return self::PCI_API_URL;
        } else {
            return self::PCI_API_URL_SANDBOX;
        }
    }

    private function _getNonPCIAPIURL($event)
    {
        if ((isset($this->_config['isProductionMode']) && $this->_config['isProductionMode'])
            || (isset($event['Intangible/EzDebit.isProductionMode']) && $event['Intangible/EzDebit.isProductionMode'])
        ) {
            return self::NON_PCI_API_URL;
        } else {
            return self::NON_PCI_API_URL_SANDBOX;
        }
    }
}
