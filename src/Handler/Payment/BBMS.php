<?php
namespace MachinePack\Core\Handler\Payment;

use MachinePack\Core\MachinePack;
use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Event\Events\Subscription;

class BBMS extends Handler
{
    private $_config;

    public function handleEvent(\MachinePack\Core\Event\Event $event): Result
    {
        if (! $event instanceof Payment) {
            return new Ignored;
        }

        //Api credentials can be either passed as args or loaded from config
        //validate configuration variables
        try {
            $this->_fetchValidateConfig($event);
        } catch (\Exception $e) {
            return new Failure($e->getMessage());
        }

        if (isset($event['Intangible/Sky.action'])) {
            switch ($event['Intangible/Sky.action']) {
                case 'get_access_token':
                    return $this->_exchangeCodeForAccessToken($event);
                case 'get_auth_url':
                    return $this->_createAuthURL();
                case 'get_checkout_public_key':
                    return $this->_getCheckoutPublicKey();
                case 'get_payment_configurations':
                    return $this->_getPaymentConfigurationList($event);
                case 'process_payment':
                    return $this->_processPayment($event);
                case 'process_checkout_transaction':
                    return $this->_processCheckoutTransaction($event);
                case 'refresh_access_token':
                    return $this->_getRefreshToken($event);
            }
        }

        return new Failure('Invalid event action provided.');
    }

    private function _fetchValidateConfig($event)
    {
        switch ($event['Intangible/Sky.action']) {
            case 'get_access_token':
                $errors = $this->_fetchValidateEventConfig(
                    $event,
                    ['client_id', 'client_secret', 'application_code', 'redirect_uri']
                );
                break;
            case 'get_auth_url':
                $errors = $this->_fetchValidateEventConfig($event, ['client_id', 'redirect_uri']);
                break;
            case 'get_checkout_public_key':
                $errors = $this->_fetchValidateEventConfig($event, ['access_token', 'subscription_key']);
                break;
            case 'refresh_access_token':
                $errors = $this->_fetchValidateEventConfig($event, ['client_id', 'client_secret', 'refresh_token']);
                break;
            case 'process_payment':
                $errors = $this->_fetchValidateEventConfig(
                    $event,
                    ['access_token', 'refresh_token', 'client_id', 'client_secret','subscription_key', 'payment_config']
                );
                break;
            case 'get_payment_configurations':
            case 'process_checkout_transaction':
            default:
                $errors = $this->_fetchValidateEventConfig(
                    $event,
                    ['access_token', 'refresh_token', 'client_id', 'client_secret' ,'subscription_key']
                );
                break;
        }

        if (!empty($errors)) {
            $this->_throwConfigurationException($errors);
        }
    }

    private function _fetchValidateEventConfig($event, $required_fields = [])
    {
        $errors = [];

        foreach ($required_fields as $field) {
            if (!isset($event['Intangible/Sky.' . $field])) {
                if (!isset($this->settings[$this->settings['env']][$field])) {
                    $errors[] = $field;
                } else {
                    $this->_config[$field] = $this->settings[$this->settings['env']][$field];
                }
            } else {
                $this->_config[$field] = $event['Intangible/Sky.' . $field];
            }
        }

        return $errors;
    }

    private function _throwConfigurationException($errors = [])
    {
        throw new \Exception(
            'Please provide the following configuration fields within your configuration file or the event payload: ' .
            implode(', ', $errors)
        );
    }

    private function _processPayment($event)
    {
        try {
            $auth_key = $this->_config['access_token'];
            $payment_configurations_response = $this->_getPaymentConfigurations($event);

            if (!($payment_configurations_response instanceof Failure)) {
                $payment_config_id = $this->_getPaymentConfigIdByName($payment_configurations_response);
                if (!empty($payment_configurations_response['Intangible/Sky.access_token'])) {
                    $auth_key = $payment_configurations_response['Intangible/Sky.access_token'];
                }
                if (!empty($payment_configurations_response['Intangible/Sky.refresh_token'])) {
                    $refresh_token = $payment_configurations_response['Intangible/Sky.refresh_token'];
                }
            } else {
                return new Failure(
                    $payment_configurations_response->reason,
                    $payment_configurations_response->data
                );
            }

            if ($payment_config_id) {
                // Create Payment Data
                if ($event['Payment.type'] == 'recurring') {
                    $cardToken = $this->_createCardToken($event, $auth_key);
                    if (!$cardToken instanceof Failure) {
                        $data = $this->_createRecurringPaymentData($event, $payment_config_id, $cardToken);
                    } else {
                        return new Failure($cardToken->reason, $cardToken->data);
                    }
                } else {
                    $data = $this->_createOneOffPaymentData($event, $payment_config_id);
                }

                $headers = [
                    "Content-Type: application/json",
                    "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
                    "Authorization: Bearer " . $auth_key
                ];

                $method = 'POST';
                $url    = 'https://api.sky.blackbaud.com/payments/v1/transactions';

                $payment_response = json_decode(
                    $this->_skyApiCall(
                        $url,
                        $method,
                        $headers,
                        $data
                    )
                );

                if (!empty($payment_response->id)) {
                    $data = [
                        'Payment.identifier'           => $payment_response->id,
                        'Payment.state'                => $payment_response->state,
                        'Intangible/Sky.refresh_token' => $refresh_token,
                        'Intangible/Sky.access_token'  => $auth_key,
                    ];

                    if ($event['Payment.type'] == 'recurring') {
                        $data['Payment.config_id']  = $payment_config_id;
                        $data['Payment.card_token'] = $cardToken;
                    }

                    if (!empty($event['Payment.returnCardDetails']) && $event['Payment.returnCardDetails'] == 'true') {
                        $data['Payment.card_exp_month'] = $payment_response->credit_card->exp_month ?? '';
                        $data['Payment.card_exp_year']  = $payment_response->credit_card->exp_year ?? '';
                        $data['Payment.card_last_four'] = $payment_response->credit_card->last_four ?? '';
                        $data['Payment.card_name']      = $payment_response->credit_card->name ?? '';
                        $data['Payment.card_type']      = $payment_response->credit_card->card_type ?? '';
                    }

                    return new Success($data);
                } else {
                    return new Failure(
                        $payment_response->message ?? (
                            'Error while create transaction :: ' . json_encode($payment_response)
                        )
                    );
                }
            } else {
                return new Failure(
                    $payment_configurations_response->reason . ' ' . $payment_configurations_response->data
                );
            }
        } catch (\Exception $ex) {
            $exception_msg = $ex->getMessage();
            $return_array  = ['Errors' => true, 'message' => $exception_msg];
            return json_encode($return_array);
            //TODO should log stacktrace
        }
    }

    private function _createOneOffPaymentData($event, $payment_config_id)
    {
        $data                = $this->_getCommonPaymentData($event);
        $data['credit_card'] = [
            'exp_month' => $event['CreditCard/CardDetails.expiryMonth'],
            'exp_year' => $event['CreditCard/CardDetails.expiryYear'],
            'name' => $event['CreditCard/CardDetails.name'],
            'number' => $event['CreditCard/CardDetails.number'],
        ];
        $data['payment_configuration_id'] = $payment_config_id;
        $data['tokenize']                 = false;
        return (object) $data;
    }

    private function _createRecurringPaymentData($event, $payment_config_id, $card_token)
    {
        $data               = $this->_getCommonPaymentData($event);
        $data['card_token'] = $card_token;
        $data['payment_configuration_id'] = $payment_config_id;
        $data['tokenize']                 = false;
        return (object) $data;
    }

    private function _getCommonPaymentData($event)
    {
        $data = [
            'amount' => round($event['MonetaryAmount.value']*100),
            'billing_contact' => [
                'first_name' => $event['Person.givenName'],
                'last_name' => $event['Person.familyName'],
                'address' => $event['PostalAddress.streetAddress'] ,
                'city' => $event['PostalAddress.addressLocality'],
                'country' => $event['PostalAddress.addressCountry'],
                'post_code' => $event['PostalAddress.addressCountry'],
                'state' => $event['PostalAddress.addressRegion']
            ],
            'phone' => $event['Person.telephone'],
            'email' => $event['Person.email'],
            'csc' => $event['CreditCard/CardDetails.cvn']
        ];

        if (!empty($event['Payment.transactionCategory'])) {
            $data['transaction_category'] = $event['Payment.transactionCategory'];
        }

        if (!empty($event['Payment.comment'])) {
            $data['comment'] = $event['Payment.comment'];
        }

        if (!empty($event['Person.ipAddress'])) {
            $data['donor_ip'] = $event['Person.ipAddress'];
        }

        return $data;
    }

    private function _createCardTokenData($event)
    {
        return (object) [
            'exp_month' => $event['CreditCard/CardDetails.expiryMonth'],
            'exp_year' => $event['CreditCard/CardDetails.expiryYear'],
            'issue_number' => $event['CreditCard/CardDetails.cvn'],
            'name' => $event['CreditCard/CardDetails.name'],
            'number' => $event['CreditCard/CardDetails.number']
        ];
    }

    private function _createCardToken($event, $auth_key)
    {
        $url     = 'https://api.sky.blackbaud.com/payments/v1/cardtokens';
        $method  = 'POST';
        $headers = [
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        ];
        $data    = $this->_createCardTokenData($event);

        $response = json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                $data
            )
        );

        if (isset($response->card_token) && !empty($response->card_token)) {
            return $response->card_token;
        } else {
            return new Failure('Unable to create Card Token', $response->message ?? '');
        }
    }

    private function _getPaymentConfigurationList($event)
    {
        $list = $this->_getPaymentConfigurations($event);
        if (is_array($list) && !empty($list)) {
            return new Success($list);
        } else {
            if ($list instanceof Failure) {
                return new Failure($list->reason);
            } else {
                return new Failure('No configurations detected.');
            }
        }
    }

    private function _getPaymentConfigurations($event, $auth_key = null)
    {
        $headers = [
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . (!empty($auth_key) ? $auth_key : $this->_config['access_token'])
        ];

        $method = 'GET';
        $url    = 'https://api.sky.blackbaud.com/payments/v1/paymentconfigurations';

        $response = $this->_skyApiCall($url, $method, $headers, array(), true);
        $payment_configurations_response = json_decode($response, true);

        if (isset($payment_configurations_response['value']) && ! empty($payment_configurations_response['value'])) {
            // has require key index
            return $payment_configurations_response;
        }

        if (isset($payment_configurations_response['statusCode'])
            && $payment_configurations_response['statusCode'] == 401
        ) {
            $refresh_token = $this->_getRefreshToken($event);
            if ($refresh_token instanceof Failure) {
                return $refresh_token;
            }
            $access_token                    = $refresh_token['access_token'];
            $payment_configurations_response = $this->_getPaymentConfigurations($event, $access_token);
            if ($payment_configurations_response instanceof Failure) {
                return $payment_configurations_response;
            }

            $payment_configurations_response['Intangible/Sky.access_token']  = $refresh_token['access_token'];
            $payment_configurations_response['Intangible/Sky.refresh_token'] = $refresh_token['refresh_token'];

            return $payment_configurations_response;
        } else {
            return new Failure('Unable to get Payment Configuration ID');
        }
    }

    private function _getPaymentConfigIdByName($payment_config_response)
    {
        $payment_config_id = '';
        if (isset($payment_config_response['value']) && is_array($payment_config_response['value'])) {
            $get_payment_config = $payment_config_response['value'];

            if ($get_payment_config) {
                foreach ($get_payment_config as $payment_config) {
                    if ($payment_config['name'] == $this->_config['payment_config']
                        || $payment_config['id'] == $this->_config['payment_config']
                    ) {
                        $payment_config_id = $payment_config['id'];
                        break;
                    }
                }
            }
        }
        return $payment_config_id;
    }

    private function _processCheckoutTransaction($event, $auth_key = null, $update_refresh_token = null)
    {
        $headers = [
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . (!empty($auth_key) ? $auth_key : $this->_config['access_token'])
        ];

        $method = 'POST';
        $url    = 'https://api.sky.blackbaud.com/payments/v1/checkout/transaction';
        $data   = [
            'amount' => round($event['MonetaryAmount.value']*100),
            'authorization_token' => $event['Intangible/AuthorizationToken'],
        ];

        $checkout_response = $this->_skyApiCall($url, $method, $headers, $data, false, true);
        MachinePack::log('MP BBMS PAYPAL $checkout_response :: ' . json_encode($checkout_response));
        //Check if response contains valid JSON
        preg_match('/{(?:[^{}]|(?R))*\}/', $checkout_response, $json_matches);
        //if no JSON included look for the data within headers
        if (empty($json_matches)) {
            //Check if Paypal transaction
            preg_match('/(\/v1\/transactions\/)([a-z-0-9])*\w+/', $checkout_response, $header_matches);
            if (!empty($header_matches[0])) {
                $transaction_data = explode('/', $header_matches[0]);
                if (!empty($transaction_data[3])) {
                    $checkout_response = (object) [
                        'id' => $transaction_data[3],
                        'state' => 'Processed',
                        'token' => '',
                        'info' => 'EXTERNAL_GATEWAY'
                    ];
                }
            }
        } else {
            //JSON should be included in the first result
            $checkout_response = json_decode($json_matches[0]);
        }

        if (isset($checkout_response->statusCode)
            && $checkout_response->statusCode == 401
        ) {
            $refresh_token_response = $this->_getRefreshToken($event);
            if ($refresh_token_response instanceof Failure) {
                return $refresh_token_response;
            } else {
                return $this->_processCheckoutTransaction(
                    $event,
                    $refresh_token_response['access_token'],
                    $refresh_token_response['refresh_token']
                );
            }
        }

        if (!empty($checkout_response->id) && !isset($checkout_response->error_code)) {
            $data = [
                'Payment.identifier' => $checkout_response->id,
                'Payment.state' => $checkout_response->state,
                'Payment.token' => $checkout_response->token,
                'Payment.info' => $checkout_response->info ?? '',
                'Payment.billing_info' => $checkout_response->billing_info ?? []
            ];

            if ($update_refresh_token) {
                $data['Intangible/Sky.access_token']  = $auth_key;
                $data['Intangible/Sky.refresh_token'] = $update_refresh_token;
            }

            return new Success($data);
        } else {
            if (!empty($checkout_response->statusCode) && $checkout_response->statusCode == 401) {
                return new Failure($checkout_response->status . ' :: ' . $checkout_response->message);
            }
            if (!empty($checkout_response->ErrorCode)) {
                return new Failure($checkout_response->ErrorCode . ' :: ' . $checkout_response->Message);
            } else {
                return new Failure('Unknown error occurred while processing payment.');
            }
        }
    }

    private function _createAuthURL()
    {
        $auth_data = [
            'client_id' => $this->_config['client_id'],
            'response_type' => 'code',
            'redirect_uri' => $this->_config['redirect_uri']
        ];
        $auth_uri  = 'https://app.blackbaud.com/authorization?' . http_build_query($auth_data);

        return new Success(
            [
                'status' => 'URL Creation',
                'url'    => $auth_uri
            ]
        );
    }

    private function _exchangeCodeForAccessToken($event)
    {
        $body = [
            'grant_type' => 'authorization_code',
            'redirect_uri' => $this->_config['redirect_uri'],
            'code' => $event['Intangible/Sky.application_code']
        ];

        $token = $this->_getToken($body);
        if (!($token instanceof Failure)) {
            return new Success(
                [
                    $token
                ]
            );
        }
        return $token;
    }

    private function _getRefreshToken($event)
    {
        $body = [
            'grant_type' => 'refresh_token',
            'refresh_token' => $this->_config['refresh_token'],
            'preserve_refresh_token' => 'true'
        ];

        if (isset($event['Intangible/Sky.preserve_refresh_token'])
            && $event['Intangible/Sky.preserve_refresh_token'] == 'no'
        ) {
            $body['preserve_refresh_token'] = 'false';
        }

        return $this->_getToken($body);
    }

    private function _getToken($body = array())
    {
        $headers = [
            'Content-type: application/x-www-form-urlencoded',
            'Authorization: Basic ' . base64_encode($this->_config['client_id'] . ':' . $this->_config['client_secret'])
        ];

        $url      = "https://oauth2.sky.blackbaud.com/token";
        $response = $this->_skyApiCall($url, 'POST', $headers, $body, true);
        $token    = json_decode($response, true);

        if (isset($token['error']) && $token['error']) {
            return new Failure(
                json_encode($token['error'])
            );
        }

        return $token;
    }

    private function _getCheckoutPublicKey()
    {
        $headers = [
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $this->_config['access_token']
        ];

        $url = "https://api.sky.blackbaud.com/payments/v1/checkout/publickey";

        $response   = $this->_skyApiCall($url, 'GET', $headers, [], true);
        $public_key = json_decode($response, true);

        if (isset($public_key['public_key']) && !empty($public_key['public_key'])) {
            return new Success(
                [
                    $public_key['public_key']
                ]
            );
        } else {
            if (isset($public_key['statusCode']) && $public_key['statusCode']) {
                return new Failure(($public_key['statusCode']) . ' ' . ($public_key['message'] ?? ''));
            } else {
                return new Failure('Error while getting the public key');
            }
        }
    }

    /**
     * Call the API via curl
     *
     * @param string $method POST|GET|PUT
     * @param string $url
     * @param array $data
     * @return void
     */
    private function _skyApiCall($url, $method, $headers, $data, $auth = false, $include_headers_in_response = false)
    {
        $params = array();

        if ($auth) {
            $params = http_build_query($data);
        } else {
            $params = json_encode($data);
        }

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSLVERSION, 0);

        if ($method == "POST") {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        } elseif ($method == "PUT") {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
        } elseif ($method == "PATCH") {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
            curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        }

        if ($include_headers_in_response) {
            curl_setopt($curl, CURLOPT_HEADER, 1);
        }

        $response = curl_exec($curl);

        if (curl_error($curl)) {
            $error_msg = curl_error($curl);
        }

        curl_close($curl);

        if (isset($error_msg)) {
            throw new \Exception($error_msg);
        }

        if ($include_headers_in_response && !empty($response)) {
            return $response;
        }

        if ($response === null && json_last_error() !== JSON_ERROR_NONE) {
            throw new \Exception('Could not parse JSON response');
        }

        return $response;
    }
}
