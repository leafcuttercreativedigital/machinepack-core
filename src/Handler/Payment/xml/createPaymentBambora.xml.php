<?php return <<<XML
<?xml version ="1.0" encoding="utf-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" 
xmlns:dts="http://www.ippayments.com.au/interface/api/dts">
<soapenv:Header/>
<soapenv:Body>
<dts:SubmitSinglePayment>
<dts:trnXML>
<![CDATA[
	 <Transaction>
      		<CustomerStorageNumber>{$event['Order.identifier']}</CustomerStorageNumber>
      		<CustNumber>{$event['Order.identifier']}</CustNumber>
      		<CustRef>{$event['Order.identifier']}</CustRef>
      		<Amount>$amount</Amount>
     			<TrnType>1</TrnType>
      		<AccountNumber>{$this->_config['accountNumber']}</AccountNumber>
      		<CreditCard>
      		  	<TokeniseAlgorithmID>8</TokeniseAlgorithmID>
							<CardNumber>{$event['CreditCard/CardDetails.number']}</CardNumber>
							<ExpM>{$event['CreditCard/CardDetails.expiryMonth']}</ExpM>
							<ExpY>{$event['CreditCard/CardDetails.expiryYear']}</ExpY>
							<CVN>{$event['CreditCard/CardDetails.cvn']}</CVN>
							<CardHolderName>{$event['CreditCard/CardDetails.name']}</CardHolderName>
       		</CreditCard>
			<Security>
				<UserName>{$this->_config['username']}</UserName>
				<Password>{$this->_config['password']}</Password>
       		</Security>
	</Transaction>
]]>
</dts:trnXML>
</dts:SubmitSinglePayment>
</soapenv:Body>
</soapenv:Envelope>
XML;
