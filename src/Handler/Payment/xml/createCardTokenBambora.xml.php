<?php return <<<XML
<?xml version="1.0" encoding="utf-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
xmlns:sipp="http://www.ippayments.com.au/interface/api/sipp">
<soapenv:Header/>
<soapenv:Body>
<sipp:TokeniseCreditCard>
<sipp:tokeniseCreditCardXML>
<![CDATA[
<TokeniseCreditCard>
     <CustomerStorageNumber>{$accountNumber}</CustomerStorageNumber>  
     <CardNumber>{$event['CreditCard/CardDetails.number']}</CardNumber> 
     <ExpM>{$event['CreditCard/CardDetails.expiryMonth']}</ExpM>
     <ExpY>{$event['CreditCard/CardDetails.expiryYear']}</ExpY>
     <CardHolderName>{$event['CreditCard/CardDetails.name']}</CardHolderName>
     <TokeniseAlgorithmID>{$tokenID}</TokeniseAlgorithmID>
     <UserName>{$username}</UserName>  
     <Password>{$password}</Password>
</TokeniseCreditCard>
]]>
</sipp:tokeniseCreditCardXML>
</sipp:TokeniseCreditCard>
</soapenv:Body>
</soapenv:Envelope>
XML;
