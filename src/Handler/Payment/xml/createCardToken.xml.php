<?php return <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<SecurePayMessage>
    <MerchantInfo>
        <merchantID>{$this->_config['merchantid']}</merchantID>
        <password>{$this->_config['password']}</password>
    </MerchantInfo>
    <RequestType>addToken</RequestType>
    <Token>
        <TokenList count="1">
            <TokenItem ID="1">
                <cardNumber>{$event['CreditCard/CardDetails.number']}</cardNumber>
                <expiryDate>$expiry</expiryDate>
                <tokenType>1</tokenType>
                <amount>$amount</amount>
                <recurringFlag>1</recurringFlag>
            </TokenItem>
        </TokenList>
    </Token>
</SecurePayMessage>
XML;
