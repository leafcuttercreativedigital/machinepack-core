<?php

namespace MachinePack\Core\Handler\Payment;

use Exception;
use MachinePack\Core\Event\Event;
use MachinePack\Core\Event\Events\Data;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Event\Events\Subscription;
use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Result\Success;

class PayPalRestV2 extends Handler
{
    private $_config;
    private $_accessToken;
    private $_service_url;

    public function handleEvent(Event $event): Result
    {
        if (!$event instanceof Payment && !$event instanceof Subscription && !$event instanceof Data) {
            return new Ignored();
        }

        $this->_config = $this->settings[$this->settings['env']];

        //Api credentials can be either passed as args or loaded from config
        //validate configuration variables
        try {
            $this->_fetchValidateConfig($event);
        } catch (\Exception $e) {
            return new Failure($e->getMessage());
        }

        if (empty($this->_config['clientId']) || empty($this->_config['clientSecret'])) {
            return new Failure(
                'Please add handler settings for PayPal. Full config should be:
                    {
                        config: {
                            env: <someenv>,
                            <someenv>: {
                                clientId: "...",
                                clientSecret: "...",
                            }
                        }
                    }
                '
            );
        }

        if (isset($event['Intangible/PaypalRest.action'])) {
            $this->_setServiceUrl();
            $this->_getAuthToken($event);

            if (!empty($this->_accessToken)) {
                if ($event['Intangible/PaypalRest.action'] === 'create_order') {
                    return $this->_createOrder($event);
                } elseif ($event['Intangible/PaypalRest.action'] === 'get_order') {
                    return $this->_getOrder($event);
                } elseif ($event['Intangible/PaypalRest.action'] === 'capture_order') {
                    return $this->_caputeOrder($event);
                } elseif ($event['Intangible/PaypalRest.action'] === 'create_product') {
                    return $this->_createProduct($event);
                } elseif ($event['Intangible/PaypalRest.action'] === 'create_plan') {
                    return $this->_createPlan($event);
                } elseif ($event['Intangible/PaypalRest.action'] === 'get_subscription') {
                    return $this->_getSubscription($event);
                } elseif ($event['Intangible/PaypalRest.action'] === 'create_subscription') {
                    return $this->_createSubscription($event);
                } else {
                    return new Failure('Unknown action provided');
                }
            } else {
                return new Failure('Error while getting access_token token.');
            }
        } else {
            return new Failure('Action parameter not provided.');
        }
    }

    private function _fetchValidateConfig($event)
    {
        //Check the event payload whether it includes valid configuration
        $errors = $this->_fetchValidateEventConfig(
            $event,
            ['clientId', 'clientSecret', 'sandbox']
        );

        if (!empty($errors)) {
            $this->_throwConfigurationException($errors);
        }
    }

    private function _fetchValidateEventConfig($event, $required_fields = [])
    {
        $errors = [];

        foreach ($required_fields as $field) {
            if (!isset($event['Intangible/PaypalRest.' . $field])) {
                if (!isset($this->settings[$this->settings['env']][$field])) {
                    $errors[] = $field;
                } else {
                    $this->_config[$field] = $this->settings[$this->settings['env']][$field];
                }
            } else {
                $this->_config[$field] = $event['Intangible/PaypalRest.' . $field];
            }
        }

        return $errors;
    }

    private function _setServiceUrl()
    {
        if ($this->_config['sandbox']) {
            $this->_service_url = 'https://api-m.sandbox.paypal.com/';
        } else {
            $this->_service_url = 'https://api-m.paypal.com/';
        }
    }

    private function _getAuthToken($event)
    {
        $url     = $this->_service_url . 'v1/oauth2/token';
        $headers = ['Content-Type: application/x-www-form-urlencoded'];
        $data    = ['grant_type' => 'client_credentials',];
        $auth    = $this->_config['clientId'] . ':' . $this->_config['clientSecret'];

        $tokenResponse = $this->_restApiCall($url, 'POST', $headers, $data, $auth);
        if (!empty($tokenResponse->access_token)) {
            $this->_accessToken = $tokenResponse->access_token;
        }
    }

    private function _createOrder($event)
    {
        $url  = $this->_service_url . 'v2/checkout/orders';
        $data = [
            "intent" => "CAPTURE",
            "purchase_units" => [
                [
                    "reference_id" => $event['Order.referenceId'],
                    "amount" => [
                        "currency_code" => $event['MonetaryAmount.currency'] ?? 'AUD',
                        "value" => number_format($event['MonetaryAmount.value'], 2, '.', '') //100.00
                    ]
                ],
            ],
            "payment_source" => [
                "paypal" => [
                    "experience_context" => [
                        "payment_method_preference" => "IMMEDIATE_PAYMENT_REQUIRED",
                        "brand_name" => $event['Order.brandName'],
                        "locale" => "en-AU",
                        "landing_page" => "LOGIN",
                        "shipping_preference" => $event['Order.shipping'],
                        "user_action" => "PAY_NOW",
                        "return_url" => $event['Order.returnUrl'],
                        "cancel_url" => $event['Order.cancelUrl']
                    ]
                ]
            ]
        ];

        if (!empty($event['Order.items'])) {
            $data["purchase_units"][0]["items"] = $event['Order.items'];
        }

        if (!empty($event['Order.description'])) {
            $data["purchase_units"][0]["description"] = $event['Order.description'];
        }

        $order = $this->_restApiCall($url, 'POST', $this->_getDefaultHeaders(), $data);
        if (!empty($order) && !empty($order->id)) {
            return new Success($order);
        } else {
            return new Failure('Error while creating order :: ' . ($order->message ?? ''), $order);
        }
    }

    /**
     * Get the order details
     * @param $event
     */
    private function _getOrder($event)
    {
        $url   = $this->_service_url . 'v2/checkout/orders/' . $event['Order.identifier'];
        $order = $this->_restApiCall($url, 'GET', $this->_getDefaultHeaders(), [], true);
        if (!empty($order) && isset($order->id)) {
            return new Success($order);
        } else {
            return new Failure('Error while fetching order :: Empty response or invalid order id');
        }
    }

    private function _caputeOrder($event)
    {
        $url            = $this->_service_url . 'v2/checkout/orders/' . $event['Order.identifier'] . '/capture';
        $capture_result = $this->_restApiCall($url, 'POST', $this->_getDefaultHeaders(), []);
        if (!empty($capture_result) && isset($capture_result->id)) {
            return new Success($capture_result);
        } else {
            return new Failure('Error while capturing order result :: Empty response or invalid order id');
        }
    }

    private function _createProduct($event)
    {
        $url  = $this->_service_url . 'v1/catalogs/products';
        $data = [
            'id' => $event['Product.productId'] ?? uniqid(),
            'name' => $event['Product.productName'] ?? 'Payment_' . time(),
            'type' => $event['Product.productType'] ?? 'SERVICE',
            'category' => $event['Product.productCategory'] ?? 'CHARITY',
        ];

        $product = $this->_restApiCall($url, 'POST', $this->_getDefaultHeaders(), $data);
        if (!empty($product) && isset($product->id)) {
            return new Success($product);
        } else {
            return new Failure(
                'Error while creating subscription product :: Empty response or invalid product id received.'
            );
        }
    }

    private function _createPlan($event)
    {
        $url  = $this->_service_url . 'v1/billing/plans';
        $data = [
            'product_id' => $event['Intangible/PaypalRest.ProductId'],
            'name' => $event['Intangible/PaypalRest.PlanName'],
            'description' => $event['Intangible/PaypalRest.PlanDescription'],
            'status' => $event['Intangible/PaypalRest.PlanStatus'] ?? 'ACTIVE',
            'billing_cycles' => $this->_createBillingCycles($event),
            'payment_preferences' => $this->_getSubscriptionPaymentPreferences($event),
            'taxes' => [
                'percentage' => $event['MonetaryTax.percentage'] ?? 0,
                'inclusive' => $event['MonetaryTax.inclusive'] ?? false
            ]
        ];

        $plan = $this->_restApiCall($url, 'POST', $this->_getDefaultHeaders(), $data);
        if (!empty($plan) && isset($plan->id)) {
            return new Success($plan);
        } else {
            return new Failure('Error while creating subscription plan :: Empty response or invalid plan id received.');
        }
    }

    private function _getSubscription($event)
    {
        $url          = $this->_service_url . 'v1/billing/subscriptions/' . $event['Order.identifier'];
        $subscription = $this->_restApiCall($url, 'GET', $this->_getDefaultHeaders(), [], true);
        if (!empty($subscription) && isset($subscription->id)) {
            return new Success($subscription);
        } else {
            return new Failure('Error while fetching order :: Empty response or invalid order id');
        }
    }

    private function _createSubscription($event)
    {
        $url = $this->_service_url . 'v1/billing/subscriptions';

        if (!empty($event['Intangible/PaypalRest.SubscriptionStartTime'])) {
            $start_time = date('Y-m-d\TH:i:s\Z', strtotime($event['Intangible/PaypalRest.SubscriptionStartTime']));
        } else {
            $start_time = date('Y-m-d\TH:i:s\Z', strtotime('+1 hour'));
        }

        $data = [
            'plan_id' => $event['Intangible/PaypalRest.PlanId'],
            'start_time' => $start_time,
            'subscriber' => [
                'name' => [
                    'given_name' => $event['Person.givenName'],
                    'surname' => $event['Person.familyName']
                ],
                'email_address' => $event['Person.email']
            ],
            'application_context' => [
                'locale' => 'en-AU',
                'shipping_preference' => 'NO_SHIPPING',
                //Configures the label name to Continue or Subscribe Now for subscription consent experience.
                'user_action' => 'SUBSCRIBE_NOW',
            ]
        ];

        if (!empty($event['Intangible/PaypalRest.SubscriptionReturnUrl'])) {
            $data['application_context']['return_url'] = $event['Intangible/PaypalRest.SubscriptionReturnUrl'];
        }

        if (!empty($event['Intangible/PaypalRest.SubscriptionCancelUrl'])) {
            $data['application_context']['cancel_url'] = $event['Intangible/PaypalRest.SubscriptionCancelUrl'];
        }

        $subscription = $this->_restApiCall($url, 'POST', $this->_getDefaultHeaders(), $data);
        if (!empty($subscription) && isset($subscription->id)) {
            return new Success($subscription);
        } else {
            return new Failure(
                'Error while creating subscription :: Empty response or invalid subscription id received.'
            );
        }
    }

    private function _createBillingCycles($event)
    {
        $value                  = $event['MonetaryAmount.value'];
        $currency               = $event['MonetaryAmount.currency'] ?? 'AUD';
        $billing_cycles         = [];
        $billing_sequence_index = 1;

        if (!empty($event['Intangible/PaypalRest.PlanTrialFrequencyIntervalUnit'])
            && !empty($event['Intangible/PaypalRest.PlanTrialFrequencyIntervalCount'])
        ) {
            $billing_cycles[] = [
                'frequency' => [
                    'interval_unit' => $event['Intangible/PaypalRest.PlanTrialFrequencyIntervalUnit'],
                    'interval_count' => $event['Intangible/PaypalRest.PlanTrialFrequencyIntervalCount']
                ],
                'tenure_type' => 'TRIAL',
                'sequence' => 1,
                //Trial billing cycles can only be executed a finite number of times. Max 999
                'total_cycles' => 1,
                'pricing_scheme' => [
                    'fixed_price' => [
                        'value' => $event['Intangible/PaypalRest.PlanTrialAmount'],
                        'currency_code' => $currency
                    ]
                ]
            ];

            $billing_sequence_index = 2;
        }

        $billing_cycles[] = [
            'frequency' => [
                //The interval at which the subscription is charged or billed.
                //The possible values are: DAY, WEEK, MONTH, YEAR
                'interval_unit' => $event['Intangible/PaypalRest.PlanFrequencyUnit'],
                //The number of intervals after which a subscriber is billed. For example,
                //if the interval_unit is DAY with an interval_count of 2,
                //the subscription is billed once every two days.
                'interval_count' => $event['Intangible/PaypalRest.PlanFrequencyInterval']
            ],
            //The tenure type of the billing cycle. In case of a plan having trial cycle,
            //only 2 trial cycles are allowed per plan.
            'tenure_type' => 'REGULAR',
            //The order in which this cycle is to run among other billing cycles. For example,
            //a trial billing cycle has a sequence of 1 while a regular billing cycle has a sequence of 2,
            //so that trial cycle runs before the regular cycle.
            'sequence' => $billing_sequence_index,
            //The number of times this billing cycle gets executed.
            //Regular billing cycles can only be executed a finite number of times. Max 999
            'total_cycles' => 0,
            'pricing_scheme' => [
                'fixed_price' => [
                    'value' => $value,
                    'currency_code' => $currency
                ]
            ]
        ];

        return $billing_cycles;
    }

    private function _getSubscriptionPaymentPreferences($event)
    {
        $continueOnFailure = $event['Intangible/PaypalRest.PlanContinueOnFailure'] ?? true;
        $preferences       = [
            //Indicates whether to automatically bill the outstanding amount in the next billing cycle.
            'auto_bill_outstanding' => true,
            //The action to take on the subscription if the initial payment for the setup fails.
            'setup_fee_failure_action' => $continueOnFailure ? 'CONTINUE' : 'CANCEL',
            //The maximum number of payment failures before a subscription is suspended
            'payment_failure_threshold' => $event['Intangible/PaypalRest.PlanFailureTreshold'] ?? 3
        ];

        //The initial set-up fee for the service.
        $preferences['setup_fee'] = [
            'value' => $event['Intangible/PaypalRest.SetupFeeValue'] ?? 0,
            'currency_code' => $event['Intangible/PaypalRest.SetupFeeCurrency'] ?? 'AUD',
        ];

        return $preferences;
    }

    private function _getDefaultHeaders()
    {
        return [
            'Content-Type: application/json',
            'Authorization: Bearer ' . $this->_accessToken
        ];
    }

    /**
     * Call the REST API via curl
     *
     * @param string $method POST|GET|PUT
     * @param string $url
     * @param array $data
     * @return void
     */
    private function _restApiCall($url, $method, $headers, $data, $auth = false)
    {
        $params = array();

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSLVERSION, 0);

        if ($auth) {
            curl_setopt($curl, CURLOPT_USERPWD, $auth);
            $params = http_build_query($data);
        } else {
            if (!empty($data)) {
                $params = json_encode($data);
            }
        }

        if ($method == "POST") {
            curl_setopt($curl, CURLOPT_POST, 1);
            if (!empty($params)) {
                curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
            }
        } elseif ($method == "PUT") {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
        } elseif ($method == "PATCH") {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
            curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        } elseif ($method == "DELETE") {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
        }

        $response = curl_exec($curl);

        if (curl_error($curl)) {
            $error_msg = curl_error($curl);
        }

        curl_close($curl);

        if (isset($error_msg)) {
            throw new \Exception($error_msg);
        }

        $jsonResponse = json_decode($response);
        if ($response === null && json_last_error() !== JSON_ERROR_NONE) {
            throw new \Exception('Could not parse JSON response');
        }

        return $jsonResponse;
    }

    private function _throwConfigurationException($errors = [])
    {
        throw new \Exception(
            'Please provide the following configuration fields within your configuration file or the event payload: ' .
            implode(', ', $errors)
        );
    }
}
