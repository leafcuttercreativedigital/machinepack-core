<?php
namespace MachinePack\Core\Handler\Payment;

use MachinePack\Core\MachinePack;
use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Event\Events\Subscription;

class Braintree extends Handler
{
    private $_config;

    public function handleEvent(\MachinePack\Core\Event\Event $event): Result
    {
        if (! $event instanceof Payment) {
            return new Ignored;
        }

        //load config from settings
        $this->_config = $this->settings[$this->settings['env']];

        //validate configuration variables
        if (empty($this->_config['environment'])
            || empty($this->_config['merchantId'])
            || empty($this->_config['publicKey'])
            || empty($this->_config['privateKey'])
        ) {
            return new Failure(
                'Please add handler settings for Braintree. Full config should be:
                {
                    config: {
                        env: <someenv>,
                        <someenv>: {
                            environment: "...",
                            merchantId: "...",
                            publicKey: "...",
                            privateKey: "..."
                        }
                    }
                }
                '
            );
        }

        $gateway = new \Braintree\Gateway(
            [
            'environment' => $this->_config['environment'],
            'merchantId' => $this->_config['merchantId'],
            'publicKey' => $this->_config['publicKey'],
            'privateKey' => $this->_config['privateKey']
            ]
        );

        //depending on one-off or recurring, make payment
        if ($event instanceof Subscription) {
            return $this->_processSubscription($event, $gateway);
        }

        return $this->_processPayment($event, $gateway);
    }

    /**
     * Handle a recurring payment request
     * @param  Payment $event    payment information
     * @return Success|Failure
     */
    private function _processSubscription($event, $gateway)
    {
        try {
            $result = $this->_createCustomer($event, $gateway);

            if ($result->success) {
                $pymtToken = $result->customer->paymentMethods[0]->token;
            } else {
                MachinePack::log(
                    'Unable to create customer',
                    'debug'
                );
                return new Failure(
                    json_encode($result)
                );
            }
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                sprintf(
                    'Unable to create customer - "%s" ',
                    $e
                )
            );
        }

        try {
            $sale = array(
                'paymentMethodToken' => $pymtToken,
                'planId' => $event['Product.productId'],
                'options' => ['startImmediately' => true]
            );

            $result = $gateway->subscription()->create($sale);

            if ($result->success) {
            	$success_array = array(
					'msg' => 'Payment successfull',
                    'amount'=> $event['MonetaryAmount.value'],
                    'payment_token' => $pymtToken,
                    'plan_id' => $event['Product.productId'],
                    'transaction_id' => isset($result->subscription->transactions[0]) ?
                        $result->subscription->transactions[0]->id : '',
            	);
                return new Success(
                    json_encode($success_array)
                );
            } else {
                MachinePack::log(
                    'Unable to create subscription',
                    'debug'
                );
                return new Failure(
                    json_encode($result)
                );
            }
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                sprintf(
                    'Unable to create subscription - "%s" ',
                    $e
                )
            );
        }
    }

    /**
     * Handle a single payment request
     * @param  Payment $event    payment information
     * @return Success|Failure
     */
    private function _processPayment(Payment $event, $gateway)
    {
        try {
            $result = $this->_createCustomer($event, $gateway);

            if ($result->success) {
                  $braintree_cust_id = $result->customer->id;
            } else {
                MachinePack::log(
                    'Unable to create customer',
                    'debug'
                );
                return new Failure(
                    json_encode($result)
                );
            }
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                sprintf(
                    'Unable to create customer - "%s" ',
                    $e
                )
            );
        }

        try {
            $sale   = array(
            'customerId'    => $braintree_cust_id,
            'amount'    => $event['MonetaryAmount.value'],
            'options'   => array('submitForSettlement'    => true)
            );
            $result = $gateway->transaction()->sale($sale);

            if ($result->success) {
                return new Success(
                    [
                    'msg'   =>  'Payment successfull',
                    'amount'    =>  $event['MonetaryAmount.value'],
                    'transaction_id'    =>  $result->transaction->id,
                    ]
                );
            } else {
                MachinePack::log(
                    'Unable to proceed payment',
                    'debug'
                );
                return new Failure(
                    json_encode($result)
                );
            }
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                sprintf(
                    'Unable to proceed payment - "%s" ',
                    $e
                )
            );
        }
    }

    /**
     * Creates Customer and return customer ID
     * @param  $event    $gateway
     * @return Success|Failure
     */
    private function _createCustomer($event, $gateway)
    {

        $result = $gateway->customer()->create(
            array(
            'firstName' => $event['Person.givenName'],
            'lastName'  =>  $event['Person.familyName'],
            'phone'     => $event['Person.telephone'],
            'email'     => $event['Person.email'],
            'creditCard' => array(
                'number'          => $event['CreditCard/CardDetails.number'],
                'cardholderName'  => $event['CreditCard/CardDetails.name'],
                'expirationMonth' => $event['CreditCard/CardDetails.expiryMonth'],
                'expirationYear'  => $event['CreditCard/CardDetails.expiryYear'],
                'cvv'             => $event['CreditCard/CardDetails.cvn'],
                'billingAddress' => array(
                    'postalCode'        => $event['PostalAddress.postalCode'],
                    'streetAddress'     => $event['PostalAddress.streetAddress'],
                    'locality'          => $event['PostalAddress.addressLocality'],
                    'region'            => $event['PostalAddress.addressRegion'],
                    'countryCodeAlpha2' => $event['PostalAddress.addressCountry']
                )
            )
            )
        );
        return $result;
    }
}
