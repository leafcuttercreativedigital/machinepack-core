<?php

namespace MachinePack\Core\Handler\Payment;

use MachinePack\Core\MachinePack;
use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Event\Events\Subscription;
use Stripe\Customer;
use Stripe\Exception\InvalidRequestException;
use Stripe\InvoiceItem;
use Stripe\PaymentIntent;
use Stripe\PaymentMethod;
use Stripe\Product;

class Stripe extends Handler
{
    const TOKEN_FIELD             = 'Intangible/StripePayment.token';
    const EMAIL_FIELD             = 'Person.email';
    const PRODUCT_ID_FIELD        = 'Product.productId';
    const MACHINEPACK_CUSTOMER_ID = 'Intangible/Customer.id';

    public function handleEvent(\MachinePack\Core\Event\Event $event): Result
    {
        if (!$event instanceof Payment) {
            return new Ignored;
        }

        if (isset($event['Intangible/Stripe.config'])) {
            $config = $event['Intangible/Stripe.config'];
        } else {
            $config = $this->settings[$this->settings['env']];
        }

        if (empty($config['secret'])) {
            return new Failure(
                'Please add handler settings for Stripe secret key. Full path should be:
                    {
                        config: {
                            env: <someenv>,
                            <someenv>: {
                                key: "pk_...",
                                secret: "sk_..."
                            }
                        }
                    }
                '
            );
        }

        try {
            \Stripe\Stripe::setApiKey($config['secret']);
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(sprintf('Unable to initialise Stripe ("%s")', $e));
        }

        try {
            $customer = $this->_createStripeCustomer($event);
            if ($customer instanceof Failure) {
                return $customer;
            }
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                sprintf(
                    'Unable to create Customer record with email "%s" ("%s")',
                    $event[Stripe::EMAIL_FIELD],
                    $e
                )
            );
        }

        if ($event instanceof Subscription) {
            return $this->_processSubscription($event, $customer);
        }

        return $this->_processPayment($event, $customer);
    }

    /**
     * handle a single payment request
     * @param Payment $event payment information
     * @param \Stripe\Customer $customer stripe customer data
     * @return Success|Failure
     */
    private function _processPayment(Payment $event, $customer)
    {
        try {
            $paymentMethod = $this->_createPaymentMethod($event, $customer);
            if (!$paymentMethod instanceof PaymentMethod) {
                return new Failure(
                    'Invalid Payment Method',
                    [
                        $paymentMethod->getMessage()
                    ]
                );
            }

            $paymentIntent = $this->_createPaymentIntent($event, $customer, $paymentMethod);

            if (!empty($paymentIntent->charges->data)) {
                if ($paymentIntent->charges->total_count > 1) {
                    MachinePack::log(
                        'STRIPE WARNING: More then one payments received for payment intent. Intent ID:' .
                        $paymentIntent->id
                    );
                }

                $charge = array_pop($paymentIntent->charges->data);
            } else {
                if (!empty($paymentIntent->status)
                    && $paymentIntent->status == 'succeeded'
                    && !empty($paymentIntent->latest_charge)
                ) {
                    //Mock the charge object instead
                    $charge           = new \stdClass();
                    $charge->created  = $paymentIntent->created;
                    $charge->status   = $paymentIntent->status;
                    $charge->customer = $paymentIntent->customer;
                    $charge->id       = $paymentIntent->latest_charge;
                }
            }

            if (isset($charge->status) && $charge->status === 'succeeded') {
                return new Success(
                    [
                        'MoneyTransfer.identifier'               => $paymentIntent->id,
                        'MoneyTransfer.timestamp'                => $charge->created,
                        'Intangible/APIResponse.APIResponseCode' => $charge->status,
                        'Intangible/Customer.id'                 => $charge->customer,
                        'Intangible/Payment.id'                  => $charge->id
                    ]
                );
            } else {
                return new Failure(
                    'Unable to charge customer',
                    [
                        $charge ?? 'Unable to create charge object'
                    ]
                );
            }
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                sprintf(
                    'Unable to charge customer "%s" total of %.2f %s ("%s")',
                    $event[Stripe::EMAIL_FIELD],
                    $event['MonetaryAmount.value'],
                    $event['MonetaryAmount.currency'],
                    $e
                )
            );
        }
    }

    private function _createPaymentMethod(Payment $event, $customer)
    {
        //If the payment already exists try to get it
        if (!empty($event['CreditCard/PaymentMethod.id'])) {
            $paymentMethod = PaymentMethod::retrieve($event['CreditCard/PaymentMethod.id']);
        } else {
            $paymentMethod = PaymentMethod::create(
                [
                    'type' => 'card',
                    'billing_details' => [
                        'address' => [
                            'line1' => $event['PostalAddress.streetAddress'],
                            'city' => $event['PostalAddress.addressLocality'],
                            'country' => $event['PostalAddress.addressCountry'],
                            'state' => $event['PostalAddress.addressRegion'],
                            'postal_code' => $event['PostalAddress.postalCode']
                        ],
                        'email' => $event['Person.email'],
                        'name' => $event['CreditCard/CardDetails.name'],
                        'phone' => $event['Person.telephone'],
                    ],
                    'card' => [
                        'number' => $event['CreditCard/CardDetails.number'],
                        'exp_month' => $event['CreditCard/CardDetails.expiryMonth'],
                        'exp_year' => $event['CreditCard/CardDetails.expiryYear'],
                        'cvc' => $event['CreditCard/CardDetails.cvn']
                    ]
                ]
            );
        }

        if ($paymentMethod instanceof PaymentMethod) {
            $attach = $paymentMethod->attach(
                [
                    'customer' => $customer->id
                ]
            );
        }

        return $paymentMethod;
    }

    private function _createPaymentIntent(Payment $event, $customer, $paymentMethod)
    {
        try {
            $intentParams = [
                'amount' => round($event['MonetaryAmount.value'] * 100.00),
                'currency' => $event['MonetaryAmount.currency'],
                //https://stripe.com/docs/payments/accept-a-payment?integration=elements
                //FOR one-off payment use payment data instead
                'payment_method' => $paymentMethod->id,
                'customer' => $customer->id,
                'confirm' => true,
                'automatic_payment_methods' => [
                    'enabled' => true,
                    'allow_redirects' => 'never'
                ]
            ];

            if (isset($event['Intangible/Stripe.description']) && !empty($event['Intangible/Stripe.description'])) {
                $intentParams['description'] = $event['Intangible/Stripe.description'];
            }

            if (isset($event['Intangible/Stripe.destination']) && !empty($event['Intangible/Stripe.destination'])) {
                $intentParams['transfer_data'] = [
                    'destination' => $event['Intangible/Stripe.destination']
                ];
            }

            $intent = PaymentIntent::create($intentParams);
            if ($intent->object == "payment_intent") {
                return $intent;
            } else {
                return new Failure('Unable to create a payment intent - invalid API response');
            }
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                sprintf(
                    'Unable to create a payment intent for customer "%s" total of %.2f %s ("%s")',
                    $event[Stripe::EMAIL_FIELD],
                    $event['MonetaryAmount.value'],
                    $event['MonetaryAmount.currency'],
                    $e
                )
            );
        }
    }

    /**
     * handle a subscription. it must have a product id for this to work
     * @param Subscription $event subscription information
     * @param \Stripe\Customer $customer stripe customer data
     * @return Success|Failure
     */
    private function _processSubscription(Subscription $event, $customer)
    {
        $paymentMethod = $this->_createPaymentMethod($event, $customer);
        if ($paymentMethod instanceof InvalidRequestException) {
            return new Failure(
                'Invalid Payment Method',
                [
                    $paymentMethod->getMessage()
                ]
            );
        }

        //If there is any additional invoice item
        if (isset($event['Intangible/AdditionalInvoiceAmount'])) {
            $item = $this->_createInvoiceItem($event, $customer);
            if (!$item instanceof Success) {
                return new Failure('Unable to create invoice item. ' . $item->reason);
            }
        }

        $plan = $this->_createPlan($event);
        if ($plan instanceof Success) {
            $plan = $plan->data->id;
        } else {
            return new Failure('Unable to create plan. ' . $plan->reason);
        }

        try {
            $subscriptionData = [
                'customer' => $customer->id,
                'items'    => [['plan' => $plan]],
                'default_payment_method' => $paymentMethod->id
            ];

            if (isset($event['Intangible/Stripe.description']) && !empty($event['Intangible/Stripe.description'])) {
                $subscriptionData['description'] = $event['Intangible/Stripe.description'];
            }

            if (isset($event['Intangible/Stripe.destination']) && !empty($event['Intangible/Stripe.destination'])) {
                $subscriptionData['transfer_data'] = [
                    'destination' => $event['Intangible/Stripe.destination']
                ];
            }

            $subscription = \Stripe\Subscription::create($subscriptionData);
            /*
             * 09/12/2020 - Do not set the billing cycle anchor, use the trial period instead
            if (isset($event['Product/Subscription.startDate']) && !empty($event['Product/Subscription.startDate'])) {
                $subscriptionData['billing_cycle_anchor'] = $event['Product/Subscription.startDate'];
            }*/
            if (isset($event['Product/Subscription.startDate']) && !empty($event['Product/Subscription.startDate'])) {
                $subscription->updateAttributes(
                    [
                        'proration_behavior' => 'none',
                        'trial_end' => strtotime($event['Product/Subscription.startDate'])
                    ]
                );
                $subscription->save();
            }

            if (isset($subscription->status) && in_array($subscription->status, ['active', 'trialing'])) {
                return new Success(
                    [
                        'MoneyTransfer.identifier'               => $subscription->id,
                        'MoneyTransfer.timestamp'                => $subscription->created,
                        'Intangible/APIResponse.APIResponseCode' => $subscription->status,
                        'Intangible/Customer.id'                 => $subscription->customer,
                        'Intangible/Payment.id'                  => $subscription->id,
                    ]
                );
            } else {
                return new Failure(
                    'Unable to charge/subscribe customer',
                    [
                        $subscription
                    ]
                );
            }
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                sprintf(
                    'Unable to charge customer "%s" total of %.2f %s ("%s")',
                    $event[Stripe::EMAIL_FIELD],
                    $event['MonetaryAmount.value'],
                    $event['MonetaryAmount.currency'],
                    $e
                )
            );
        }
    }

    /**
     * Set up a Stripe client
     * @param  Payment $event event data
     * @return \Stripe\Customer
     */
    private function _createStripeCustomer(Payment $event)
    {
        if (!$event[Stripe::MACHINEPACK_CUSTOMER_ID]) {
            try {
                $customer = \Stripe\Customer::create(
                    [
                        'address' => [
                            'line1'       => $event['PostalAddress.streetAddress'],
                            'city'        => $event['PostalAddress.addressLocality'],
                            'country'     => $event['PostalAddress.addressCountry'],
                            'postal_code' => $event['PostalAddress.postalCode'],
                            'state'       => $event['PostalAddress.addressRegion'],
                        ],
                        'description' => sprintf(
                            "%s %s <%s>",
                            $event['Person.givenName'],
                            $event['Person.familyName'] ?? '',
                            $event[Stripe::EMAIL_FIELD]
                        ),
                        'email'  => $event[Stripe::EMAIL_FIELD],
                        'name'   => sprintf("%s %s", $event['Person.givenName'], $event['Person.familyName'] ?? ''),
                        'phone'  => $event['Person.telephone'] ?? ''
                    ]
                );
                return $customer;
            } catch (\Exception $e) {
                return new Failure('Error: ' . $e->getMessage());
            }
        } else {
            $customer_id = $event[Stripe::MACHINEPACK_CUSTOMER_ID];
        }
    }

    private function _createInvoiceItem(Payment $event, $customer)
    {
        try {
            $invoiceItemData = [
                'customer'    => $customer->id,
                'amount'      => round($event['Intangible/AdditionalInvoiceAmount'] * 100.00),
                'currency'    => $event['MonetaryAmount.currency'],
                'description' => $event['Intangible/AdditionalInvoiceDescription']
            ];

            $invoiceItem = InvoiceItem::create($invoiceItemData);

            return new Success(
                $invoiceItem
            );
        } catch (\Exception $e) {
            return new Failure('Error: ' . $e->getMessage());
        }
    }

    /**
     * Set up a Stripe Plan
     * @param Payment $event event data
     * @return \Stripe\Plan
     */
    private function _createPlan(Payment $event)
    {
        try {
            $planData = [
                'amount'   => round($event['MonetaryAmount.value'] * 100.0),
                'currency' => $event['MonetaryAmount.currency'],
                'interval' => 'month',
            ];

            //Check if the product id is valid
            if ($this->_productExist('id', $event[Stripe::PRODUCT_ID_FIELD])) {
                $planData['product'] = $event[Stripe::PRODUCT_ID_FIELD];
            } else {
                //If the product id is not valid and there's a product name try to fetch by name or create it
                if (isset($event['Product.productName']) && !empty($event['Product.productName'])) {
                    $product = $this->_findOrCreateProductByName($event['Product.productName']);
                    if ($product) {
                        $planData['product'] = $product->id;
                    }
                    //If the product id is not valid and there's no product name create a generic product
                } else {
                    $new_product = $this->_createProduct(['Product.productName' => 'Recurring Donation']);
                    if ($new_product instanceof Success) {
                        $planData['product'] = $new_product->data->id;
                    }
                }
            }

            $plan = \Stripe\Plan::create($planData);

            return new Success(
                $plan
            );
        } catch (\Exception $e) {
            return new Failure('Error: ' . $e->getMessage());
        }
    }


    /**
     * Create a card token for recurring payments
     * @param Payment $event payment information
     * @param \Stripe\Token $cardtoken stripe customer data
     * @return Success|Failure
     */
    private function _createCardToken(\MachinePack\Core\Event\Event $event)
    {
        try {
            $cardToken = \Stripe\Token::create(
                [
                    'card' => [
                        'cardNumber'      => $event['CreditCard/CardDetails.number'],
                        'expiryDateMonth' => $event['CreditCard/CardDetails.expiryMonth'],
                        'expiryDateYear'  => $event['CreditCard/CardDetails.expiryYear'],
                        'cvn'             => $event['CreditCard/CardDetails.cvn']
                    ],
                ]
            );
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                sprintf(
                    'Unable to create customer token with card ("%s")',
                    $e
                )
            );
        }
        return new Success(
            [
                'token' => $cardToken
            ]
        );
    }

    /**
     * Get the list of existing products
     * @return Failure|Success
     */
    private function _getProducts()
    {
        try {
            $products = Product::all();
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                sprintf(
                    'Unable to fetch the list of products',
                    $e
                )
            );
        }
        return new Success(
            $products->data
        );
    }

    private function _createProduct($event)
    {
        try {
            $product = Product::create(
                [
                    'name' => $event['Product.productName']
                ]
            );
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                sprintf(
                    'Unable to create product',
                    $e
                )
            );
        }
        return new Success(
            $product
        );
    }

    /**
     * Verify that the remote product exists
     * @param $product_id
     * @return bool
     */
    private function _productExist($product_id)
    {
        $remoteProducts = $this->_getProducts();
        if ($remoteProducts instanceof Success) {
            foreach ($remoteProducts->data as $product) {
                if ($product_id == $product->id) {
                    return true;
                }
            }
        } else {
            return false;
        }

        return false;
    }

    private function _findOrCreateProductByName($productName)
    {
        //Try to find the object by the name
        $remoteProducts = $this->_getProducts();
        if ($remoteProducts instanceof Success) {
            foreach ($remoteProducts->data as $product) {
                if ($productName == $product->name) {
                    return $product;
                }
            }
        }

        //If not found, create a new one
        $newProduct = $this->_createProduct(['Product.productName' => $productName]);
        if ($newProduct instanceof Success) {
            return $newProduct->data;
        }

        return false;
    }
}
