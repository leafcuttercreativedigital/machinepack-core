<?php
namespace MachinePack\Core\Handler\Payment;

use MachinePack\Core\MachinePack;
use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Event\Events\Subscription;

class Bambora extends Handler
{
    const CONFIG_FIELDS = [
        'username',
        'password',
        'accountNumber',
        'dtsapiURL',
        'sippapiURL',
        'tokeniseAlgorithmID'
    ];

    private $_config;

    public function handleEvent(\MachinePack\Core\Event\Event $event): Result
    {
        if (! $event instanceof Payment) {
            return new Ignored;
        }

        $this->_fetchConfig($event);

        //validate configuration variables
        if ((empty($this->_config['username'])
            || empty($this->_config['password'])
            || empty($this->_config['accountNumber'])
            || empty($this->_config['dtsapiURL'])
            || empty($this->_config['sippapiURL'])
            || empty($this->_config['tokeniseAlgorithmID']))
            && (empty($event['Intangible/API.username'])
            || empty($event['Intangible/API.password'])
            || empty($event['Intangible/API.accountNumber'])
            || empty($event['Intangible/API.tokeniseAlgorithmID'])
            || empty($event['Intangible/API.dtsapiURL'])
            || empty($event['Intangible/API.sippapiURL']))
        ) {
            return new Failure(
                'Please add handler settings for Bambora. Full config should be:
                    {
                        config: {
                            env: <someenv>,
                            <someenv>: {
                                username: "...",
                                password: "...",
                                accountNumber: "...",
                                dtsapiURL: "...",
                                sippapiURL: "..."
                            }
                        }
                    }
                '
            );
        }

        if (!$event['Order.identifier']) {
            return new Failure('Error ::: No valid order identifier supplier.');
        }
        
        //Trigger the PayPal API flow
        if (isset($event['Order.PayPal']) && $event['Order.PayPal']) {
            return $this->_processPayPal($event);
        }
        //depending on one-off or recurring, make payment
        if ($event instanceof Subscription) {
            return $this->_processSubscription($event);
        }

        return $this->_processPayment($event);
    }

    private function _fetchConfig($event)
    {
        //Check the event payload whether it includes valid configuration
        $is_event_config_valid = true;
        foreach (self::CONFIG_FIELDS as $key) {
            if (isset($event['Intangible/API.' . $key]) && (!empty($event['Intangible/API.' . $key]))
            ) {
                $this->_config[$key] = $event['Intangible/API.' . $key];
            } else {
                $is_event_config_valid = false;
            }
        }

        if (!$is_event_config_valid) {
            //load config from settings
            $this->_config = $this->settings[$this->settings['env']];
        }
    }

    /**
     * Handle a recurring payment request
     * @param  Payment $event    payment information
     * @return Success|Failure
     */
    private function _storeCreditCard($event)
    {
        try {
            $response = $this->_submitCreditCard($event);
            if (isset($response->ReturnValue) && $response->ReturnValue->__toString() !== "0") {
                return new Failure(
                    json_encode($response)
                );
            }
            $token = $response->Token->__toString();
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }
        return array(
            'MoneyTransfer.token' => $token
        );
    }

    /**
     * Handle the PayPal payment request
     * @param  Payment $event    payment information
     * @return Success|Failure
     */
    private function _processPayPal($event)
    {
        try {
            $paypal_data = $this->_buildPaypalDataArray($event);
            $headers     = array(
                "Content-Type: application/x-www-form-urlencoded",
                "Cache-Control: no-cache"
            );
            $url         = 'https://demo.ippayments.com.au/access/index.aspx';

            $parser   = $this->_curl('POST', $headers, $paypal_data, false, $url);
            $error    = $parser->body->form->input[1]->attributes()['value']->__toString();
            $response = $parser->body->form->input[2]->attributes()['value']->__toString();
            
            if ($error) {
                return new Failure(
                    'Error - Something went wrong when trying to create the PayPal session.'
                );
            }
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }

        return new Success(
            [
                'MoneyTransfer.identifier' => $response
            ]
        );
    }

    /**
     * Builds the data array required for PayPal API
     * @param  Payment $event    payment information
     * @return Array
     */
    private function _buildPaypalDataArray($event)
    {

        $amount      = (float) ($event['MonetaryAmount.value']) * 100;
        $paypal_data = array(
            "UserName" => $this->_config['username'],
            'password' => $this->_config['password'],
            "CustRef" => $event['Intangible/Customer.Reference'],
            "CustNumber" => $event['Order.identifier'],
            "Amount" => $amount,
            "SessionId" => $event['Intangible/Session.Key'],
            "SessionKey" => $event['Intangible/Session.Key'],
            "DL" => 'checkout_v1_purchase',
            "ServerURL" => $event['Intangible/Server.URL'],
            "UserURL" => $event['Intangible/User.URL'],
            "AccountNumber" => $this->_config['accountNumber']
        );

        $pp_refernce_1 = array(
            $event['Intangible/PPName'], //Name
            $event['Intangible/PPDesc'], //Desc
            $amount, //AMT
            $event['Intangible/Number'], //Number
            1, //Quantity
            'Donation', //Item Category

        );
        $paypal_data['PP_Reference1'] = $this->_generatePPResponse($pp_refernce_1);

        $pp_refernce_2 = array(
            '', //PayPalNumber
            '', //PageStyle
            $event['Intangible/BrandName'], //BrandName
            $event['Intangible/NoShipping'], //NoShipping
            '', //AddrOverride
            '', //LandingPage
            $event['Person.email'], //Email
            '' //AllowNote

        );
        $paypal_data['PP_Reference2'] = $this->_generatePPResponse($pp_refernce_2);

        $pp_refernce_3 = array(
            '', //ShipToName
            '', //ShipToStreet
            '', //ShipToStreet2
            '', //ShipToCity
            '', //ShipToState
            '', //ShipToZip
            '', //ShipToCountryCode
            '', //ShipToPhoneNum

        );
        $paypal_data['PP_Reference3'] = $this->_generatePPResponse($pp_refernce_3);

        $pp_refernce_4 = array(
           $amount, //ItemAMT
           '', //ShippingGAMT
           '', //TaxAMT
           $event['Intangible/PPDesc'], //DescOrder
           '', //Custom
           '', //NotifyURL
           '', //SolutionType

        );
        $paypal_data['PP_Reference4'] = $this->_generatePPResponse($pp_refernce_4);

        return http_build_query($paypal_data);
    }

    /**
     * Generates the PP Response required by Bambora
     * @param  Payment $event    payment information
     * @return base64
     */
    private function _generatePPResponse($pp_refence)
    {
        $reference = \implode('|', $pp_refence);
        return base64_encode($reference);
    }

    /**
     * Handle a recurring payment request
     * @param  Payment $event    payment information
     * @return Success|Failure
     */
    private function _processSubscription($event)
    {
        try {
            $card_token = $this->_storeCreditCard($event);
            if ($card_token instanceof Failure) {
                return $card_token;
            }
            $response = $this->_createSubscription($event);
            if ($response->ResponseCode->__toString() !== "0") {
                return new Failure(
                    json_encode($response)
                );
            }
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }

        return new Success(
            [
                'MoneyTransfer.token'                           => $card_token['MoneyTransfer.token'],
                'MoneyTransfer.identifier'                      => $response->ScheduleIdentifier->__toString(),
                'Intangible/APIResponse.APIResponseCode'        => $response->ResponseCode->__toString(),
            ]
        );
    }

    /**
     * Handle a single payment request
     * @param  Payment $event    payment information
     * @return Success|Failure
     */
    private function _processPayment(Payment $event)
    {
        try {
            $response = $this->_createPayment($event);
            if ($response->ResponseCode->__toString() !== "0") {
                return new Failure(
                    json_encode($response)
                );
            }
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }

        return new Success(
            [
                'MoneyTransfer.token'                           => $response->CreditCardToken->__toString(),
                'MoneyTransfer.identifier'                      => $response->Receipt->__toString(),
                'Intangible/APIResponse.TruncatedCard'          => $response->TruncatedCard->__toString(),
                'Intangible/APIResponse.expiry'                 => $response->ExpM->__toString() . '/' .
                                                                    $response->ExpY->__toString(),
            ]
        );
    }

    /**
     * Handle a single payment request
     * @param  Payment $event    payment information
     * @return Success|Failure
     */
    private function _submitCreditCard($event)
    {
        $amount          = (float) ($event['MonetaryAmount.value']) * 100;
        $tokenID         = $this->_config['tokeniseAlgorithmID'];
        $accountNumber   = $this->_config['accountNumber'];
        $username        = $this->_config['username'];
        $password        = $this->_config['password'];
        $xml_post_string = require __DIR__ . '/xml/createCardTokenBambora.xml.php';
        $headers         = array(
            "Content-type: text/xml;charset=UTF-8",
            "SOAPAction: http://www.ippayments.com.au/interface/api/sipp/TokeniseCreditCard",
            "Content-length: ".strlen($xml_post_string)
        );

        $parser = $this->_curl('POST', $headers, $xml_post_string, true);

        // user $parser to get your data out of XML response and to display it.
        return simplexml_load_string($parser->TokeniseCreditCardResponse->TokeniseCreditCardResult);
    }

    /**
     * Handle a single payment request
     * @param  Payment $event    payment information
     * @return Success|Failure
     */
    private function _createPayment($event)
    {
        $amount          = (float) ($event['MonetaryAmount.value']) * 100;
        $xml_post_string = require __DIR__ . '/xml/createPaymentBambora.xml.php';

        $headers = array(
            "Content-type: text/xml;charset=UTF-8",
            "SOAPAction: http://www.ippayments.com.au/interface/api/dts/SubmitSinglePayment",
            "Content-length: ".strlen($xml_post_string)
        );

        $parser = $this->_curl('POST', $headers, $xml_post_string);
        // user $parser to get your data out of XML response and to display it.
        return simplexml_load_string($parser->SubmitSinglePaymentResponse->SubmitSinglePaymentResult);
    }

    /**
     * Handle a recurring payment request
     * @param  Payment $event    payment information
     * @return Success|Failure
     */
    private function _createSubscription($event)
    {
        $amount          = (float) ($event['MonetaryAmount.value']) * 100;
        $date            = isset($event['Product/Subscription.date'])
        && $event['Product/Subscription.date'] ? $event['Product/Subscription.date'] : date('Y-m-d');
        $frequency       = isset($event['Product/Subscription.duration'])
        && !empty($event['Product/Subscription.duration']) ? $event['Product/Subscription.duration'] : 'M';
        $xml_post_string = require __DIR__ . '/xml/createSubscriptionBambora.xml.php';

        $headers = array(
            "Content-type: text/xml;charset=UTF-8",
            "SOAPAction: http://www.ippayments.com.au/interface/api/dts/SubmitPaymentSchedule",
            "Content-length: ".strlen($xml_post_string)
        );

        $parser = $this->_curl('POST', $headers, $xml_post_string);

        // user $parser to get your data out of XML response and to display it.
        return simplexml_load_string($parser->SubmitPaymentScheduleResponse->SubmitPaymentScheduleResult);
    }

    /**
     * Call the API via curl
     *
     * @param string $method POST|GET|PUT
     * @param string $url
     * @param array $data
     * @return void
     */
    private function _curl($method, $headers, $data = array(), $is_sipp = false, $url = false)
    {
        if (!$url) {
            $url = $this->_config['dtsapiURL'];
            if ($is_sipp) {
                $url = $this->_config['sippapiURL'];
            }
        }

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSLVERSION, 0);

        if ($method == "POST") {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        } elseif ($method == "PUT") {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
        }

        $response = curl_exec($curl);
        // converting
        $response1 = str_replace("<soap:Body>", "", $response);
        $response2 = str_replace("</soap:Body>", "", $response1);

        // converting to XML
        $response = simplexml_load_string($response2);

        if (curl_error($curl)) {
            $error_msg = curl_error($curl);
        }

        curl_close($curl);

        if (isset($error_msg)) {
            throw new \Exception($error_msg);
        }

        if ($response === null && json_last_error() !== JSON_ERROR_NONE) {
            throw new \Exception('Could not parse JSON response');
        }

        return $response;
    }
}
