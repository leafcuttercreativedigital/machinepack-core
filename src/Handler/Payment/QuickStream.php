<?php
namespace MachinePack\Core\Handler\Payment;

use MachinePack\Core\MachinePack;
use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Event\Events\Subscription;

class QuickStream extends Handler
{

    private $_config;

    public function handleEvent(\MachinePack\Core\Event\Event $event): Result
    {
        if (!$event instanceof Payment) {
            return new Ignored;
        }

        //load config from settings
        $this->_config = $this->settings[$this->settings['env']];

        //validate configuration variables
        if (empty($this->_config['SecretAPIKey'])
            || empty($this->_config['PublishableAPIKey'])
            || empty($this->_config['apiBaseUrl'])
            || empty($this->_config['SupplierBusinessCode'])
            || !array_key_exists('isProductionMode', $this->_config)
        ) {
            MachinePack::log('Please add handler settings for QuickStream.', 'debug');
            return new Failure(
                'Please add handler settings for QuickStream. Full config should be:
                    {
                        config: {
                            env: <someenv>
                            <someenv>: {
                                SecretAPIKey: "..."
                                publishableApiKey: "..."
                                apiBaseUrl: "..."
                                SupplierBusinessCode: "..."
                                isProductionMode: "..."
                            }
                        }
                    }
                '
            );
        }

        //otherwise depending on one-off or recurring, make payment
        if ($event instanceof Subscription) {
            return $this->_processSubscription($event);
        }

        return $this->_processPayment($event);
    }

    /**
     * Handle a payment request (recurring or one off)
     * @param  Payment $event       payment information
     * @param  boolean $recurring   recurring payment flag
     * @return Success|Failure
     */
    private function _processPayment(Payment $event)
    {
        try {
            //COMMENTING THIS CODE OUT DUE TO CLIENT REQUEST (SDMF)
            /* $customer = $this->_customerSearchRequest($event);
            if (!$customer) {
                $customer = $this->_customerCaptureRequest($event);
            }

            if (!$customer instanceof Failure) {
                $customerId     = $customer['customerId'];
                $customerNumber = $customer['customerNumber'];
                $tokenId        = $this->_createTokenCaptureRequest($event);

                if (!$tokenId instanceof Failure) {
                    //Removing the check for an existing account
                    //$accountId = $this->_accountSearchRequest($event, $customerId);
                    //$accountId   = $this->_accountCaptureRequest($event, $tokenId, $customerId);
                    $transaction = $this->_transactionCaptureRequest($event, $tokenId);
                } else {
                    return $tokenId;
                }
             } else {
                return $customerId;
            } */

            //SDMF PROCESS
            if ($event['Order.recurring'] === 'MONTHLY') {
                //SDMF PROCESS
                $customer = $this->_customerCaptureRequest($event);
                if (!$customer instanceof Failure) {
                    $customerId     = $customer['customerId'];
                    $customerNumber = $customer['customerNumber'];
                    $accountId      = $this->_accountCaptureRequest($event, $customerId);

                    if (!$accountId instanceof Failure) {
                        $tokenId     = null;
                        $transaction = $this->_transactionCaptureRequest($event, $tokenId, $customerNumber, $accountId);
                    } else {
                        return $accountId;
                    }
                } else {
                    return $customer;
                }
            } else {
                $tokenId = $this->_createTokenCaptureRequest($event);
                if (!$tokenId instanceof Failure) {
                    $transaction = $this->_transactionCaptureRequest($event, $tokenId);
                } else {
                    return $tokenId;
                }
            }

            //if response code is not set or is not 0, there was an error in taking payment
            if (!isset($transaction->status) || $transaction->status != "Approved") {
                MachinePack::log(
                    'QuickStream could not take payment, response code: ' .
                        $transaction->responseCode,
                    'debug'
                );
                return new Failure(
                    'Error - ' . $transaction->responseCode . ' - ' . $transaction->responseDescription
                );
            }

            //otherwise, payment was taken, return success
            return new Success(
                [
                    'Person.identifier'                                     => $customerId,
                    'MoneyTransfer.identifier'                              => $transaction->receiptNumber,
                    'Intangible/APIResponse.APIResponseCode'                => $transaction->responseCode,
                    'Intangible/APIResponse.APIResponseMessage'             => $transaction->responseDescription,
                    'Intangible/APIResponse.Status'                         => $transaction->status,
                ]
            );
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }
    }

    /**
     * Handle a payment request (recurring or one off)
     * @param  Payment $event       payment information
     * @param  boolean $recurring   recurring payment flag
     * @return Success|Failure
     */
    private function _processSubscription(Payment $event)
    {
        try {
            $customer = $this->_customerSearchRequest($event);
            if (!$customer) {
                $customer = $this->_customerCaptureRequest($event);
            }

            if (!$customer instanceof Failure) {
                $customerId     = $customer['customerId'];
                $customerNumber = $customer['customerNumber'];
                $tokenId        = $this->_createTokenCaptureRequest($event);

                if (!$tokenId instanceof Failure) {
                    //Removing the check for an existing account
                    //$accountId = $this->_accountSearchRequest($event, $customerId);
                    $accountId   = $this->_accountCaptureRequest($event, $customerId);
                    $transaction = $this->_recurringCaptureRequest($event, $customerId, $accountId);
                } else {
                    return $tokenId;
                }
            } else {
                return $customerId;
            }

            //if response code is not set or is not 0, there was an error in taking payment
            if (!isset($transaction->paymentScheduleId)) {
                MachinePack::log(
                    'QuickStream could not process the recurring payment',
                    'debug'
                );
                return new Failure(
                    json_encode($transaction)
                );
            }

            //otherwise, payment was taken, return success
            return new Success(
                [
                    'Person.identifier'                                     => $customerId,
                    'MoneyTransfer.identifier'                              => $transaction->paymentScheduleId,
                ]
            );
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }
    }

    /**
     * Create a customer request to send to QuickStream
     * @param  Payment $event    subscription information
     * @return array
     */
    private function _customerCaptureRequest(\MachinePack\Core\Event\Event $event)
    {
        $stamp = date("Y-m-d_H:i:s");
        $url   = $this->_config['apiBaseUrl'] . '/customers';
        $txn   = [
            'customerNumber' => $event['Person.givenName']. '_'. $event['Person.familyName'] . '_' . $stamp,
            'customerName' => $event['Person.givenName'].'_'.$event['Person.familyName'],
            'phoneNumber' => $event['Person.telephone'],
            'emailAddress' => $event['Person.email'],
            'preferredNotificationMedium' => $event['Person.notification'],
            'address' => [
                'street1' => $event['PostalAddress.streetAddress'],
                'city' => $event['PostalAddress.addressLocality'],
                'state' => $event['PostalAddress.addressRegion'],
                'postcode' => $event['PostalAddress.postalCode'],
                'country' => $event['PostalAddress.addressCountry'],
            ],
        ];

        $response = $this->_callAPI('POST', $url, $txn);

        if (!empty($response->errors)) {
            $error_string = '';
            foreach ($response->errors as $error) {
                $error_string .= $error->messages[0];
            }

            return new Failure(
                'Error - ' . $error_string
            );
        }

        return array(
            'customerId' => $response->customerId,
            'customerNumber' => $response->customerNumber
        );
    }

    /**
     * Create a customer request to send to QuickStream
     * @param  Payment $event    subscription information
     * @return array
     */
    private function _customerSearchRequest(\MachinePack\Core\Event\Event $event)
    {
        $url      = $this->_config['apiBaseUrl'] . '/customers?emailAddress=' . $event['Person.email'];
        $response = $this->_callAPI('GET', $url);
        if (empty($response->data)) {
            return null;
        }

        return array(
            'customerId' => $response->data[0]->customerId,
            'customerNumber' => $response->data[0]->customerNumber
        );
    }

    /**
     * Create a token capture request to send to QuickStream
     * @param  Payment $event    subscription information
     * @return array
     */
    private function _createTokenCaptureRequest(\MachinePack\Core\Event\Event $event)
    {
        $url = $this->_config['apiBaseUrl'] . '/single-use-tokens';
        $txn = [
            'supplierBusinessCode' => $this->_config['SupplierBusinessCode'],
            'accountType' => 'CREDIT_CARD',
            'cardholderName' => $event['CreditCard/CardDetails.name'],
            'cardNumber' => $event['CreditCard/CardDetails.number'],
            'expiryDateMonth' => $event['CreditCard/CardDetails.expiryMonth'],
            'expiryDateYear' =>$event['CreditCard/CardDetails.expiryYear'],
            'cvn' => $event['CreditCard/CardDetails.cvn'],
        ];

        $response = $this->_callAPI('POST', $url, $txn, true);

        if (!empty($response->errors)) {
            $error_string = '';
            foreach ($response->errors as $error) {
                $error_string .= $error->messages[0] . '. ';
            }

            return new Failure(
                'Error - ' . $error_string
            );
        }

        return $response->singleUseTokenId;
    }

    /**
     * Create a token capture request to send to QuickStream
     * @param  Payment $event    subscription information
     * @return array
     */
    private function _transactionCaptureRequest(
        \MachinePack\Core\Event\Event $event,
        $tokenId,
        $customerNumber = null,
        $accountId = null
    ) {
        $metadata                   = array();
        $metadata["FirstName"]      = $event["Person.givenName"];
        $metadata["LastName"]       = $event["Person.familyName"];
        $metadata["CompanyName"]    = $event["company"] ?? null;
        $metadata["ContactAddress"] = $event["PostalAddress.streetAddress"];
        $metadata["Suburb"]         = $event["PostalAddress.addressLocality"];
        $metadata["State"]          = $event["PostalAddress.addressRegion"];
        $metadata["Postcode"]       = $event["PostalAddress.postalCode"];
        $metadata["Country"]        = $event["PostalAddress.addressCountry"];
        $metadata["ContactPhone"]   = $event["Person.telephone"];
        $metadata["Email"]          = $event["Person.email"];

        if ($event['Order.recurring'] === 'MONTHLY') {
            $metadata['PaymentFrequency']   = 'MONTHLY';
            $metadata['PreferredDebitDate'] = date("d/m/Y");
        }

        $url = $this->_config['apiBaseUrl'] . '/transactions';
        $txn = [
            'transactionType' => 'PAYMENT',
            'supplierBusinessCode' => $this->_config['SupplierBusinessCode'],
            'principalAmount' => $event['MonetaryAmount.value'],
            'currency' => $event['MonetaryAmount.currency'],
            'ipAddress' => $event['Person.ip'],
            'metadata' => $metadata
        ];

        if ($tokenId) {
            $txn['singleUseTokenId'] = $tokenId;
            $txn['eci']              = 'INTERNET';
        }

        if ($customerNumber) {
            $txn['customerReferenceNumber'] = $customerNumber;
        }

        if ($accountId) {
            $txn['accountToken'] = $accountId;
            $txn['eci']          = 'PHONE';
        }
        $response = $this->_callAPI('POST', $url, $txn);

        return $response;
    }

    /**
     * Create a token capture request to send to QuickStream
     * @param  Payment $event    subscription information
     * @return array
     */
    private function _accountCaptureRequest(\MachinePack\Core\Event\Event $event, $customerId)
    {
        $url = $this->_config['apiBaseUrl'] . '/customers/'.$customerId.'/accounts';
        $txn = [
            'creditCard' => [
                'cardholderName' => $event['CreditCard/CardDetails.name'],
                'cardNumber' => $event['CreditCard/CardDetails.number'],
                'expiryDateMonth' => $event['CreditCard/CardDetails.expiryMonth'],
                'expiryDateYear' =>$event['CreditCard/CardDetails.expiryYear']
            ]
        ];

        $response = $this->_callAPI('POST', $url, $txn);

        if (!empty($response->errors)) {
            $error_string = '';
            foreach ($response->errors as $error) {
                $error_string .= $error->messages[0] . '. ';
            }

            return new Failure(
                'Error - ' . $error_string
            );
        }

        return $response->accountToken;
    }

    /**
     * Create a token capture request to send to QuickStream
     * @param  Payment $event    subscription information
     * @return array
     */
    private function _accountSearchRequest(\MachinePack\Core\Event\Event $event, $customerId)
    {
        $url      = $this->_config['apiBaseUrl'] . '/customers/'.$customerId.'/accounts';
        $response = $this->_callAPI('GET', $url);
        if (empty($response->data)) {
            return null;
        }

        return $response->data[0]->accountToken;
    }

    /**
     * Create a token capture request to send to QuickStream
     * @param  Payment $event    subscription information
     * @return array
     */
    private function _recurringCaptureRequest(\MachinePack\Core\Event\Event $event, $customerId, $accountId)
    {
        $url = $this->_config['apiBaseUrl'] . '/customers/'.$customerId.'/recurring-payments';
        $txn = [
            'accountToken' => $accountId,
            'scheduleType' => 'CONTINUE_UNTIL_FURTHER_NOTICE',
            'frequency' => $event['Order.recurring'],
            'nextPaymentDate' => date('Y-m-d'),
            'supplierBusinessCode' => $this->_config['SupplierBusinessCode'],
            'instalmentAmount' => $event['MonetaryAmount.value'],
            'currency' => $event['MonetaryAmount.currency'],
        ];

        $response = $this->_callAPI('POST', $url, $txn);

        return $response;
    }

    /**
     * Call the API via curl
     *
     * @param string $method POST|GET|PUT
     * @param string $url
     * @param array $data
     * @return void
     */
    private function _callAPI($method, $url, $data = array(), $isToken = false)
    {
        $auth = $this->_config['SecretAPIKey'];
        if ($isToken) {
            $auth = $this->_config['PublishableAPIKey'];
        }

        $headers = array(
            "Authorization: Basic " . base64_encode($auth)
        );

        if ($method !== 'GET') {
            $headers[] = "Content-Type: application/json";
        }

        $data_json = '';
        if (!empty($data)) {
            $data_json = json_encode($data);
        }

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSLVERSION, 0);

        if ($method == "POST") {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data_json);
        } elseif ($method == "PUT") {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
        }

        $rawResponse = curl_exec($curl);

        if (curl_error($curl)) {
            $error_msg = curl_error($curl);
        }

        curl_close($curl);

        if (isset($error_msg)) {
            throw new \Exception($error_msg);
        }

        $response = json_decode($rawResponse);

        if ($response === null && json_last_error() !== JSON_ERROR_NONE) {
            throw new \Exception('Could not parse JSON response');
        }

        return $response;
    }
}
