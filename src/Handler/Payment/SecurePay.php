<?php
namespace MachinePack\Core\Handler\Payment;

use MachinePack\Core\MachinePack;
use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Event\Events\Subscription;

class SecurePay extends Handler
{
    private $_config;
    private $_token;

    public function handleEvent(\MachinePack\Core\Event\Event $event): Result
    {
        if (!$event instanceof Payment) {
            return new Ignored;
        }

        //load config from settings
        $this->_config = $this->settings[$this->settings['env']];

        //validate configuration variables
        if ((empty($this->_config['apiBaseUrl'])
            || empty($this->_config['authUrl'])
            || empty($this->_config['clientid'])
            || empty($this->_config['merchantid'])
            || empty($this->_config['secret'])
            || !array_key_exists('isProductionMode', $this->_config))
            && (empty($event['Intangible/SecurePay.auth_url'])
            || empty($event['Intangible/SecurePay.merchant_code'])
            || empty($event['Intangible/SecurePay.client_id'])
            || empty($event['Intangible/SecurePay.client_secret'])
            || empty($event['Intangible/SecurePay.ApiUrl']) )
        ) {
            MachinePack::log('Please add handler settings for SecurePay.', 'debug');
            return new Failure(
                'Please add handler settings for SecurePay. Full config should be:
                    {
                        config: {
                            env: <someenv>
                            <someenv>: {
                                apiBaseUrl: "..."
                                authUrl: "..."
                                merchantid: "..."
                                clientid: "..."
                                secret: "..."
                                isProductionMode: "..."
                            }
                        }
                    }
                '
            );
        }

        if (empty($event['Intangible/SecurePay.Token'])) {
            return new Failure(
                'No Payment Token provided. Please make sure you generate the Payment token from the JavaScript SDK.'
            );
        }

        if (empty($event['Intangible/SecurePay.access_token'])) {
            $this->_token = $this->_generateAccessToken($event);
        } else {
            $this->_token = $event['Intangible/SecurePay.access_token'];
        }

        if ($event instanceof Subscription) {
            return $this->_processPayment($event, true);
        }

        return $this->_processPayment($event);
    }

    private function _generateAccessToken(Payment $event)
    {
        $client_id = $event['Intangible/SecurePay.client_id'] ?? $this->_config['clientid'];
        $secret    = $event['Intangible/SecurePay.client_secret'] ?? $this->_config['secret'];
        $url       = $event['Intangible/SecurePay.auth_url'] ??
                        'https://welcome.api2.sandbox.auspost.com.au/oauth/token';

        $headers = array(
            'Authorization: Basic ' . base64_encode($client_id . ':' . $secret),
            'Content-Type' => 'application/x-www-form-urlencoded'
        );
        $data    = http_build_query(
            array(
            'grant_type' => 'client_credentials',
            'audience' => 'https://api.payments.auspost.com.au',
            )
        );

        $response = $this->_curl($headers, $url, $data);
        return  $response->access_token;
    }
    /**
     * Handle a payment request (recurring or one off)
     * @param  Payment $event       payment information
     * @param  boolean $recurring   recurring payment flag
     * @return Success|Failure
     */
    private function _processPayment(Payment $event, $recurreing = false)
    {
        try {
            $customerCode = $this->_generatePaymentInstrument($event);
            if ($customerCode instanceof Failure) {
                $this->_token = $this->_generateAccessToken($event);
                $customerCode = $this->_generatePaymentInstrument($event);
            }

            $url     = ($event['Intangible/SecurePay.ApiUrl'] ?? $this->_config['apiBaseUrl']) . '/v2/payments';
            $headers = array(
                'Content-Type' => 'Content-Type: application/json',
                'Authorization: Bearer ' . $this->_token,
            );

            //NOTE: If no email is provided then the token will expire in 30 minutes.
            //https://auspost.com.au/payments/docs/securepay/?javascript#securepay-api-card-payments-rest-api-create-payment
            $paymentData = array(
                "merchantCode"  => $event['Intangible/SecurePay.merchant_code'] ?? $this->_config['merchantid'],
                "token"         => $event['Intangible/SecurePay.Token'],
                "ip"            => $event['Person.ip'] ?? "127.0.0.1",
                "amount"        => $event['MonetaryAmount.value'] * 100,
                "currency"      => $event['MonetaryAmount.currency'] ?? "AUD",
                "orderId"       => $event['Order.identifier'] ?? uniqid(),
            );

            if (!($customerCode instanceof Failure)) {
                $paymentData["customerCode"] = $customerCode;
            } else {
                return new Failure('Unable to generate payment instrument');
            }

            $response = $this->_curl($headers, $url, json_encode($paymentData));
            if (isset($response->status) && $response->status === 'paid') {
                return new Success(
                    [
                        'MoneyTransfer.identifier'                             => $response->bankTransactionId,
                        'Intangible/APIResponse.APIResponseCode'               => $response->gatewayResponseCode,
                        'Intangible/APIResponse.APIResponseMessage'            => $response->gatewayResponseMessage,
                        'Intangible/APIResponse.OrderId'                       => $response->orderId,
                        'Intangible/APIResponse.AccessToken'                   => $this->_token,
                        'Intangible/APIResponse.CustomerCode'                  => $response->customerCode,
                    ]
                );
            } else {
                return new Failure('ERROR: Unable to process payment', $response);
            }
        } catch (\Exception $e) {
            return new Failure(
                $e->getMessage()
            );
        }
    }
    private function _generatePaymentInstrument(Payment $event)
    {
        $customerCode = $event['Intangible/SecurePay.customer_code'] ?? uniqid();
        $url          = ($event['Intangible/SecurePay.ApiUrl'] ??
                        $this->_config['apiBaseUrl']) . '/v2/customers/'.$customerCode.'/payment-instruments/token';

        $headers = array(
            'Content-Type: Content-Type: application/json',
            'token: '.$event['Intangible/SecurePay.Token'],
            'ip: ' . $event['Person.ip'],
            'Authorization: Bearer ' . $this->_token,
        );
        
        $response = $this->_curl($headers, $url);
        return $response->customerCode ?? new Failure('ERROR: Unable to create token.', $response);
    }

    private function _curl($headers, $url, $data = array())
    {
        $ch = curl_init($url);
        if (false === $ch) {
            throw new \Exception('Unable to initialise curl.');
        }

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);

        if (!empty($data)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }

        // converting
        $response = curl_exec($ch);
        curl_close($ch);

        if (false === $response) {
            throw new \Exception(
                sprintf(
                    'Payment system appears down. Errno %s (%s).',
                    curl_errno($ch),
                    curl_error($ch)
                )
            );
        }

        return json_decode($response);
    }
}
