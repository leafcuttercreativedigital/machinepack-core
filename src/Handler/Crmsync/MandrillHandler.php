<?php
namespace MachinePack\Core\Handler\Crmsync;

use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Event\Events\Crmsync;
use Mandrill;

class MandrillHandler extends Handler
{
    private $_config;

    public function handleEvent(\MachinePack\Core\Event\Event $event): Result
    {
        if (! $event instanceof Crmsync) {
            return new Ignored;
        }

        //load config from settings
        $this->_config = $this->settings[$this->settings['env']];

        //Api credentials can be either passed as args or loaded from config
        if (empty($this->_config['apiKey'])
            && empty($event['Intangible/Mandrill.ApiKey'])
        ) {
            //validate configuration variables
            return new Failure(
                'Please provide apiKey or add settings for Mandrill. Full config should be:
                {
                    config: {
                        env: <someenv>,
                        <someenv>: {
                            apiKey:
                        }
                    }
                }
            '
            );
        }

        if (isset($event['Intangible/get_templates']) && $event['Intangible/get_templates']) {
            return  $this->_getTemplateList($event);
        }
        return $this->_sendTransactionalEmail($event);
    }

    /**
     * Submit the transaction email event payload to Mandrill
     * @param Crmsync $event
     */
    private function _getTemplateList(Crmsync $event)
    {
        $template_array = array();
        try {
            $mandrill  = new Mandrill($event['Intangible/Mandrill.ApiKey'] ?? $this->_config['apiKey']);
            $templates = $mandrill->templates->getList();
            foreach ($templates as $template) {
                $template_array[] = array(
                    'slug' => $template['slug'],
                    'template' => $template['name']
                );
            }
            return new Success($template_array);
        } catch (\Exception $e) {
            return new Failure('Error ::: ' . $e);
        }
    }

    /**
     * Submit the transaction email event payload to Mandrill
     * @param Crmsync $event
     */
    private function _sendTransactionalEmail(Crmsync $event)
    {
        try {
            $mandrill      = new Mandrill($event['Intangible/Mandrill.ApiKey'] ?? $this->_config['apiKey']);
            $template_name = $event['EmailMessage.template'];

            if (isset($event['Intangible/Event.payload'])
                && !is_array($event['Intangible/Event.payload'])
            ) {
                return new Failure('Error:: Message - variables must be in array.');
            }

            if (!is_array($event['EmailMessage.recipient.to'])) {
                return new Failure('Error:: EmailMessage.recipient.to - variables must be in array.');
            }

            $message = array(
                'subject' => $event['EmailMessage.subject'],
                'from_email' => $event['EmailMessage.sender.email'],
                'from_name' => $event['EmailMessage.sender.name'],
                'to' => $event['EmailMessage.recipient.to'],
                'merge' => true,
                'merge_language' => 'handlebars',
                'global_merge_vars' => $this->_formatData($event['Intangible/Event.payload'])
            );

            if (isset($event['EmailMessage.replyto']) && $event['EmailMessage.replyto']) {
                $message['headers']['Reply-To'] = $event['EmailMessage.replyto'];
            }

            if (isset($event['EmailMessage.cc']) && $event['EmailMessage.cc']) {
                $message['to'][] = array(
                    'email' => $event['EmailMessage.cc'],
                    'name'  => $event['EmailMessage.cc.name'],
                    'type'  => 'cc'
                );
            }

            if (isset($event['EmailMessage.bcc']) && $event['EmailMessage.bcc']) {
                $message['to'][] = array(
                    'email' => $event['EmailMessage.bcc'],
                    'name'  => $event['EmailMessage.bcc.name'],
                    'type'  => 'bcc'
                );
            }

            $template_content = null;
            $results          = $mandrill->messages->sendTemplate($template_name, $template_content, $message);

            foreach ($results as $result) {
                if (isset($result['status']) && $result['status'] !== 'sent') {
                    return new Failure('Error ::: ' . $result['status'] . ' - ' . $result['reject_reason']);
                }
            }

            return new Success('Email successfully sent.');
        } catch (\Exception $e) {
            return new Failure('Error ::: ' . $e);
        }
    }

    private function _formatData(array $data)
    {
        $return_data = [];
        foreach ($data as $key => $value) {
            $return_data[] = [ 'name' => $key, 'content' => $value ];
        }
        return $return_data;
    }
}
