<?php
namespace MachinePack\Core\Handler\Crmsync;

use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Event\Events\Crmsync;
use SoapClient;
use SoapHeader;

class SalesforceDynamic extends Handler
{
    private $_config;

    public function handleEvent(\MachinePack\Core\Event\Event $event): Result
    {
        if (! $event instanceof Crmsync) {
            return new Ignored;
        }

        //load config from settings
        $this->_config = $this->settings[$this->settings['env']];

        //Api credentials can be either passed as args or loaded from config
        if (empty($event['Intangible/Salesforce.clientSecret'])
            || empty($event['Intangible/Salesforce.clientId'])
            || empty($event['Intangible/Salesforce.username'])
            || empty($event['Intangible/Salesforce.password'])
            || empty($event['Intangible/Salesforce.apiUrl'])
        ) {
            //validate configuration variables
            if (empty($this->_config['clientSecret'])
                || empty($this->_config['clientId'])
                || empty($this->_config['username'])
                || empty($this->_config['password'])
                || empty($this->_config['apiUrl'])
            ) {
                return new Failure(
                    'Please add handler settings for Saleforce. Full config should be:
                    {
                        config: {
                            env: <someenv>,
                            <someenv>: {
                                clientSecret:
                                clientId:
                                username:
                                password:
                                apiUrl:
                            }
                        }
                    }
                '
                );
            }
        } else {
            $this->_config['clientSecret'] = $event['Intangible/Salesforce.clientSecret'];
            $this->_config['clientId']     = $event['Intangible/Salesforce.clientId'];
            $this->_config['username']     = $event['Intangible/Salesforce.username'];
            $this->_config['password']     = $event['Intangible/Salesforce.password'];
            $this->_config['apiUrl']       = $event['Intangible/Salesforce.apiUrl'];
            $this->_config['authUrl']      = $event['Intangible/Salesforce.authUrl'];
        }

        if (isset($event['query']) && $event['query']) {
            $record = $this->_submitToSalesforceQuery($event);
        } elseif (isset($event['Intangible/ApexRest']) && $event['Intangible/ApexRest']) {
            $record = $this->_submitToSalesForceApexRest($event);
            return $this->_handleApexRestResponse($event, $record);
        } else {
            $record = $this->_submitToSalesforce($event);
        }

        if ($record instanceof Failure) {
            return $record;
        }

        if (is_array($record)) {
            $error_code    = $record[0]->errorCode;
            $error_message = $record[0]->message;

            return new Failure(
                'Error:: Code - ' . $error_code . '. Message - ' . $error_message
            );
        } else {
            if (isset($record->id)) {
                return new Success(
                    [
                        'Intangible/Record.identifier' => $record->id
                    ]
                );
            }

            return new Success(
                [
                    'Intangible/Record' => $record
                ]
            );
        }
    }

    /**
     * Submit the event payload to salesforce
     * @param  Crmsync $event    payment information
     * @return record
     */
    private function _submitToSalesforce(Crmsync $event)
    {
        try {
            if (!empty($event['accessToken'])) {
                $json_token = $event['accessToken'];
            } else {
                $json_token = $this->_generateAccessToken();
            }

            if (isset($json_token->error)) {
                return new Failure(
                    $json_token->error . '(' . ($json_token->error_description ?? 'No Details') . ')'
                );
            } else {
                $token        = $json_token->access_token;
                $instance_url = $json_token->instance_url;

                if ($instance_url && $token) {
                    $url = '/services/data/v42.0/sobjects/' . $event["sobject"] . '/';

                    if (isset($event['id']) && $event['id']) {
                        $url .= $event['id'];
                    }

                    $full_url = $instance_url . $url;

                    $record = $this->_curl($event['method'], $full_url, $token, $event['Intangible/Event.payload']);
                    $record = json_decode($record);

                    return $record;
                } else {
                    return new Failure('Invalid Token or Instance URL');
                }
            }
        } catch (\Exception $e) {
            return new Failure(
                $e->getMessage()
            );
            //TODO should log stacktrace
        }
    }

    /**
     * Submit the event payload to salesforce as a query
     * @param  Crmsync $event    payment information
     * @return record
     */
    private function _submitToSalesforceQuery(Crmsync $event)
    {
        try {
            if (!empty($event['accessToken'])) {
                $json_token = $event['accessToken'];
            } else {
                $json_token = $this->_generateAccessToken();
            }

            if (isset($json_token->error)) {
                return new Failure(
                    $json_token->error . '(' . ($json_token->error_description ?? 'No Details') . ')'
                );
            } else {
                $token        = $json_token->access_token;
                $instance_url = $json_token->instance_url;

                if ($instance_url && $token) {
                    $url = '/services/data/v42.0/query/?q=';

                    if (isset($event['query']) && $event['query']) {
                        $url .= $event['query'];
                    }

                    $full_url = $instance_url . $url;

                    $record = $this->_curl($event['method'], $full_url, $token, $event['Intangible/Event.payload']);
                    $record = json_decode($record);

                    return $record;
                } else {
                    return new Failure('Invalid Token or Instance URL');
                }
            }
        } catch (\Exception $e) {
            return new Failure(
                $e->getMessage()
            );
            //TODO should log stacktrace
        }
    }

    /**
     * Submit the event payload to salesforce apex rest endpoint
     * @param Crmsync $event    payload
     * @return record
     */
    private function _submitToSalesForceApexRest(Crmsync $event)
    {
        try {
            if (!isset($event['Intangible/Event.name']) || empty($event['Intangible/Event.name'])) {
                return new Failure(
                    'No valid event name provided.'
                );
            }

            if ($event['Intangible/Event.name'] === 'payments2us') {
                $headers = array(
                    "Content-Type: application/vnd.api+json",
                    "Payments2Us-Key: " . $event["Intangible/Payments2Us.webhook_secret"]
                );

                if (!empty($event['Intangible/Payments2Us.Auth'])) {
                    $json_token = $this->_generateAccessToken();

                    if (isset($json_token->error)) {
                        return new Failure('Payment2Us :: Error while generating access token');
                    } else {
                        $token     = $json_token->access_token;
                        $headers[] = "Authorization: Bearer " . $token;
                    }
                }

                $url = $this->_config['apiUrl']
                    . '/services/apexrest/'
                    . $event["Intangible/Payments2Us.service_id"]
                    .'/v1/wh/PaymentComplete/'
                    . $event["Intangible/Payments2Us.form_id"];

                $record = $this->_payment2UsCurl(
                    $event['Intangible/Event.method'],
                    $url,
                    $headers,
                    $event['Intangible/Event.payload']
                );

                return json_decode($record);
            } else {
                if (!empty($event['Intangible/Event.accessToken'])) {
                    $json_token = $event['Intangible/Event.accessToken'];
                } else {
                    $json_token = $this->_generateAccessToken();
                }

                if (isset($json_token->error)) {
                    return new Failure(
                        $json_token->error . '(' . ($json_token->error_description ?? 'No Details') . ')'
                    );
                } else {
                    $token        = $json_token->access_token;
                    $instance_url = $json_token->instance_url;

                    if ($instance_url && $token) {
                        if ($event['Intangible/Event.name'] === 'npsp_plus_websiteIntegration') {
                            $url = '/services/apexrest/npsp_plus/Flow?setting=websiteIntegration';
                        } else {
                            $url = '/services/apexrest/Flow?event=' . $event['Intangible/Event.name'];
                        }

                        if (isset($event['query']) && $event['query']) {
                            $url .= $event['query'];
                        }

                        $full_url = $instance_url . $url;

                        $record = $this->_curl(
                            $event['Intangible/Event.method'],
                            $full_url,
                            $token,
                            $event['Intangible/Event.payload']
                        );
                        $record = json_decode($record);

                        return $record;
                    } else {
                        return new Failure('Invalid Token or Instance URL');
                    }
                }
            }
        } catch (\Exception $e) {
            return new Failure(
                $e->getMessage()
            );
            //TODO should log stacktrace
        }
    }

    /**
     * Handle the Apex Rest response based on the provided event
     * @param $result
     */
    private function _handleApexRestResponse($event, $result)
    {
        switch ($event['Intangible/Event.name']) {
            case 'oneOffDonation':
            case 'guardianAngel':
                if (isset($result->contactId) && isset($result->donationId)) {
                    return new Success(
                        [
                            'Intangible/Record.contactId' => $result->contactId,
                            'Intangible/Record.donationId' => $result->donationId,
                            'Intangible/Record.organisationId' => $result->organisationId ?? null
                        ]
                    );
                } else {
                    return new Failure(
                        'Error:: Invalid oneOffDonation response: ' . json_encode($result)
                    );
                }
            case 'regularGivingDonation':
            case 'guardianAngelRecurring':
                if (isset($result->contactId) && isset($result->donationId)) {
                    return new Success(
                        [
                            'Intangible/Record.contactId' => $result->contactId,
                            'Intangible/Record.donationId' => $result->donationId
                        ]
                    );
                } else {
                    return new Failure(
                        'Error:: Invalid regularGivingDonation response: ' . json_encode($result)
                    );
                }
            case 'createGift':
                if (isset($result->giftId)) {
                    return new Success(
                        [
                            'Intangible/Record.giftId' => $result->giftId
                        ]
                    );
                } else {
                    return new Failure(
                        'Error:: Invalid createGift response: ' . json_encode($result)
                    );
                }
            case 'membershipPayment':
                if (isset($result->membershipId)) {
                    return new Success(
                        [
                            'Intangible/Record.membershipId' => $result->membershipId
                        ]
                    );
                } else {
                    return new Failure(
                        'Error:: Invalid membershipPayment response: ' . json_encode($result)
                    );
                }
            case 'payments2us':
                if (isset($result->PaymentTxnId)) {
                    return new Success(
                        [
                            'Intangible/Record.PaymentTxnId' => $result->PaymentTxnId
                        ]
                    );
                } else {
                    return new Failure(
                        'Error:: Invalid payments2us response: ' . json_encode($result)
                    );
                }
            case 'npsp_plus_websiteIntegration':
            case 'websiteIntegration':
                if (isset($result->contactId)
                    && (isset($result->donationId) || isset($result->recurringId) || isset($result->giftId))
                ) {
                    return new Success(
                        [
                            'Intangible/Record.contactId' => $result->contactId ?? '',
                            'Intangible/Record.donationId' => $result->donationId ?? '',
                            'Intangible/Record.giftId' => $result->giftId ?? '',
                            'Intangible/Record.ticketId' => $result->ticketId ?? '',
                            'Intangible/Record.receiptNumber' => $result->raceiptNumber ?? '',
                            'Intangible/Record.recurringId' => $result->recurringId ?? '',
                            'Intangible/Record.parentOpportunityId' => $result->parentOpportunityId ?? '',
                        ]
                    );
                } else {
                    return new Failure(
                        'Error:: Invalid websiteIntegration response: ' . json_encode($result)
                    );
                }
            default:
                return new Failure(
                    'Error:: Invalid response or unrecognized event name. Response: ' . json_encode($result)
                );
        }
    }

    /**
     * Generate an access token for Salesforce
     *
     * @return array $response
     */
    private function _generateAccessToken()
    {
        try {
            $params = array(
                "grant_type"    => "password",
                "client_secret" => $this->_config['clientSecret'],
                "client_id"     => $this->_config['clientId'],
                "username"      => $this->_config['username'],
                "password"      => $this->_config['password']
            );

            $json_response = $this->_curl('POST', '', false, $params);
            return json_decode($json_response);
        } catch (\Exception $ex) {
            $exceptionMsg = $ex->getMessage();
            $returnArray  = array('Errors' => true, 'message' => $exceptionMsg);
            return json_encode($returnArray);
        }
    }

    /**
     * Make a curl request
     *
     * @param string $method    GET|POST
     * @param string $url       the url to call
     * @param string $bearer    the bearer token for authentication
     * @param array $data       request payload
     * @return void
     */
    private function _curl($method, $url, $bearer, $data = array())
    {
        try {
            $headers    = array();
            $input_data = '';

            if ($bearer) {
                $headers    = array(
                    "Content-Type: application/json",
                    "Authorization: Bearer " . $bearer
                );
                $input_data = json_encode($data);
            } else {
                $params = http_build_query($data);
                $apiurl = !empty($this->_config['authUrl']) ? $this->_config['authUrl'] : $this->_config['apiUrl'];
                $url    = $apiurl . "/services/oauth2/token?" . $params;
            }
            $curl = curl_init();

            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

            if ($method == 'POST') {
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $input_data);
            } elseif ($method == 'PATCH') {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
                curl_setopt($curl, CURLOPT_POSTFIELDS, $input_data);
            } elseif ($method == 'PUT') {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
            } elseif ($method == 'DELETE') {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
            } else {
                curl_setopt($curl, CURLOPT_HEADER, 0);
            }

            $result = curl_exec($curl);
            curl_close($curl);

            return $result;
        } catch (Exception $ex) {
            $exceptionMsg = $ex->getMessage();
            $returnArray  = array('Errors' => true, 'message' => $exceptionMsg);
            return json_encode($returnArray);
        }
    }

    /**
     * @param $method
     * @param $url
     * @param $data
     * @return void
     */
    private function _payment2UsCurl($method, $url, $headers, $data = array())
    {
        try {
            $input_data = json_encode($data);
            $curl       = curl_init();

            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

            if ($method == 'POST') {
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $input_data);
                curl_setopt($curl, CURLINFO_HEADER_OUT, true);
            } elseif ($method == 'PATCH') {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
                curl_setopt($curl, CURLOPT_POSTFIELDS, $input_data);
            } elseif ($method == 'PUT') {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
            } elseif ($method == 'DELETE') {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
            } else {
                curl_setopt($curl, CURLOPT_HEADER, 0);
            }

            $result = curl_exec($curl);
            curl_close($curl);

            return $result;
        } catch (Exception $ex) {
            $exceptionMsg = $ex->getMessage();
            $returnArray  = array('Errors' => true, 'message' => $exceptionMsg);
            return json_encode($returnArray);
        }
    }
}
