<?php
namespace MachinePack\Core\Handler\Crmsync;

use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Event\Events\Crmsync;
use SoapClient;
use SoapHeader;

class Salesforce extends Handler
{
    private $_config;

    public function handleEvent(\MachinePack\Core\Event\Event $event): Result
    {
        if (! $event instanceof Crmsync) {
            return new Ignored;
        }

        //load config from settings
        $this->_config = $this->settings[$this->settings['env']];

        //Api credentials can be either passed as args or loaded from config
        if (empty($event['Intangible/Salesforce.clientSecret'])
            || empty($event['Intangible/Salesforce.clientId'])
            || empty($event['Intangible/Salesforce.username'])
            || empty($event['Intangible/Salesforce.password'])
            || empty($event['Intangible/Salesforce.apiUrl'])
        ) {
            //validate configuration variables
            if (empty($this->_config['clientSecret'])
                || empty($this->_config['clientId'])
                || empty($this->_config['username'])
                || empty($this->_config['password'])
                || empty($this->_config['apiUrl'])
            ) {
                return new Failure(
                    'Please add handler settings for Saleforce. Full config should be:
                    {
                        config: {
                            env: <someenv>,
                            <someenv>: {
                                clientSecret:
                                clientId:
                                username:
                                password:
                                apiUrl:
                            }
                        }
                    }
                '
                );
            }
        } else {
            $this->_config['clientSecret'] = $event['Intangible/Salesforce.clientSecret'];
            $this->_config['clientId']     = $event['Intangible/Salesforce.clientId'];
            $this->_config['username']     = $event['Intangible/Salesforce.username'];
            $this->_config['password']     = $event['Intangible/Salesforce.password'];
            $this->_config['apiUrl']       = $event['Intangible/Salesforce.apiUrl'];
        }

        return $this->_submitToSalesforce($event);
    }

    /**
     * Submit the event payload to salesforce
     * @param  Crmsync $event    payment information
     * @return record
     */
    private function _submitToSalesforce(Crmsync $event)
    {
        try {
            if (isset($event['action'])) {
                if ($event['action'] == 'getContactById') {
                    $contact = $this->_getObjectById('Contact', $event['id']);
                    if ($contact instanceof Success && $contact) {
                        return $contact;
                    } else {
                        return new Failure('No valid contact found');
                    }
                }
                if ($event['action'] == 'getObjectById') {
                    $object = $this->_getObjectById($event['object'], $event['id']);
                    if ($object instanceof Success && $object) {
                        return $object;
                    } else {
                        return new Failure('No valid contact found');
                    }
                }
                if ($event['action'] == 'getObjectFields') {
                    $fields = $this->_getObjectFields($event['object'], $event['field'] ?? false);
                    if ($fields instanceof Success) {
                        return $fields;
                    } else {
                        return new Failure('No valid object found');
                    }
                }
            }

            $existing_contact = null;
            $contact          = array();
            $record           = array();
            $payment_record   = array();
            $contact_id       = '';

            if (isset($event['search'])) {
                $existing_contact = $this->_submitToSalesforceQuery($event['search']);
                if (isset($existing_contact->totalSize) && $existing_contact->totalSize >= 1) {
                    $contact_id = $existing_contact->records[0]->Id;
                }
            }

            if (isset($event['createContact'])) {
                if (!$contact_id) {
                    $contact = $this->_createContact($event['createContact']);
                    if (is_array($contact)) {
                        return new Failure(
                            'Failed to create Contact',
                            [
                                'errorCode' => $contact[0]->errorCode,
                                'message' => $contact[0]->message
                            ]
                        );
                    }
                    $contact_id = $contact->id;
                }

                $contact = $this->_getObjectById('Contact', $contact_id);
                if ($contact instanceof Success && $contact) {
                    $contact_info = array(
                        'npsp__Primary_Contact__c' => $contact->data->Id,
                        'AccountID' => $contact->data->AccountId
                    );
                } else {
                    if ($contact instanceof Failure) {
                        throw new \Exception($contact->reason);
                    } else {
                        throw new \Exception('Invalid Contact.');
                    }
                }
            } else {
                return new Failure(
                    'Please ensure that the createContact key and array has been provided.'
                );
            }

            foreach ($event->getData() as $key => $opportunity) {
                if (strpos($key, 'createOpportunity') !== false) {
                    $record = $this->_createOpportunity($opportunity, $contact_info);

                    if ($record instanceof Failure) {
                        throw new \Exception($record->reason);
                    }

                    if (is_array($record)) {
                        return new Failure(
                            'Failed to create Opportunity',
                            [
                                'errorCode' => $record[0]->errorCode,
                                'message' => $record[0]->message
                            ]
                        );
                    }
                    $record_id = $record->id;
                    $payment_record['Intangible/' . $opportunity['sobject']] = $record_id;
                }

                if (strpos($key, 'updateOpportunityPayment') !== false && !empty($record_id)) {
                    $oppPayment = $this->_getOpportunityPayment($record_id);
                    if (isset($oppPayment->totalSize)
                        && $oppPayment->totalSize >= 1
                        && !empty($oppPayment->records[0]->Id)
                    ) {
                        $oppPaymentId = $oppPayment->records[0]->Id;
                    } else {
                        $oppPaymentId = null;
                    }

                    $oppPayment_info = array(
                        'npe01__Opportunity__c' => $record_id,
                    );

                    $record = $this->_updateOpportunityPayment($opportunity, $oppPayment_info, $oppPaymentId);
                    if ($record instanceof Failure) {
                        throw new \Exception($record->reason);
                    }

                    if (is_array($record)) {
                        return new Failure(
                            'Failed to update Opportunity Payment',
                            [
                                'errorCode' => $record[0]->errorCode,
                                'message' => $record[0]->message
                            ]
                        );
                    }
                }
                if (strpos($key, 'createRecurring') !== false) {
                    $contact_information = array(
                        'npe03__Contact__c' => $contact->data->Id,
                    );
                    $record              = $this->_createRecurring($opportunity, $contact_information);
                    if ($record instanceof Failure) {
                        throw new \Exception($record->reason);
                    }

                    if (is_array($record)) {
                        return new Failure(
                            'Failed to create Recurring Object',
                            [
                                'errorCode' => $record[0]->errorCode,
                                'message' => $record[0]->message
                            ]
                        );
                    }
                    $record_id = $record->id;
                    $payment_record['Intangible/' . $opportunity['sobject']] = $record_id;
                }
            }

            $return_array = array(
                'Person.identifier' => $contact->data->Id,
                'Account.identifier' => $contact->data->AccountId,
            );

            return new Success(
                array_merge($return_array, $payment_record)
            );
        } catch (\Exception $e) {
            return new Failure(
                $e->getMessage()
            );
        }
    }

    private function _createContact($event)
    {
        try {
            $json_token   = $this->_generateAccessToken();
            $token        = $json_token->access_token;
            $instance_url = $json_token->instance_url;
            $method       = $event['method'];
            $url          = '/services/data/v42.0/sobjects/Contact/';

            if ($instance_url && $token) {
                $full_url = $instance_url . $url;
                $response = $this->_curl($method, $full_url, $token, $event['Intangible/Event.payload']);
                $record   = json_decode($response);
                return $record;
            }

            return new Failure(
                'Access token invalid'
            );
        } catch (\Exception $e) {
            return new Failure(
                $e->getMessage()
            );
            //TODO should log stacktrace
        }
    }

    private function _getObjectById($object, $id)
    {
        try {
            $json_token   = $this->_generateAccessToken();
            $token        = $json_token->access_token;
            $instance_url = $json_token->instance_url;
            $method       = 'GET';
            $url          = '/services/data/v42.0/sobjects/' . $object . '/' . $id;

            if ($instance_url && $token) {
                $full_url = $instance_url . $url;
                $record   = $this->_curl($method, $full_url, $token);
                $record   = json_decode($record);

                if ($record && !empty($record->Id)) {
                    return new Success($record);
                } else {
                    return new Failure('Invalid Object or ID');
                }
            }

            return new Failure(
                'Access token invalid'
            );
        } catch (\Exception $e) {
            return new Failure(
                $e->getMessage()
            );
            //TODO should log stacktrace
        }
    }

    private function _getObjectFields($object, $field = false)
    {
        try {
            $json_token   = $this->_generateAccessToken();
            $token        = $json_token->access_token;
            $instance_url = $json_token->instance_url;
            $method       = 'GET';
            $url          = '/services/data/v62.0/sobjects/' . $object . '/describe';

            if ($instance_url && $token) {
                $full_url = $instance_url . $url;
                $fields   = $this->_curl($method, $full_url, $token);
                $fields   = json_decode($fields);
                if (is_array($fields) && is_object($fields[0])) {
                    $error = $fields[0]->errorCode ?? '';
                }

                if (!empty($error)) {
                    return new Failure($error);
                }

                if (!empty($field)) {
                    foreach ($fields->fields as $fieldrecord) {
                        if ($fieldrecord->name == $field) {
                            return new Success($fieldrecord);
                        }
                    }
                }

                return new Success($fields);
            }

            return new Failure(
                'Access token invalid'
            );
        } catch (\Exception $e) {
            return new Failure(
                $e->getMessage()
            );
        }
    }

    /**
     * Submit the event payload to salesforce
     * @param  Crmsync $event    payment information
     * @return record
     */
    private function _createOpportunity($event, $contact_info)
    {
        try {
            $json_token   = $this->_generateAccessToken();
            $token        = $json_token->access_token;
            $instance_url = $json_token->instance_url;
            $method       = $event['method'];

            if ($instance_url && $token) {
                $url = '/services/data/v42.0/sobjects/Opportunity/';

                if (isset($event['id']) && $event['id']) {
                    $url   .= $event['id'];
                    $method = 'GET';
                }

                $full_url = $instance_url . $url;

                $event = array_merge($contact_info, $event['Intangible/Event.payload']);

                $record = $this->_curl($method, $full_url, $token, $event);
                $record = json_decode($record);

                return $record;
            }

            return new Failure(
                'Access token invalid'
            );
        } catch (\Exception $e) {
            return new Failure(
                $e->getMessage()
            );
            //TODO should log stacktrace
        }
    }
    private function _updateOpportunityPayment($event, $oppPayment_info, $oppPaymentId)
    {
        try {
            $json_token   = $this->_generateAccessToken();
            $token        = $json_token->access_token;
            $instance_url = $json_token->instance_url;
           

            if ($instance_url && $token) {
                $url = '/services/data/v48.0/sobjects/npe01__OppPayment__c/';
               
                if (isset($oppPaymentId) && !empty($oppPaymentId)) {
                    $url   .= $oppPaymentId;
                    $method = 'PATCH';
                    $event  =  $event['Intangible/Event.payload'];
                } else {
                    $method = 'POST';
                    $event  = array_merge($oppPayment_info, $event['Intangible/Event.payload']);
                }

                $full_url = $instance_url . $url;
               
                $record = $this->_curl($method, $full_url, $token, $event);
                $record = json_decode($record);

                return $record;
            }

            return new Failure(
                'Access token invalid'
            );
        } catch (\Exception $e) {
            return new Failure(
                $e->getMessage()
            );
            //TODO should log stacktrace
        }
    }

    private function _createRecurring($event, $contact_info)
    {
        try {
            $json_token   = $this->_generateAccessToken();
            $token        = $json_token->access_token;
            $instance_url = $json_token->instance_url;
           

            if ($instance_url && $token) {
                $url      = '/services/data/v42.0/sobjects/npe03__Recurring_Donation__c/';
                $method   = 'POST';
                $full_url = $instance_url . $url;
                $event    = array_merge($contact_info, $event['Intangible/Event.payload']);
                $record   = $this->_curl($method, $full_url, $token, $event);
                $record   = json_decode($record);

                return $record;
            }

            return new Failure(
                'Access token invalid'
            );
        } catch (\Exception $e) {
            return new Failure(
                $e->getMessage()
            );
            //TODO should log stacktrace
        }
    }

    /**
     * Submit the event payload to salesforce as a query
     * @param  Crmsync $event    payment information
     * @return record
     */
    private function _submitToSalesforceQuery($event)
    {
        try {
            $json_token   = $this->_generateAccessToken();
            $token        = $json_token->access_token;
            $instance_url = $json_token->instance_url;

            if ($instance_url && $token) {
                $url = '/services/data/v42.0/query/?q=';

                if (isset($event['query']) && $event['query']) {
                    $url .= $event['query'];
                }

                $full_url = $instance_url . $url;

                $record = $this->_curl($event['method'], $full_url, $token, array());
                $record = json_decode($record);

                return $record;
            }

            return new Failure(
                'Access token invalid'
            );
        } catch (\Exception $e) {
            return new Failure(
                $e->getMessage()
            );
            //TODO should log stacktrace
        }
    }

    private function _getOpportunityPayment($opportunityId)
    {
        $event           = array();
        $event['method'] = 'GET';
        $event['query']  = "Select+Id+From+npe01__OppPayment__c+Where+npe01__Opportunity__c='" . $opportunityId ."'";
        $event['Intangible/Event.payload'] = array();

        return $this->_submitToSalesforceQuery($event);
    }

    /**
     * Generate an access token for Salesforce
     *
     * @return array $response
     */
    private function _generateAccessToken()
    {
        try {
            $params = array(
                "grant_type"    => "password",
                "client_secret" => $this->_config['clientSecret'],
                "client_id"     => $this->_config['clientId'],
                "username"      => $this->_config['username'],
                "password"      => $this->_config['password']
            );

            $json_response = $this->_curl('POST', '', false, $params);
            return json_decode($json_response);
        } catch (\Exception $ex) {
            $exceptionMsg = $ex->getMessage();
            $returnArray  = array('Errors' => true, 'message' => $exceptionMsg);
            return json_encode($returnArray);
        }
    }

    /**
     * Make a curl request
     *
     * @param string $method    GET|POST
     * @param string $url       the url to call
     * @param string $bearer    the bearer token for authentication
     * @param array $data       request payload
     * @return void
     */
    private function _curl($method, $url, $bearer, $data = array())
    {
        try {
            $headers    = array();
            $input_data = '';

            if ($bearer) {
                $headers    = array(
                    "Content-Type: application/json",
                    "Authorization: Bearer " . $bearer
                );
                $input_data = json_encode($data);
            } else {
                $params = http_build_query($data);
                $url    = $this->_config['apiUrl']."/services/oauth2/token?" . $params;
            }

            $curl = curl_init();

            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

            if ($method == 'POST') {
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $input_data);
            } elseif ($method == 'PATCH') {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
                curl_setopt($curl, CURLOPT_POSTFIELDS, $input_data);
            } elseif ($method == 'PUT') {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
            } elseif ($method == 'DELETE') {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
            } else {
                curl_setopt($curl, CURLOPT_HEADER, 0);
            }

            $result = curl_exec($curl);
            curl_close($curl);

            return $result;
        } catch (Exception $ex) {
            $exceptionMsg = $ex->getMessage();
            $returnArray  = array('Errors' => true, 'message' => $exceptionMsg);
            return json_encode($returnArray);
        }
    }
}
