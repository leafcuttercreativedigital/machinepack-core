<?php
namespace MachinePack\Core\Handler\Crmsync;

use Braintree\Exception;
use MachinePack\Core\Handler\Handler;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Event\Events\Crmsync;

class SkyRFDSQLD extends Handler
{
    private $_config;
    private $_previous_preferred_address;

    public function handleEvent(\MachinePack\Core\Event\Event $event): Result
    {
        if (! $event instanceof Crmsync) {
            return new Ignored;
        }

        //Api credentials can be either passed as args or loaded from config
        //validate configuration variables
        try {
            $this->_fetchValidateConfig($event);
        } catch (\Exception $e) {
            return new Failure($e->getMessage());
        }

        if (isset($event['Intangible/Sky.action'])) {
            switch ($event['Intangible/Sky.action']) {
                case 'batch_lookup':
                    return $this->_batchLookup($event);
                case 'create_batch':
                    return $this->_createBatch($event);
                case 'delete_batch':
                    return $this->_deleteBatch($event);
                case 'get_name_formats':
                    return $this->_getNameFormats($event);
                case 'get_campaigns':
                    return $this->_getCampaigns($event);
                case 'get_funds':
                    return $this->_getFunds($event);
                case 'get_appeals':
                    return $this->_getAppeal($event);
                case 'get_gift_subtypes':
                    return $this->_getGiftSubtypes($event);
                case 'get_consent_channels':
                    return $this->_getConsentChannels($event);
                case 'get_consent_channel_categories':
                    return $this->_getConsentChannelCategories($event);
                case 'get_constituent_codes':
                    return $this->_getConstituentCodes($event);
                case 'get_custom_field_category':
                    return $this->_getCustomFieldCategory($event);
                case 'get_custom_field_category_values':
                    return $this->_getCustomFieldCategoryValues($event);
                case 'get_access_token':
                    return $this->_exchangeCodeForAccessToken($event);
                case 'get_auth_url':
                    return $this->_createAuthURL();
                case 'get_constituent':
                    return $this->_returnConstituent($event);
                case 'get_constituent_custom_fields':
                    return $this->_returnConstituentCustomFields($event);
                case 'get_address_types':
                    return $this->_getAddressTypes($event);
                case 'get_address_info_sources':
                    return $this->_getAddressInfoSources($event);
                case 'get_phone_types':
                    return $this->_getPhoneTypes($event);
                case 'get_relationship_types':
                    return $this->_getRelationshipTypes($event);
                case 'get_constituent_titles':
                    return $this->_getConstituentTitles($event);
                case 'get_tribute_types':
                    return $this->_getTributeTypes($event);
                case 'get_look_up':
                    return $this->_returnConstituentLookUpID($event);
                case 'process_gift':
                    return $this->_processGift($event);
                case 'process_batch_payment':
                    return $this->_processGift($event, true);
                case 'refresh_access_token':
                    return $this->_refreshToken($event, true);
            }
        }

        return new Failure('Invalid event action provided.');
    }

    private function _fetchValidateConfig($event)
    {
        //Check the event payload whether it includes valid configuration
        // phpcs:disable
        switch ($event['Intangible/Sky.action']) {
            case 'get_access_token':
                $errors = $this->_fetchValidateEventConfig(
                    $event,
                    ['application_code','client_id', 'client_secret', 'redirect_uri']
                );
                break;
            case 'get_auth_url':
                $errors = $this->_fetchValidateEventConfig($event, ['client_id', 'redirect_uri']);
                break;
            case 'refresh_access_token':
                $errors = $this->_fetchValidateEventConfig($event, ['client_id', 'client_secret', 'refresh_token']);
                break;
            case 'get_look_up':
            default:
                $errors = $this->_fetchValidateEventConfig(
                    $event,
                    ['client_id', 'client_secret', 'subscription_key', 'access_token', 'refresh_token']
                );
            // phpcs:enable
        }

        if (!empty($errors)) {
            $this->_throwConfigurationException($errors);
        }
    }

    private function _fetchValidateEventConfig($event, $required_fields = [])
    {
        $errors = [];

        foreach ($required_fields as $field) {
            if (!isset($event['Intangible/Sky.' . $field])) {
                if (!isset($this->settings[$this->settings['env']][$field])) {
                    $errors[] = $field;
                } else {
                    $this->_config[$field] = $this->settings[$this->settings['env']][$field];
                }
            } else {
                $this->_config[$field] = $event['Intangible/Sky.' . $field];
            }
        }

        return $errors;
    }

    private function _throwConfigurationException($errors = [])
    {
        throw new \Exception(
            'Please provide the following configuration fields within your configuration file or the event payload: ' .
            implode(', ', $errors)
        );
    }

    private function _getCustomFieldCategory($event)
    {
        $token_response = array();
        $entity         = !empty($event['Intangible/Sky.entity']) ? $event['Intangible/Sky.entity'] : 'constituent';

        $category_list = $this->_getCategories($event, $entity);
        if (isset($category_list->statusCode) && $category_list->statusCode == 401) {
            $token_response = $this->_refreshToken($event);
            if ($token_response instanceof Failure) {
                return $token_response;
            }
        }

        $category_list = $this->_getCategories($event, $entity, $token_response);
        if (!empty($category_list)) {
            $success_payload = $this->_getDefaultSuccessPayload($token_response);
            if (isset($category_list->count) && $category_list->count > 0) {
                $success_payload['Intangible/CustomField.categories'] = $category_list;
            } else {
                $success_payload['Intangible/CustomField.categories'] = [];
            }

            return new Success($success_payload);
        } else {
            return new Failure('Unable to get the list of custom field categories');
        }
    }

    private function _getCategories($event, $entity = 'constituent', $token = false)
    {
        $auth_key = $event['Intangible/Sky.access_token'];
        if (isset($token['access_token'])) {
            $auth_key = $token['access_token'];
        }
        $headers = array(
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        );

        if ($entity == 'gift') {
            $url = "https://api.sky.blackbaud.com/nxt-data-integration/v1/re/customfieldcategories?record_type=Gift";
        } else {
            $suffix = $entity == 'constituent' ? '/details' : '';
            $url    = "https://api.sky.blackbaud.com/".$entity."/v1/".$entity."s/customfields/categories".$suffix;
        }

        $method = 'GET';

        return json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                array()
            )
        );
    }

    private function _getCustomFieldCategoryValues($event)
    {
        $token_response  = array();
        $entity          = !empty($event['Intangible/Sky.entity']) ? $event['Intangible/Sky.entity'] : 'constituent';
        $category_values = $this->_getCategoryValues($event, $entity);
        if (isset($category_values->statusCode) && $category_values->statusCode == 401) {
            $token_response = $this->_refreshToken($event);
            if ($token_response instanceof Failure) {
                return $token_response;
            }
            $category_values = $this->_getCategoryValues($event, $entity, $token_response);
        }

        if (!empty($category_values)) {
            $success_payload = $this->_getDefaultSuccessPayload($token_response);
            if (isset($category_values->count) && $category_values->count > 0) {
                $success_payload['Intangible/CustomField.category.values'] = $category_values;
            } else {
                $success_payload['Intangible/CustomField.category.values'] = [];
            }

            return new Success($success_payload);
        } else {
            return new Failure('Unable to get the list of values from the custom field category.');
        }
    }

    private function _getCategoryValues($event, $entity = 'constituent', $token = false)
    {
        $auth_key = $event['Intangible/Sky.access_token'];
        if (isset($token['access_token'])) {
            $auth_key = $token['access_token'];
        }
        $headers = array(
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        );

        if ($entity == 'gift') {
            $codeTable = $event['Intangible/CustomField.codeTableId'] ?? false;
            if (empty($codeTable)) {
                return json_encode([]);
            } else {
                $url = "https://api.sky.blackbaud.com/nxt-data-integration/v1/re/codetables/"
                    . $codeTable . "/tableentries";
            }
        } else {
            $url = "https://api.sky.blackbaud.com/" . $entity . "/v1/"
                . $entity . "s/customfields/categories/values?category_name="
                . $event['Intangible/CustomField.category'];
        }

        $method = 'GET';

        return json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                array()
            )
        );
    }

    private function _getCampaigns($event)
    {
        $token_response = array();
        $campaigns      = $this->_getCampaignEntries($event);

        if (isset($campaigns->statusCode) && $campaigns->statusCode == 401) {
            $token_response = $this->_refreshToken($event);
            if ($token_response instanceof Failure) {
                return $token_response;
            }
            $campaigns = $this->_getCampaignEntries($event, $token_response);
        }

        if (!empty($campaigns)) {
            $success_payload = $this->_getDefaultSuccessPayload($token_response);
            if (isset($campaigns->count) && $campaigns->count > 0) {
                $success_payload['Intangible/Sky.Campaigns'] = $campaigns;
            } else {
                $success_payload['Intangible/Sky.Campaigns'] = [];
            }

            return new Success($success_payload);
        } else {
            return new Failure('Unable to get the list of Campaign.');
        }
    }

    private function _getCampaignEntries($event, $token = false)
    {
        $auth_key = $event['Intangible/Sky.access_token'];
        if (isset($token['access_token'])) {
            $auth_key = $token['access_token'];
        }
        $headers = array(
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        );

        $url    = "https://api.sky.blackbaud.com/nxt-data-integration/v1/re/campaigns";
        $method = 'GET';
        $data   = array();

        if (!empty($event['Intangible/List.exclude_inactive'])) {
            $url .= "?include_inactive=false";
        }

        return json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                $data
            )
        );
    }

    private function _getFunds($event)
    {
        $token_response = array();
        $funds          = $this->_getFundEntries($event);

        if (isset($funds->statusCode) && $funds->statusCode == 401) {
            $token_response = $this->_refreshToken($event);
            if ($token_response instanceof Failure) {
                return $token_response;
            }
            $funds = $this->_getFundEntries($event, $token_response);
        }

        if (!empty($funds)) {
            $success_payload = $this->_getDefaultSuccessPayload($token_response);
            if (isset($funds->count) && $funds->count > 0) {
                $success_payload['Intangible/Sky.Funds'] = $funds;
            } else {
                $success_payload['Intangible/Sky.Funds'] = [];
            }

            return new Success($success_payload);
        } else {
            return new Failure('Unable to get the list of Funds.');
        }
    }

    private function _getFundEntries($event, $token = false)
    {
        $auth_key = $event['Intangible/Sky.access_token'];
        if (isset($token['access_token'])) {
            $auth_key = $token['access_token'];
        }
        $headers = array(
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        );

        $url    = "https://api.sky.blackbaud.com/nxt-data-integration/v1/re/funds";
        $method = 'GET';
        $data   = array();

        if (!empty($event['Intangible/List.exclude_inactive'])) {
            $url .= "?include_inactive=false";
        }

        return json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                $data
            )
        );
    }

    private function _getAppeal($event)
    {
        $token_response = array();
        $batch_size     = 500;
        $appeals        = [];
        $first_batch    = $this->_getAppealEntries($event, $batch_size);

        if (isset($first_batch->statusCode) && $first_batch->statusCode == 401) {
            $token_response = $this->_refreshToken($event);
            if ($token_response instanceof Failure) {
                return $token_response;
            }
            $first_batch = $this->_getAppealEntries($event, $batch_size, 0, $token_response);
        }

        if (!empty($first_batch)) {
            if (isset($first_batch->count) && $first_batch->count > 0 && isset($first_batch->value)) {
                $appeals    = array_merge($appeals, $first_batch->value);
                $iterations = ceil($first_batch->count / $batch_size);
                if ($iterations > 1) {
                    for ($i = 2; $i <= $iterations; $i++) {
                        $appeal_batch_entries = $this->_getAppealEntries($event, $batch_size, $batch_size * ($i - 1));
                        if (isset($appeal_batch_entries->count)
                            && $appeal_batch_entries->count > 0
                            && isset($appeal_batch_entries->value)
                        ) {
                            $appeals = array_merge($appeals, $appeal_batch_entries->value);
                        }
                    }
                }
            }
        }

        if (!empty($appeals)) {
            $success_payload = $this->_getDefaultSuccessPayload($token_response);
            //Init the mocked RE NXT response object
            $result        = new \stdClass();
            $result->count = count($appeals);
            $result->value = $appeals;

            $success_payload['Intangible/Sky.Appeals'] = $result;

            return new Success($success_payload);
        } else {
            return new Failure('Unable to get the list of Appeals.');
        }
    }

    private function _getAppealEntries($event, $limit = 500, $offset = 0, $token = false)
    {
        $auth_key = $event['Intangible/Sky.access_token'];
        if (isset($token['access_token'])) {
            $auth_key = $token['access_token'];
        }
        $headers = array(
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        );

        $url    = "https://api.sky.blackbaud.com/nxt-data-integration/v1/re/appeals?limit=";
        $url   .= $limit . "&offset=" . $offset;
        $method = 'GET';
        $data   = array();

        if (!empty($event['Intangible/List.exclude_inactive'])) {
            $url .= "&include_inactive=false";
        }

        return json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                $data
            )
        );
    }

    private function _getGiftSubtypes($event)
    {
        $token_response = array();
        $subtypes       = $this->_getGiftSubtypesEntries($event);

        if (isset($subtypes->statusCode) && $subtypes->statusCode == 401) {
            $token_response = $this->_refreshToken($event);
            if ($token_response instanceof Failure) {
                return $token_response;
            }
            $subtypes = $this->_getGiftSubtypesEntries($event, $token_response);
        }

        if (!empty($subtypes)) {
            $success_payload = $this->_getDefaultSuccessPayload($token_response);
            if (isset($subtypes->count) && $subtypes->count > 0) {
                $success_payload['Intangible/Sky.GiftSubtypes'] = $subtypes;
            } else {
                $success_payload['Intangible/Sky.GiftSubtypes'] = [];
            }

            return new Success($success_payload);
        } else {
            return new Failure('Unable to get the list of Gift subtypes.');
        }
    }

    private function _getGiftSubtypesEntries($event, $token = false)
    {
        $auth_key = $event['Intangible/Sky.access_token'];
        if (isset($token['access_token'])) {
            $auth_key = $token['access_token'];
        }
        $headers = array(
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        );

        $url    = "https://api.sky.blackbaud.com/gift/v1/giftsubtypes";
        $method = 'GET';

        return json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                array()
            )
        );
    }

    private function _getConsentChannels($event)
    {
        $token_response       = array();
        $constituent_channels = $this->_getConsentChannelsValues($event);
        if (isset($constituent_channels->statusCode) && $constituent_channels->statusCode == 401) {
            $token_response = $this->_refreshToken($event);
            if ($token_response instanceof Failure) {
                return $token_response;
            }
            $constituent_channels = $this->_getConsentChannelsValues($event, $token_response);
        }

        if (!empty($constituent_channels)) {
            $success_payload = $this->_getDefaultSuccessPayload($token_response);
            if (isset($constituent_channels->count) && $constituent_channels->count > 0) {
                $success_payload['Intangible/Sky.Consent_channels'] = $constituent_channels;
            } else {
                $success_payload['Intangible/Sky.Consent_channels'] = [];
            }

            return new Success($success_payload);
        } else {
            return new Failure('Unable to get the list of Consent Channel values.');
        }
    }

    private function _getConsentChannelsValues($event, $token = false)
    {
        $auth_key = $event['Intangible/Sky.access_token'];
        if (isset($token['access_token'])) {
            $auth_key = $token['access_token'];
        }
        $headers = array(
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        );

        $url    = "https://api.sky.blackbaud.com/commpref/v1/consent/channels";
        $method = 'GET';

        return json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                array()
            )
        );
    }

    private function _getConsentChannelCategories($event)
    {
        $token_response                 = array();
        $constituent_channel_categories = $this->_getConsentChannelCategoriesValues($event);
        if (isset($constituent_channel_categories->statusCode) && $constituent_channel_categories->statusCode == 401) {
            $token_response = $this->_refreshToken($event);
            if ($token_response instanceof Failure) {
                return $token_response;
            }
            $constituent_channel_categories = $this->_getConsentChannelCategoriesValues($event, $token_response);
        }

        if (!empty($constituent_channel_categories)) {
            $success_payload = $this->_getDefaultSuccessPayload($token_response);
            if (isset($constituent_channel_categories->count) && $constituent_channel_categories->count > 0) {
                $success_payload['Intangible/Sky.Consent_channel_categories'] = $constituent_channel_categories;
            } else {
                $success_payload['Intangible/Sky.Consent_channel_categories'] = [];
            }

            return new Success($success_payload);
        } else {
            return new Failure('Unable to get the list of Consent Channel Categories.');
        }
    }

    private function _getConsentChannelCategoriesValues($event, $token = false)
    {
        $auth_key = $event['Intangible/Sky.access_token'];
        if (isset($token['access_token'])) {
            $auth_key = $token['access_token'];
        }
        $headers = array(
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        );

        $url    = "https://api.sky.blackbaud.com/commpref/v1/consent/channelcategories";
        $method = 'GET';

        return json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                array()
            )
        );
    }

    private function _getConstituentCodes($event)
    {
        $token_response    = array();
        $constituent_codes = $this->_getConstituentCodeValues($event);
        if (isset($constituent_codes->statusCode) && $constituent_codes->statusCode == 401) {
            $token_response = $this->_refreshToken($event);
            if ($token_response instanceof Failure) {
                return $token_response;
            }
            $constituent_codes = $this->_getConstituentCodeValues($event, $token_response);
        }

        if (!empty($constituent_codes)) {
            $success_payload = $this->_getDefaultSuccessPayload($token_response);
            if (isset($constituent_codes->count) && $constituent_codes->count > 0) {
                $success_payload['Intangible/Sky.constituent_codes'] = $constituent_codes;
            } else {
                $success_payload['Intangible/Sky.constituent_codes'] = [];
            }

            return new Success($success_payload);
        } else {
            return new Failure('Unable to get the list of Constituent code values.');
        }
    }

    private function _getConstituentCodeValues($event, $token = false)
    {
        $auth_key = $event['Intangible/Sky.access_token'];
        if (isset($token['access_token'])) {
            $auth_key = $token['access_token'];
        }
        $headers = array(
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        );

        $url    = "https://api.sky.blackbaud.com/constituent/v1/constituentcodetypes";
        $method = 'GET';

        return json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                array()
            )
        );
    }

    private function _getNameFormats($event)
    {
        $token_response = array();
        $name_formats   = $this->_getNameFormatValues($event);
        if (isset($name_formats->statusCode) && $name_formats->statusCode == 401) {
            $token_response = $this->_refreshToken($event);
            if ($token_response instanceof Failure) {
                return $token_response;
            }
            $name_formats = $this->_getNameFormatValues($event, $token_response);
        }

        if (!empty($name_formats)) {
            $success_payload = $this->_getDefaultSuccessPayload($token_response);
            if (isset($name_formats->count) && $name_formats->count > 0) {
                $success_payload['Intangible/Sky.name_formats'] = $name_formats;
            } else {
                $success_payload['Intangible/Sky.name_formats'] = [];
            }

            return new Success($success_payload);
        } else {
            return new Failure('Unable to get the list of Name formats.');
        }
    }

    private function _getNameFormatValues($event, $token = false)
    {
        $auth_key = $event['Intangible/Sky.access_token'];
        if (isset($token['access_token'])) {
            $auth_key = $token['access_token'];
        }
        $headers = array(
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        );

        $url    = "https://api.sky.blackbaud.com/constituent/v1/nameformatconfigurations";
        $method = 'GET';

        return json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                array()
            )
        );
    }

    private function _getAddressTypes($event)
    {
        $token_response    = array();
        $address_type_list = $this->_getAddressTypesValues($event);
        if (isset($address_type_list->statusCode) && $address_type_list->statusCode == 401) {
            $token_response = $this->_refreshToken($event);
            if ($token_response instanceof Failure) {
                return $token_response;
            }
            $address_type_list = $this->_getAddressTypesValues($event, $token_response);
        }

        if (!empty($address_type_list)) {
            $success_payload = $this->_getDefaultSuccessPayload($token_response);
            if (isset($address_type_list->count) && $address_type_list->count > 0) {
                $success_payload['Intangible/AddressTypes'] = $address_type_list;
            } else {
                $success_payload['Intangible/AddressTypes'] = [];
            }

            return new Success($success_payload);
        } else {
            return new Failure('Unable to get the list of custom field categories');
        }
    }

    private function _getAddressTypesValues($event, $token = false)
    {
        $auth_key = $event['Intangible/Sky.access_token'];
        if (isset($token['access_token'])) {
            $auth_key = $token['access_token'];
        }
        $headers = array(
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        );

        $url    = "https://api.sky.blackbaud.com/constituent/v1/addresstypes";
        $method = 'GET';

        return json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                array()
            )
        );
    }

    private function _getAddressInfoSources($event)
    {
        $entries        = null;
        $token_response = array();
        $code_tables    = $this->_getCodeTables($event);
        if (isset($code_tables->statusCode) && $code_tables->statusCode == 401) {
            $token_response = $this->_refreshToken($event);
            if ($token_response instanceof Failure) {
                return $token_response;
            }

            $code_tables = $this->_getCodeTables($event, $token_response);
        }

        $tribute_types_code_table_id = null;
        if (isset($code_tables->count) && $code_tables->count > 0) {
            foreach ($code_tables->value as $code_table) {
                if ($code_table->name == "Address Info Sources") {
                    $tribute_types_code_table_id = $code_table->code_tables_id;
                }
            }
        }

        if (!empty($tribute_types_code_table_id)) {
            $code_table_entries = $this->_getCodeTableEntries($event, $tribute_types_code_table_id);
            if (isset($code_table_entries->count) && $code_table_entries->count > 0) {
                $entries = $code_table_entries;
            }
        }

        if (!empty($entries)) {
            $success_payload = $this->_getDefaultSuccessPayload($token_response);
            $success_payload['Intangible/AddressInfoSources'] = $entries;

            return new Success($success_payload);
        } else {
            return new Failure('Unable to get the list of tribute categories');
        }
    }

    private function _getPhoneTypes($event)
    {
        $token_response  = array();
        $phone_type_list = $this->_getPhoneTypesValues($event);
        if (isset($phone_type_list->statusCode) && $phone_type_list->statusCode == 401) {
            $token_response = $this->_refreshToken($event);
            if ($token_response instanceof Failure) {
                return $token_response;
            }
            $phone_type_list = $this->_getPhoneTypesValues($event, $token_response);
        }

        if (!empty($phone_type_list)) {
            $success_payload = $this->_getDefaultSuccessPayload($token_response);
            if (isset($phone_type_list->count) && $phone_type_list->count > 0) {
                $success_payload['Intangible/PhoneTypes'] = $phone_type_list;
            } else {
                $success_payload['Intangible/PhoneTypes'] = [];
            }

            return new Success($success_payload);
        } else {
            return new Failure('Unable to get the list of custom field categories');
        }
    }

    private function _getPhoneTypesValues($event, $token = false)
    {
        $auth_key = $event['Intangible/Sky.access_token'];
        if (isset($token['access_token'])) {
            $auth_key = $token['access_token'];
        }
        $headers = array(
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        );

        $url    = "https://api.sky.blackbaud.com/constituent/v1/phonetypes";
        $method = 'GET';

        return json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                array()
            )
        );
    }

    private function _getRelationshipTypes($event)
    {
        $token_response         = array();
        $relationship_type_list = $this->_getRelationshipTypesValues($event);
        if (isset($relationship_type_list->statusCode) && $relationship_type_list->statusCode == 401) {
            $token_response = $this->_refreshToken($event);
            if ($token_response instanceof Failure) {
                return $token_response;
            }
            $relationship_type_list = $this->_getRelationshipTypesValues($event, $token_response);
        }

        if (!empty($relationship_type_list)) {
            $success_payload = $this->_getDefaultSuccessPayload($token_response);
            if (isset($relationship_type_list->count) && $relationship_type_list->count > 0) {
                $success_payload['Intangible/RelationshipTypes'] = $relationship_type_list;
            } else {
                $success_payload['Intangible/RelationshipTypes'] = [];
            }

            return new Success($success_payload);
        } else {
            return new Failure('Unable to get the list of custom field categories');
        }
    }

    private function _getRelationshipTypesValues($event, $token = false)
    {
        $auth_key = $event['Intangible/Sky.access_token'];
        if (isset($token['access_token'])) {
            $auth_key = $token['access_token'];
        }
        $headers = array(
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        );

        $url    = "https://api.sky.blackbaud.com/constituent/v1/relationshiptypes";
        $method = 'GET';

        return json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                array()
            )
        );
    }

    private function _getConstituentTitles($event)
    {
        $token_response = array();
        $title_list     = $this->_getConstituentTitleValues($event);

        if (isset($title_list->statusCode) && $title_list->statusCode == 401) {
            $token_response = $this->_refreshToken($event);
            if ($token_response instanceof Failure) {
                return $token_response;
            }
            $title_list = $this->_getConstituentTitleValues($event, $token_response);
        }

        if (!empty($title_list)) {
            $success_payload = $this->_getDefaultSuccessPayload($token_response);
            if (isset($title_list->count) && $title_list->count > 0) {
                $success_payload['Intangible/Sky.ConstituentTitles'] = $title_list;
            } else {
                $success_payload['Intangible/Sky.ConstituentTitles'] = [];
            }

            return new Success($success_payload);
        } else {
            return new Failure('Unable to get the list of constituent titles.');
        }
    }

    private function _getConstituentTitleValues($event, $token = false)
    {
        $auth_key = $event['Intangible/Sky.access_token'];
        if (isset($token['access_token'])) {
            $auth_key = $token['access_token'];
        }
        $headers = array(
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        );

        $url    = "https://api.sky.blackbaud.com/constituent/v1/titles";
        $method = 'GET';

        return json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                array()
            )
        );
    }

    private function _getTributeTypes($event)
    {
        $entries        = null;
        $token_response = array();
        $code_tables    = $this->_getCodeTables($event);
        if (isset($code_tables->statusCode) && $code_tables->statusCode == 401) {
            $token_response = $this->_refreshToken($event);
            if ($token_response instanceof Failure) {
                return $token_response;
            }

            $code_tables = $this->_getCodeTables($event, $token_response);
        }

        $tribute_types_code_table_id = null;
        if (isset($code_tables->count) && $code_tables->count > 0) {
            foreach ($code_tables->value as $code_table) {
                if ($code_table->name == "Tribute Types") {
                    $tribute_types_code_table_id = $code_table->code_tables_id;
                }
            }
        }

        if (!empty($tribute_types_code_table_id)) {
            $code_table_entries = $this->_getCodeTableEntries($event, $tribute_types_code_table_id);
            if (isset($code_table_entries->count) && $code_table_entries->count > 0) {
                $entries = $code_table_entries;
            }
        }

        if (!empty($entries)) {
            $success_payload = $this->_getDefaultSuccessPayload($token_response);
            $success_payload['Intangible/TributeTypes'] = $entries;

            return new Success($success_payload);
        } else {
            return new Failure('Unable to get the list of tribute categories');
        }
    }

    private function _returnConstituent($event)
    {
        $token_response      = array();
        $constituentResponse = $this->_getConstituent($event);
        if (isset($constituentResponse->statusCode) && $constituentResponse->statusCode == 401) {
            $token_response = $this->_refreshToken($event);
            if ($token_response instanceof Failure) {
                return $token_response;
            }
            $constituentResponse = $this->_getConstituent($event, $token_response);
        }

        if (!empty($constituentResponse->id)) {
            $success_payload                = $this->_getDefaultSuccessPayload($token_response);
            $success_payload['Person.data'] = $constituentResponse;

            return new Success($success_payload);
        } else {
            $message = "Error while fetching constituent details";
            MachinePack::log('Invalid constituentResponse :: ' . json_encode($constituentResponse));
            if (is_array($constituentResponse) && isset($constituentResponse[0])) {
                if (isset($constituentResponse[0]->message)) {
                    $message = $constituentResponse[0]->message;
                }
            }

            return new Failure($message);
        }
    }

    private function _returnConstituentLookUpID($event)
    {
        $token_response      = array();
        $constituentResponse = $this->_getConstituentSearch($event);
        if (isset($constituentResponse->statusCode) && $constituentResponse->statusCode == 401) {
            $token_response = $this->_refreshToken($event);
            if ($token_response instanceof Failure) {
                return $token_response;
            }
            $constituentResponse = $this->_getConstituentSearch($event, $token_response);
        }
        if (!isset($constituentResponse->statusCode)) {
            if (isset($constituentResponse->count) && $constituentResponse->count > 0) {
                $success_payload = $this->_getDefaultSuccessPayload($token_response);
                $success_payload['Person.identifier']        = $constituentResponse->value[0]->id;
                $success_payload['Person.lookup.identifier'] = $constituentResponse->value[0]->lookup_id;

                return new Success($success_payload);
            }
        } else {
            return new Failure($constituentResponse->statusCode . ' ' . $constituentResponse->message);
        }
    }

    private function _returnConstituentCustomFields($event)
    {
        if (empty($event['Intangible/Event.payload']['constituent_id'])) {
            return new Failure('Invalid constituent_id.');
        }

        $token_response                  = array();
        $constituentCustomFieldsResponse = $this->_getConstituentCustomFields($event);
        if (isset($constituentCustomFieldsResponse->statusCode)
            && $constituentCustomFieldsResponse->statusCode == 401
        ) {
            $token_response = $this->_refreshToken($event);
            if ($token_response instanceof Failure) {
                return $token_response;
            }
            $constituentCustomFieldsResponse = $this->_getConstituentCustomFields($event, $token_response);
        }

        if (!empty($constituentCustomFieldsResponse->count && !empty($constituentCustomFieldsResponse->value))) {
            $success_payload                = $this->_getDefaultSuccessPayload($token_response);
            $success_payload['Person.data'] = $constituentCustomFieldsResponse->value;

            return new Success($success_payload);
        } else {
            $message = "Error while fetching constituent custom fields";
            if (isset($constituentCustomFieldsResponse[0])) {
                if (isset($constituentCustomFieldsResponse[0]->message)) {
                    $message = $constituentCustomFieldsResponse[0]->message;
                }
            }

            return new Failure($message);
        }
    }

    private function _getConstituentPhoneNumbers($constituentId, $event, $token = false)
    {
        $auth_key = $event['Intangible/Sky.access_token'];
        if (isset($token['access_token'])) {
            $auth_key = $token['access_token'];
        }
        $headers = array(
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        );

        $url    = "https://api.sky.blackbaud.com/constituent/v1/constituents/"
            . $constituentId . "/phones";
        $method = 'GET';

        return json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                array()
            )
        );
    }

    private function _getConstituentCustomFields($event, $token = false)
    {
        $auth_key = $event['Intangible/Sky.access_token'];
        if (isset($token['access_token'])) {
            $auth_key = $token['access_token'];
        }
        $headers = array(
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        );

        $url    = "https://api.sky.blackbaud.com/constituent/v1/constituents/"
            . $event['Intangible/Event.payload']['constituent_id'] . "/customfields";
        $method = 'GET';

        return json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                array()
            )
        );
    }

    private function _createBatch($event)
    {
        $response = $this->_processCreateBatch($event);
        if (isset($response->statusCode) && $response->statusCode == 401) {
            $token_response = $this->_refreshToken($event);
            if ($token_response instanceof Failure) {
                return $token_response;
            }
            $response = $this->_processCreateBatch($event, $token_response);
        }

        if (!empty($response) && isset($response->batch_id)) {
            $success_payload = $this->_getDefaultSuccessPayload($token_response ?? []);
            $success_payload['Intangible/Sky.Batch.identifier'] = $response->batch_id;

            return new Success($success_payload);
        }
        return new Failure("Unable to create batch");
    }

    private function _deleteBatch($event)
    {
        try {
            $this->_processDeleteBatch($event);
        } catch (\Exception $e) {
            return new Failure($e->getMessage());
        }

        return new Success();
    }

    private function _batchLookup($event)
    {
        $response = $this->_processBatchLookup($event);
        if (isset($response->statusCode) && $response->statusCode == 401) {
            $token_response = $this->_refreshToken($event);
            if ($token_response instanceof Failure) {
                return $token_response;
            }
            $response = $this->_processBatchLookup($event, $token_response);
        }

        if (!empty($response) && isset($response->count) && $response->count >= 1) {
            $success_payload = $this->_getDefaultSuccessPayload($token_response ?? []);
            $success_payload['Intangible/Sky.Batch.count'] = $response->count;
            $success_payload['Intangible/Sky.giftbatches'] = $response->giftbatches;

            return new Success($success_payload);
        }
        return new Failure("Unable to find batch");
    }

    private function _processCreateBatch($event, $token = false)
    {
        $auth_key = $event['Intangible/Sky.access_token'];
        if (isset($token['access_token'])) {
            $auth_key = $token['access_token'];
        }
        $headers = array(
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        );

        $url    = 'https://api.sky.blackbaud.com/gift-batch/v1/giftbatches';
        $method = 'POST';
        $data   = array(
            'batch_description' => $event['Intangible/Event.payload']['batch_description'] ?? "",
            'expected_batch_total' => $event['Intangible/Event.payload']['expected_batch_total'] ?? "",
            'expected_number' => $event['Intangible/Event.payload']['expected_number'] ?? ""
        );

        if (!empty($event['Intangible/Event.payload']['batch_number'])) {
            $data['batch_number'] = $event['Intangible/Event.payload']['batch_number'];
        }

        $response = json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                $data
            )
        );

        return $response;
    }

    private function _processDeleteBatch($event)
    {
        $auth_key = $event['Intangible/Sky.access_token'];
        if (isset($token['access_token'])) {
            $auth_key = $token['access_token'];
        }
        $headers = array(
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        );

        $url    = 'https://api.sky.blackbaud.com/gift-batch/v1/giftbatches/'
            . $event['Intangible/Event.payload']['batch_id'];
        $method = 'DELETE';

        $response = json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                []
            )
        );

        return $response;
    }

    private function _processBatchLookup($event, $token = false)
    {
        $auth_key = $event['Intangible/Sky.access_token'];
        if (isset($token['access_token'])) {
            $auth_key = $token['access_token'];
        }
        $headers = array(
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        );

        $url    = 'https://api.sky.blackbaud.com/gift-batch/v1/giftbatches?approved=false&added_by='
            . $event['Intangible/Sky.batch_added_by'];
        $method = 'GET';
        $data   = array();

        $response = json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                $data
            )
        );

        return $response;
    }

    private function _processGift($event, $batch = false)
    {
        $token_response      = array();
        $constituentResponse = $this->_createConstituent($event);

        if (isset($constituentResponse->statusCode) && $constituentResponse->statusCode == 401) {
            $token_response = $this->_refreshToken($event);
            if ($token_response instanceof Failure) {
                return $token_response;
            }
            $constituentResponse = $this->_createConstituent($event, $token_response);
        }


        if ($constituentResponse instanceof Failure) {
            return $constituentResponse;
        }

        if (!empty($constituentResponse)) {
            if (isset($constituentResponse->statusCode) && $constituentResponse->statusCode == 401) {
                return new Failure(
                    $constituentResponse->message,
                    array('errorcode' => $constituentResponse->statusCode)
                );
            }
            if (is_array($constituentResponse) && !isset($constituentResponse->id)) {
                return new Failure(
                    $constituentResponse[0]->message,
                    array('errorcode' => $constituentResponse[0]->error_code)
                );
            } elseif (isset($constituentResponse->Errors)) {
                return new Failure(
                    $constituentResponse->message
                );
            }

            $constituentId     = $constituentResponse->id;
            $giftConstituentId = $constituentId;
            //If the client wish to rather assign the gift to the organisation
            if (!empty($constituentResponse->assignGiftToOrganisation)
                && !empty($constituentResponse->organisationId)
            ) {
                $giftConstituentId = $constituentResponse->organisationId;
            }

            $giftResponse = $this->_createGift($event, $giftConstituentId, $batch, $token_response);
            //If the token is invalid, try to refresh it
            if (is_object($giftResponse) && !empty($giftResponse->statusCode) && ($giftResponse->statusCode == 401)) {
                $token_response = $this->_refreshToken($event);
                if ($token_response instanceof Failure) {
                    return $token_response;
                }
                $giftResponse = $this->_createGift($event, $giftConstituentId, $batch, $token_response);
            }

            //Check whether the gift response is valid
            if (is_object($giftResponse)) {
                if ($batch && empty($giftResponse->gifts)) {
                    return new Failure(
                        'Invalid Batch Gift Response - empty gifts list',
                        ['response' => json_encode($giftResponse)]
                    );
                }

                if (!$batch && empty($giftResponse->id)) {
                    return new Failure(
                        'Invalid Gift Response - Invalid gift ID',
                        ['response' => json_encode($giftResponse)]
                    );
                }
            } else {
                return new Failure(
                    'Invalid Gift Response - Non Object',
                    ['response' => json_encode($giftResponse)]
                );
            }

            if (is_object($giftResponse) && !empty($giftResponse->errors)) {
                return new Failure(
                    "Creating batch gift failed.",
                    [
                        'errorcode' => $giftResponse->errors[0]->exception_error_message,
                        'lookup' => $giftResponse->errors[0]->lookup_id,
                        'batch_id' => $giftResponse->errors[0]->batch_id,
                        'Person.identifier' => $constituentResponse->id
                    ]
                );
            }

            $gift_id  = false;
            $batch_id = false;
            if ($batch) {
                foreach ($giftResponse->gifts as $gift) {
                    if (!empty($event['Intangible/Event.payload']['patch_gift_receipt'])) {
                        $this->_patchReceipt($gift->id, $event);
                    }
                    $gift_id = $gift->id;
                    //Note: batch_id is usually empty, SKY API doesn't return this for some reason
                    MachinePack::log(
                        'MP SKY API $giftResponse gifts -> gift batch_id :: ' . json_encode($gift->batch_id)
                    );
                    $batch_id = !empty($gift->batch_id) ? $gift->batch_id :
                        (!empty($event['Intangible/Sky.Batch.identifier']) ?
                            $event['Intangible/Sky.Batch.identifier'] : '');
                }
            } else {
                if (!empty($event['Intangible/Event.payload']['patch_gift_receipt'])) {
                    $this->_patchReceipt($giftResponse->id, $event);
                }
                $gift_id = $giftResponse->id;
            }

            //Update Constituent Consent if required
            if (!empty($event['Intangible/Event.payload']['constituent_update_consent'])) {
                if ($event['Intangible/Event.payload']['constituent_update_consent'] === true) {
                    if (isset($event['Intangible/Event.payload']['constituent_consent'])
                        && !empty($event['Intangible/Event.payload']['constituent_consent_channel'])
                    ) {
                        $response = "NoResponse";
                        if (in_array($event['Intangible/Event.payload']['constituent_consent'], ['yes', 'on', 1])) {
                            $response = "OptIn";
                        } elseif (in_array($event['Intangible/Event.payload']['constituent_consent'], ['no', '', 0])) {
                            $response = "OptOut";
                        }

                        $category = null;
                        if (!empty($event['Intangible/Event.payload']['constituent_consent_channel_category'])) {
                            $category = $event['Intangible/Event.payload']['constituent_consent_channel_category'];
                        }

                        $this->_updateConstituentConsent(
                            $event,
                            $constituentId,
                            $event['Intangible/Event.payload']['constituent_consent_channel'],
                            $response,
                            $category
                        );
                    }
                }
            }

            if (!$batch) {
                $this->_createTributeForNonBatchGift($event, $token_response);
            }

            $success_payload = $this->_getDefaultSuccessPayload($token_response);
            $success_payload['Person.identifier']               = $constituentId;
            $success_payload['MoneyTransfer.identifier']        = $gift_id;
            $success_payload['Intangible/GiftBatch.identifier'] = $batch_id;

            return new Success($success_payload);
        } else {
            return new Failure('Error:: Invalid API response while retrieving Constituent Id.');
        }
    }

    /**
     * Create a constituent
     * @param Crmsync $event
     */
    private function _createConstituent($event, $token = false)
    {
        try {
            $auth_key = $event['Intangible/Sky.access_token'];
            if (isset($token['access_token'])) {
                $auth_key = $token['access_token'];
            }
            $headers = array(
                "Content-Type: application/json",
                "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
                "Authorization: Bearer " . $auth_key
            );

            if (!empty($event['Intangible/Event.payload']['constituent_lookup'])) {
                switch ($event['Intangible/Event.payload']['constituent_lookup']) {
                    case 'email':
                        $constituentResponse = $this->_getConstituentSearch($event, $token);
                        break;
                    default:
                        $constituentResponse = $this->_getConstituentCustomSearch($event, $token, true);
                        //If no results found try the alternative search method
                        if (isset($constituentResponse->count) && $constituentResponse->count == 0) {
                            if (!empty($event['Intangible/Event.payload']['constituent_lookup_alternative'])
                                && $event['Intangible/Event.payload']['constituent_lookup_alternative'] === 'email'
                            ) {
                                $constituentResponse = $this->_getConstituentSearch($event, $token);
                                break;
                            }
                        }
                }
            } else {
                $constituentResponse = $this->_getConstituentSearch($event, $token);
            }

            MachinePack::log('$constituentResponse :: ' . json_encode($constituentResponse));

            if ($constituentResponse instanceof Failure) {
                return new Failure($constituentResponse->data);
            }

            if (isset($constituentResponse->statusCode)) {
                //Token needs to be refreshed - will be handled in parent call/function
                if ($constituentResponse->statusCode == 401) {
                    return $constituentResponse;
                } else {
                    return new Failure($constituentResponse->message);
                }
            } else {
                $existing_donor = false;
                if (isset($constituentResponse->count) && $constituentResponse->count == 0) {
                    $method = 'POST';
                    $url    = 'https://api.sky.blackbaud.com/constituent/v1/constituents';
                    $data   = $this->_getConstituentData($event);
                    MachinePack::log('CRM RE NXT DEBUG :: _getConstituentData :: ' . json_encode($data));
                    $constituentResponse = json_decode(
                        $this->_skyApiCall(
                            $url,
                            $method,
                            $headers,
                            $data
                        )
                    );
                    $constituentId       = $constituentResponse->id;


                    if (!empty($data['address'])) {
                        //Patch the new primary address with extra fields if necessary
                        $this->_patchNewConstituentPrimaryAddress($event, $constituentId, $headers);
                    }

                    //Create incomplete address if necessary
                    MachinePack::log('_getConstituentData $data :: ' . json_encode($data));
                    if (empty($data['address'])) {
                        $this->_recordNewConstituentIncompleteAddress($constituentId, $event, $headers);
                    }

                    //Handle custom fields if provided
                    if (!empty($event['Intangible/Event.payload']['custom_fields'])
                        && $constituentId
                    ) {
                        $this->_createConstituentCustomFields(
                            $constituentId,
                            $event,
                            $headers
                        );
                    }

                    if (!empty($event['Intangible/Event.payload']['constituent_code'])
                        && $constituentId
                    ) {
                        $this->_createConstituentCode(
                            $constituentId,
                            $event['Intangible/Event.payload']['constituent_code'],
                            $headers
                        );
                    }
                } else {
                    $existing_donor = true;
                    $constituentId  = $constituentResponse->value[0]->id;
                    $this->_patchConstituent(
                        $constituentId,
                        $event,
                        $headers
                    );
                }

                //If mailing address is empty/incomplete && mailing address type is set
                //then
                //If billing address is complete
                //use the billing address as mailing address
                //There's another scenario where the constituent already exist, but has no (primary) address
                //In that case the $data['address'] would be empty
                //So we need to check whether the provided mailing address is complete
                $billing_as_primary_mailing = false;
                if (empty($data['address']) && !empty($event['Intangible/Event.payload']['address_type'])) {
                    if ($existing_donor && !$this->_isAddressComplete($event)) {
                        $billing_as_primary_mailing = true;
                    }
                }

                //Update the billing address for both new / existing constituents
                $this->_updateConstituentBillingAddress(
                    $constituentId,
                    $existing_donor,
                    $billing_as_primary_mailing,
                    $event,
                    $headers
                );

                if ($this->_isRelationshipRequired($event)) {
                    //Create individual - organisation relationship
                    if (!empty($event['Intangible/Event.payload']['organisation_name'])) {
                        $organisationConstituentResponse = $this->_getOrganisationConstituentSearch($event);
                        if (isset($organisationConstituentResponse->count)
                            && $organisationConstituentResponse->count > 0
                        ) {
                            if ($organisationConstituentResponse->count == 1) {
                                $organisationConstituentId = $organisationConstituentResponse->value[0]->id;
                            } else {
                                $constituentList = $this->_getConstituentListByIds(
                                    $event,
                                    $organisationConstituentResponse
                                );

                                if (!empty($constituentList && !empty($constituentList->count))) {
                                    if (!empty($constituentList->value) && is_array($constituentList->value)) {
                                        $constituentListRecords = $constituentList->value;
                                        //It should pick the first matching result from $organisationConstituentResponse
                                        foreach ($constituentListRecords as $constituentListRecord) {
                                            if ($constituentListRecord->type == 'Organization') {
                                                $organisationConstituentId = $constituentListRecord->id;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (!isset($organisationConstituentId)) {
                            $organisationConstituentId = $this->_createOrganisationConstituent($event, $headers);
                        }

                        if ($organisationConstituentId) {
                            $this->_createRelationship(
                                $constituentId,
                                $organisationConstituentId,
                                $headers,
                                $event['Intangible/Event.payload']
                            );

                            if (!empty($event['Intangible/Event.payload']['gift_assign_to_organisation'])) {
                                $individualConstituent = $constituentResponse->value[0] ?? $constituentResponse;
                                //Attach the organisation id to individual
                                $individualConstituent->assignGiftToOrganisation = true;
                                $individualConstituent->organisationId           = $organisationConstituentId;
                                return $individualConstituent;
                            }
                        }
                    }
                }
            }

            if (!empty($constituentResponse)) {
                if (isset($constituentResponse->err)) {
                    return new Failure($constituentResponse->err);
                }
            }

            //Return either the first result of the search or created profile
            return $constituentResponse->value[0] ?? $constituentResponse;
        } catch (\Exception $ex) {
            $exceptionMsg = $ex->getMessage();
            $returnArray  = array('Errors' => true, 'message' => $exceptionMsg);
            return json_encode($returnArray);
            //TODO should log stacktrace
        }
    }

    private function _createConstituentSimple($event, $token = false)
    {
        $auth_key = $event['Intangible/Sky.access_token'];
        if (isset($token['access_token'])) {
            $auth_key = $token['access_token'];
        }
        $headers = array(
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        );

        $method = 'POST';
        $url    = 'https://api.sky.blackbaud.com/constituent/v1/constituents';

        $data                = $event['constituent_data'];
        $constituentResponse = json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                $data
            )
        );

        return $constituentResponse;
    }

    private function _createOrganisationConstituent($event, $headers)
    {
        try {
            $method = 'POST';
            $url    = 'https://api.sky.blackbaud.com/constituent/v1/constituents';
            $data   = [
                'deceased' => 'false',
                'name' => $event['Intangible/Event.payload']['organisation_name'],
                "type" => "Organization"
            ];

            $constituentResponse = json_decode(
                $this->_skyApiCall(
                    $url,
                    $method,
                    $headers,
                    $data
                )
            );

            return $constituentResponse->id ?? false;
        } catch (\Exception $e) {
            MachinePack::log('SKY API _createOrganisationConstituent Error: ' . $e, 'debug');
        }
    }

    private function _updateConstituentConsent($event, $constituentId, $channel, $response, $category = null)
    {
        try {
            $url = 'https://api.sky.blackbaud.com/commpref/v1/consent/consents';

            $headers = array(
                "Content-Type: application/json",
                "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
                "Authorization: Bearer " . $event['Intangible/Sky.access_token']
            );

            $data = array(
                'constituent_id' => $constituentId,
                'channel' => $channel,
                'consent_date' => date('c'),
                'constituent_consent_response' => $response
            );

            if (!empty($category)) {
                $data['category'] = $category;
            }

            $response = $this->_skyApiCall(
                $url,
                'POST',
                $headers,
                $data
            );

            return $response;
        } catch (\Exception $ex) {
            $exceptionMsg = $ex->getMessage();
            $returnArray  = array('Errors' => true, 'message' => $exceptionMsg);
            return json_encode($returnArray);
        }
    }

    private function _isRelationshipRequired($event)
    {
        return isset($event['Intangible/Event.payload']['create_relationship']) &&
            $event['Intangible/Event.payload']['create_relationship'] === true;
    }

    private function _getConstituentData($event)
    {
        MachinePack::log(
            'CRM RE NXT DEBUG _getConstituentData $event :: ' . json_encode($event['Intangible/Event.payload'])
        );
        $data = array(
            "deceased" => 'false',
            "email" => array(
                "address" => $event['Intangible/Event.payload']['address_email'],
                "do_not_email" => false,
                "inactive" => false,
                "primary" => true,
                "type" => "Email"
            ),
            "type" => $event['Intangible/Event.payload']['type']
        );

        if (!empty($event['Intangible/Event.payload']['address_phone'])) {
            $data["phone"] = array (
                "do_not_call" =>  false,
                "inactive" => false,
                "number" => $event['Intangible/Event.payload']['address_phone'],
                "primary" => true,
                "type" => $event['Intangible/Event.payload']['phone_type']
            );
        }

        $is_mailing_address_complete = $this->_isAddressComplete($event);
        MachinePack::log(
            '_getConstituentData $is_mailing_address_complete :: ' . json_encode($is_mailing_address_complete)
        );
        //If the mailing address is incomplete don't create the address here, use create new address method instead!
        if ($is_mailing_address_complete) {
            $data["address"] = array(
                "address_lines" => $event['Intangible/Event.payload']['address_street'],
                "city" => $event['Intangible/Event.payload']['address_suburb'],
                "state" => $event['Intangible/Event.payload']['address_state'],
                "country" => $event['Intangible/Event.payload']['address_country'],
                "do_not_mail" => false,
                "postal_code" => $event['Intangible/Event.payload']['address_postcode'],
                "preferred" => true,
                "type" => $event['Intangible/Event.payload']['address_type'],
            );

            if (!empty($event['Intangible/Event.payload']['constituent_address_set_start_date'])) {
                $data['address']['start'] = date('c');
            }

            //Note - some fields such information source or preferred are not available in create constituent method
        }

        if (!empty($event['Intangible/Event.payload']['primary_addressee_configuration_id'])) {
            $data["primary_addressee"] = array(
                "configuration_id" => $event['Intangible/Event.payload']['primary_addressee_configuration_id']
            );
        }

        if (!empty($event['Intangible/Event.payload']['primary_salutation_custom_format'])
            && !empty($event['Intangible/Event.payload']['primary_salutation_formatted_name'])
        ) {
            $data["primary_salutation"] = array(
                "custom_format"  => $event['Intangible/Event.payload']['primary_salutation_custom_format'],
                "formatted_name" => $event['Intangible/Event.payload']['primary_salutation_formatted_name']
            );
        }

        if (!empty($event['Intangible/Event.payload']['constituent_title'])) {
            $data["title"] = $event['Intangible/Event.payload']['constituent_title'];
        }

        if ($event['Intangible/Event.payload']['type'] == 'Organization') {
            $data['name'] = $event['Intangible/Event.payload']['organisation_name'];
        } else {
            $data['first'] = $event['Intangible/Event.payload']['address_first_name'];
            $data['last']  = $event['Intangible/Event.payload']['address_last_name'];
        }

        //If no valid address is available set "no_valid_address"
        if (!$this->_constituentHasAValidAddress($is_mailing_address_complete, $event)) {
            $data['no_valid_address'] = true;
        }

        return $data;
    }

    private function _constituentHasAValidAddress($is_mailing_address_complete, $event)
    {
        //Check mailing & billing address
        $is_biling_address_complete = $this->_isAddressComplete($event, 'billing');
        return $is_mailing_address_complete && $is_biling_address_complete;
    }

    private function _isAddressComplete($event, $address_type = 'address', $exclude_fields = [])
    {
        $required_address_fields = [
            'street', 'suburb', 'postcode', 'state', 'country'
        ];

        $is_address_complete = true;
        foreach ($required_address_fields as $address_field) {
            if (!in_array($address_field, $exclude_fields)) {
                if (empty($event['Intangible/Event.payload'][$address_type . '_' . $address_field])) {
                    $is_address_complete = false;
                    MachinePack::log('_isAddressComplete empty field :: ' . $address_field);
                }
            }
        }

        return $is_address_complete;
    }

    private function _isAddressEmpty($event, $address_type = 'address', $exclude_fields = [])
    {
        $required_address_fields = [
            'street', 'suburb', 'postcode', 'state',
            /*
             * 'country' Don't check the country because it's always available
             * We don't want to record address with the country field only
            */
        ];

        $is_address_empty = true;
        foreach ($required_address_fields as $address_field) {
            if (!in_array($address_field, $exclude_fields)) {
                if (!empty($event['Intangible/Event.payload'][$address_type . '_' . $address_field])) {
                    $is_address_empty = false;
                }
            }
        }

        return $is_address_empty;
    }

    private function _getConstituent($event, $token = false)
    {
        $auth_key = $event['Intangible/Sky.access_token'];
        if (isset($token['access_token'])) {
            $auth_key = $token['access_token'];
        }
        $headers = array(
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        );

        if (!empty($event['Intangible/Event.payload']['constituent_id'])) {
            $url    = 'https://api.sky.blackbaud.com/constituent/v1/constituents/' .
                urlencode($event['Intangible/Event.payload']['constituent_id']);
            $method = 'GET';

            return json_decode(
                $this->_skyApiCall(
                    $url,
                    $method,
                    $headers,
                    array()
                )
            );
        } else {
            return new Failure('Error - No valid Constituent ID provided.');
        }
    }

    private function _getConstituentSearch($event, $token = false, $search_key = 'address_email', $limit = 1)
    {
        $auth_key = $event['Intangible/Sky.access_token'];
        if (isset($token['access_token'])) {
            $auth_key = $token['access_token'];
        }
        $headers = array(
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        );

        if (isset($event['Intangible/Event.payload'][$search_key])
            && $event['Intangible/Event.payload'][$search_key]
        ) {
            $url    = 'https://api.sky.blackbaud.com/constituent/v1/constituents/search?search_text=' .
                urlencode($event['Intangible/Event.payload'][$search_key]) .
                '&include_inactive=false&limit=' . $limit;
            $method = 'GET';

            return json_decode(
                $this->_skyApiCall(
                    $url,
                    $method,
                    $headers,
                    array()
                )
            );
        } else {
            return new Failure('Constituent Search Error - Invalid payload key or value provided.');
        }
    }

    private function _getOrganisationConstituentSearch($event)
    {
        $constituentResponse = $this->_getConstituentSearch($event, false, 'organisation_name', 100);

        if (isset($constituentResponse->statusCode)) {
            //Token needs to be refreshed - will be handled in parent call/function
            if ($constituentResponse->statusCode == 401) {
                return $constituentResponse;
            } else {
                return new Failure($constituentResponse->message);
            }
        } else {
            return $constituentResponse;
        }
    }

    private function _getConstituentCustomSearch($event, $token, $return_single = false)
    {
        $auth_key = $event['Intangible/Sky.access_token'];
        if (isset($token['access_token'])) {
            $auth_key = $token['access_token'];
        }
        $headers = array(
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        );

        $params             = [];
        $payload_attributes = [];
        $mapping            = [
            'address_first_name' => 'first_name', 'address_last_name' => 'last_name',
            'address_email' => 'email', 'address_phone' => 'phone_number'
        ];

        switch ($event['Intangible/Event.payload']['constituent_lookup']) {
            case 'firstname_lastname':
                $payload_attributes = ['address_first_name', 'address_last_name'];
                break;
            case 'email_firstname_lastname':
                $payload_attributes = ['address_email', 'address_first_name', 'address_last_name'];
                break;
            case 'email_lastname':
                $payload_attributes = ['address_email', 'address_last_name'];
                break;
            case 'email_firstname_lastname_phone':
                $payload_attributes = ['address_email', 'address_first_name', 'address_last_name', 'address_phone'];
                break;
        }

        if (!empty($payload_attributes)) {
            foreach ($payload_attributes as $payload_attribute) {
                if (isset($mapping[$payload_attribute])) {
                    if (!empty($event['Intangible/Event.payload'][$payload_attribute])) {
                        $params[] = $mapping[$payload_attribute] . '='
                            . urlencode($event['Intangible/Event.payload'][$payload_attribute]);
                    }
                }
            }
        }

        if ($return_single) {
            $params[] = "limit=1";
        }

        if (!empty($params)) {
            $url    = 'https://api.sky.blackbaud.com/nxt-data-integration/v1/re/constituents/customsearch?'
                . implode("&", $params);
            $method = 'GET';

            $response = json_decode(
                $this->_skyApiCall(
                    $url,
                    $method,
                    $headers,
                    array()
                )
            );

            MachinePack::log(
                'ConstituentCustomSearch Event.payload :: ' . json_encode($event['Intangible/Event.payload'])
            );
            MachinePack::log('ConstituentCustomSearch Response :: ' . json_encode($response));

            //Mock the constituent search result
            if (isset($response->results)) {
                $count   = 0;
                $results = [];

                if ($event['Intangible/Event.payload']['constituent_exclude_deceased']) {
                    if (!empty($response->results)) {
                        MachinePack::log('$response->results :: ' . json_encode($response->results));
                        //If more then a single result returned, check for any deceased constituents
                        foreach ($response->results as $key => $record) {
                            if ($record->is_deceased) {
                                unset($response->results[$key]);
                            }
                        }
                    }

                    // Re-index the array elements
                    $response->results = array_values($response->results);
                }

                MachinePack::log('ConstituentCustomSearch filtered results :: ' . json_encode($response->results));

                if (!empty($response->results[0])) {
                    $result     = $response->results[0];
                    $result->id = $result->record_id;
                    $results    = [$result];
                    $count      = 1;
                }

                return (object) [
                    'count' => $count,
                    'value' => $results
                ];
            } else {
                if (!empty($response->statusCode)) {
                    return $response;
                } else {
                    return new Failure('Error while constituent customsearch endpoint :: ' . json_encode($response));
                }
            }
        } else {
            return new Failure('Error - No valid search parameters detected.');
        }
    }

    private function _getConstituentListByIds($event, $constituentResponse)
    {
        try {
            $ids = [];
            foreach ($constituentResponse->value as $constituent) {
                $ids[] = 'constituent_id=' . $constituent->id;
            }

            $method = 'GET';
            $url    = 'https://api.sky.blackbaud.com/constituent/v1/constituents?' .
                implode('&', $ids) . '&include_inactive=false';
            $data   = (object) array();

            $auth_key = $event['Intangible/Sky.access_token'];
            $headers  = array(
                "Content-Type: application/json",
                "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
                "Authorization: Bearer " . $auth_key
            );

            $constituentList = json_decode(
                $this->_skyApiCall(
                    $url,
                    $method,
                    $headers,
                    $data
                )
            );
        } catch (\Exception $e) {
            MachinePack::log('SKY API _getConstituentListByIds Error: ' . $e, 'debug');
        }

        if (isset($constituentList->count)) {
            return $constituentList;
        } else {
            return false;
        }
    }

    private function _createConstituentCustomFields($constituentId, $event, $headers)
    {
        $data   = [];
        $result = true;

        foreach ($event['Intangible/Event.payload']['custom_fields'] as $key => $value) {
            $field = [
                'category' => $key,
                'value' => $value
            ];

            if (!empty($event['Intangible/Event.payload']['gift_datetime'])) {
                $field['date'] = $event['Intangible/Event.payload']['gift_datetime'];
            }

            $data[] = $field;
        }

        if (!empty($data)) {
            try {
                $constituentCustomFieldsResponse = json_decode(
                    $this->_skyApiCall(
                        'https://api.sky.blackbaud.com/constituent/v1/constituents/'
                        . $constituentId . '/customfieldcollection',
                        'POST',
                        $headers,
                        $data
                    )
                );

                if (empty($constituentCustomFieldsResponse->count)) {
                    $result = false;
                }
            } catch (\Exception $ex) {
                //Todo log error
                $result = false;
            }
        }

        return $result;
    }

    private function _createConstituentCode($constituentId, $description, $headers)
    {
        $data   = [
            'constituent_id' => $constituentId,
            'description' => $description,
        ];
        $result = true;

        try {
            $constituentCustomFieldsResponse = json_decode(
                $this->_skyApiCall(
                    'https://api.sky.blackbaud.com/constituent/v1/constituentcodes',
                    'POST',
                    $headers,
                    $data
                )
            );

            if (empty($constituentCustomFieldsResponse)) {
                $result = false;
            }
        } catch (\Exception $ex) {
            //Todo log error
            $result = false;
        }

        return $result;
    }

    private function _patchConstituent($constituentId, $event, $headers)
    {
        if (isset($event['Intangible/Event.payload']['update_constituent_name'])) {
            $this->_updateConstituentName($constituentId, $event, $headers);
        }

        if (isset($event['Intangible/Event.payload']['update_constituent_phone'])) {
            $this->_updateConstituentPhone($constituentId, $event, $headers);
        }

        //Don't update the constituent addresses unless specifically requested
        if (isset($event['Intangible/Event.payload']['update_constituent_addresses'])) {
            //Check whether the mailing address is complete or not
            $is_address_complete = $this->_isAddressComplete($event);
            if ($is_address_complete) {
                $this->_updateConstituentAddresses($constituentId, $event, $headers);
            } else {
                //Create an incomplete address if not empty address available
                if ($event['Intangible/Event.payload']['constituent_record_incomplete_address'] === true) {
                    if (!empty($event['Intangible/Event.payload']['constituent_incomplete_address_type'])) {
                        $is_address_empty = $this->_isAddressEmpty($event);
                        if (!$is_address_empty) {
                            $this->_createConstituentIncompleteAddress(
                                $constituentId,
                                $event['Intangible/Event.payload']['constituent_incomplete_address_type'],
                                $event,
                                $headers,
                                'address'
                            );
                        }
                    }
                }
            }
        }
    }

    private function _updateConstituentName($constituentId, $event, $headers)
    {
        $constituentID = $constituentId;
        $method        = 'PATCH';
        $url           = 'https://api.sky.blackbaud.com/constituent/v1/constituents/' . $constituentID;
        $data          = (object) array(
            "first" => $event['Intangible/Event.payload']['address_first_name'],
            "last" => $event['Intangible/Event.payload']['address_last_name'],
        );

        $response = $this->_skyApiCall(
            $url,
            $method,
            $headers,
            $data
        );
    }

    private function _updateConstituentPhone($constituentId, $event, $headers)
    {
        if (!empty($event['Intangible/Event.payload']['address_phone'])
            && (!empty($event['Intangible/Event.payload']['phone_type']))
        ) {
            $constituentPhoneNumbers = $this->_getConstituentPhoneNumbers($constituentId, $event);
            if (isset($constituentPhoneNumbers->count) && $constituentPhoneNumbers->count > 0) {
                foreach ($constituentPhoneNumbers->value as $constituentPhoneNumber) {
                    if ($constituentPhoneNumber->type == $event['Intangible/Event.payload']['phone_type']) {
                        $method = 'PATCH';
                        $url    = 'https://api.sky.blackbaud.com/constituent/v1/phones/' . $constituentPhoneNumber->id;
                        $data   = (object) array(
                            "number" => $event['Intangible/Event.payload']['address_phone']
                        );

                        $response = $this->_skyApiCall(
                            $url,
                            $method,
                            $headers,
                            $data
                        );
                    }
                }
            }
        }
    }

    private function _updateConstituentAddresses($constituentId, $event, $headers)
    {
        //There should always be at least 1 address assigned to the constituent
        //Each ReNxt account may have a different set of address types
        $primary_address_type = $event['Intangible/Event.payload']['constituent_primary_address_type'] ?? false;
        $former_address_type  = $event['Intangible/Event.payload']['constituent_former_address_type'] ?? false;
        //Address type are required. Abort if not provided.
        if (empty($primary_address_type) || empty($former_address_type)) {
            return;
        }

        //Get list of all constituent addresses
        $addresses                  = $this->_getConstituentAddresses($constituentId, $headers);
        $previousMatchingAddresses  = null;
        $previousPreferredAddresses = [];
        $isNewPrimaryAddressUnique  = true;

        if (!empty($addresses)) {
            foreach ($addresses as $address) {
                $isDuplicate = $this->_isConstituentAddressDuplicate($address, $event);
                if ($isDuplicate) {
                    $previousMatchingAddresses[] = $address;
                    if ($address->preferred === true) {
                        $isNewPrimaryAddressUnique = false;
                    }
                }

                //TODO - Instead of checking primary address type, we need to check the "preferred" flag
                if ($address->preferred === true) {
                    //Save the address into array - patch them all as "$former_address_type" later
                    $previousPreferredAddresses[] = $address;
                }
            }
        }

        MachinePack::log("_updateConstituentAddresses $addresses :: " . json_encode($addresses));
        MachinePack::log(
            "_updateConstituentAddresses $previousMatchingAddresses :: " . json_encode($previousMatchingAddresses)
        );
        MachinePack::log(
            "_updateConstituentAddresses $previousPreferredAddresses :: " . json_encode($previousPreferredAddresses)
        );
        MachinePack::log(
            "_updateConstituentAddresses $isNewPrimaryAddressUnique :: " . json_encode($isNewPrimaryAddressUnique)
        );
        MachinePack::log(
            "_updateConstituentAddresses method :: "
            . json_encode($event['Intangible/Event.payload']['update_constituent_primary_address_method'])
        );

        if ($isNewPrimaryAddressUnique) {
            if ($event['Intangible/Event.payload']['update_constituent_primary_address_method'] == 'always_new') {
                MachinePack::log('creating new primary address - always_new method');
                $this->_createConstituentPrimaryAddress($constituentId, $primary_address_type, $event, $headers);
            }

            //Method was previously not available - use default logic if method not provided
            if (empty($event['update_constituent_primary_address_method'])
                || $event['update_constituent_primary_address_method'] == 'update_or_create'
            ) {
                MachinePack::log('checking previous addresses - update_or_create method');
                if (!empty($previousMatchingAddresses)) {
                    //Probably better to only update the last in the list
                    $latestPreviousMatchingAddress = end($previousMatchingAddresses);
                    MachinePack::log(
                        'previous matching address found, updating previousaddress :: '
                        . json_encode($latestPreviousMatchingAddress)
                    );
                    $this->_patchConstituentAddress(
                        $latestPreviousMatchingAddress,
                        $headers,
                        [
                            'type' => $primary_address_type, 'preferred' => true,
                            'start' => date('c'), 'do_not_mail' => false
                        ]
                    );
                } else {
                    MachinePack::log('no previous matching address available, creating a new one');
                    //If no previous matching address available, create a new one
                    $this->_createConstituentPrimaryAddress($constituentId, $primary_address_type, $event, $headers);
                }
            }
        }/* else {
            //Do nothing, the current primary matches the new one, no action required
        }*/

        //If there are any primary type addresses mark them all as former type
        if (!empty($previousPreferredAddresses)) {
            foreach ($previousPreferredAddresses as $previous_preferred_address) {
                MachinePack::log('Patching previous primary address :: ' . json_encode($previous_preferred_address));
                //TODO When trying to set the preferred flag to false from true we are getting:
                //"The address is marked as 'Preferred' and that cannot be changed." error
                //It might be just a RFDS QLD configuration though...
                $address_update_params = [
                    'type' => $former_address_type,
                    'preferred' => false,
                    'end' => date('c'),
                    'do_not_mail' => true
                ];

                if (!$event['Intangible/Event.payload']['constituent_former_address_update_address_type']) {
                    unset($address_update_params['type']);
                }
                if (!$event['Intangible/Event.payload']['constituent_former_address_do_not_mail_set_false']) {
                    unset($address_update_params['do_not_mail']);
                }

                $this->_patchConstituentAddress(
                    $previous_preferred_address,
                    $headers,
                    $address_update_params
                );
                $this->_previous_preferred_address = $previous_preferred_address;
                //TODO Probably break after the first one?
            }
        }
    }

    private function _getConstituentAddresses($constituentId, $headers)
    {
        $method = 'GET';
        $url    = 'https://api.sky.blackbaud.com/constituent/v1/constituents/' .
            $constituentId . '/addresses?include_inactive=false';
        $data   = (object) array();

        $constituentAddressesResponse = json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                $data
            )
        );

        if (!empty($constituentAddressesResponse->count)) {
            return $constituentAddressesResponse->value;
        } else {
            return false;
        }
    }

    private function _isConstituentAddressDuplicate($address, $event, $addressType = 'address', $nonEmptyOnly = false)
    {
        $payload      = $event['Intangible/Event.payload'];
        $address_keys = [
            'address_lines' => 'street',
            'city' => 'suburb',
            'state' => 'state',
            'country' => 'country',
            'postal_code' => 'postcode'
        ];

        foreach ($address_keys as $address_key => $payload_key) {
            if ($nonEmptyOnly && empty($payload[$addressType . '_' . $payload_key])) {
                //Do nothing (possible match)
                continue;
            } else {
                $is_address_line   = $address_key == 'address_lines';
                $address_key_value = $this->_getAddressSearchSample(
                    $address->{$address_key},
                    $is_address_line
                );
                $payload_key_value = $this->_getAddressSearchSample(
                    $payload[$addressType . '_' . $payload_key],
                    $is_address_line
                );

                //Compare the samples
                if ($address_key_value != $payload_key_value) {
                    return false;
                }
            }
        }

        return true;
    }

    private function _getAddressSearchSample($value, $isAddressLine = false)
    {
        if ($isAddressLine) {
            $list = $this->_getStreetaddressAbbrevationsList();

            //Break the string into words
            $words  = explode(' ', strtolower(trim($value)));
            $output = [];
            foreach ($words as $key => $word) {
                if (isset($list[$word])) {
                    $output[$key] = $list[$word];
                } else {
                    $output[$key] = $word;
                }
            }

            return implode(' ', $output);
        } else {
            return strtolower(trim($value));
        }
    }

    private function _getStreetaddressAbbrevationsList()
    {
        $data = [
            'alley' => ['alley', 'ally', 'ally.'],
            'arcade' => ['arcade', 'arc', 'arc.'],
            'boulevard' => ['boulevard', 'bvd', 'bvd.'],
            'circuit' => ['circuit', 'cct', 'cct.'],
            'close' => ['close', 'cl', 'cl.', 'cls', 'cls.'],
            'court' => ['court', 'cr', 'cr.', 'crt', 'crt.'],
            'crescent' => ['crescent', 'cres', 'cres.', 'crs', 'crs.'],
            'drive' => ['drive', 'dr', 'dr.'],
            'esplanade' => ['esplanade', 'esp', 'esp.'],
            'grove' => ['grove', 'gr', 'gr.'],
            'highway' => ['highway', 'hwy', 'hwy.'],
            'lane' => ['lane', 'lne', 'lne.'],
            'parade' => ['parade', 'pde', 'pde.'],
            'place' => ['place', 'pl', 'pl.'],
            'road' => ['road', 'rd', 'rd.'],
            'square' => ['square', 'sq', 'sq.'],
            'street' => ['street', 'str', 'str.', 'st', 'st.'],
            'terrace' => ['terrace', 'tce', 'tce.'],
            'way' => ['way', 'wy', 'wy.'],
        ];

        $list = [];
        foreach ($data as $word => $abbrList) {
            foreach ($abbrList as $abb) {
                $list[$abb] = $word;
            }
        }

        return $list;
    }

    private function _createConstituentPrimaryAddress($constituentId, $primary_address_type, $event, $headers)
    {
        $method = 'POST';
        $url    = 'https://api.sky.blackbaud.com/constituent/v1/addresses';
        $data   = (object) array(
            "constituent_id" => $constituentId,
            "address_lines"  => $event['Intangible/Event.payload']['address_street'],
            "city"           =>  $event['Intangible/Event.payload']['address_suburb'],
            "state"          => $event['Intangible/Event.payload']['address_state'],
            "country"        => $event['Intangible/Event.payload']['address_country'],
            "do_not_mail"    => false,
            "postal_code"    => $event['Intangible/Event.payload']['address_postcode'],
            "preferred"      => true,
            "type"           => $primary_address_type
        );

        if (!empty($event['Intangible/Event.payload']['constituent_address_set_start_date'])) {
            $data->start = date('c');
        }

        //Use the primary_address_information_source for primary addresses
        if (!empty($event['Intangible/Event.payload']['constituent_address_set_information_source'])
            && !empty($event['Intangible/Event.payload']['primary_address_information_source'])
        ) {
            $data->information_source
                = $event['Intangible/Event.payload']['primary_address_information_source'];
        }

        $response = json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                $data
            )
        );
    }

    private function _createConstituentIncompleteAddress(
        $constituentId,
        $incomplete_address_type,
        $event,
        $headers,
        $event_address_type = 'address'
    ) {
        $method = 'POST';
        $url    = 'https://api.sky.blackbaud.com/constituent/v1/addresses';

        $address_fields = [
            'address_lines' => 'street', 'city' => 'suburb',
            'state' => 'state', 'country' => 'country',
            'postal_code' => 'postcode'
        ];

        $data = (object) array(
            "constituent_id" => $constituentId,
            "do_not_mail" => true,
            "preferred" => false,
            "type" => $incomplete_address_type
        );

        $key_prefix = $event_address_type;
        foreach ($address_fields as $address_field => $payload_key) {
            if (!empty($event['Intangible/Event.payload'][$key_prefix . '_' . $payload_key])) {
                $data->{$address_field} = $event['Intangible/Event.payload'][$key_prefix . '_' . $payload_key];
            }
        }

        if (!empty($event['Intangible/Event.payload']['constituent_address_set_start_date'])) {
            $data->start = date('c');
        }

        //Use the primary_address_information_source for primary addresses
        $information_source_key = $event_address_type == 'billing' ? 'billing_address_information_source' :
            'primary_address_information_source';
        if (!empty($event['Intangible/Event.payload']['constituent_address_set_information_source'])
            && !empty($event['Intangible/Event.payload'][$information_source_key])
        ) {
            $data->information_source
                = $event['Intangible/Event.payload'][$information_source_key];
        }

        MachinePack::log('Incomplete address created - data :: ' . json_encode($data));

        $response = json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                $data
            )
        );
    }

    private function _createConstituentBillingAddress(
        $constituentId,
        $billing_address_type,
        $set_billing_as_primary,
        $previous_primary_addresses,
        $event,
        $headers
    ) {
        $method = 'POST';
        $url    = 'https://api.sky.blackbaud.com/constituent/v1/addresses';
        $data   = (object) array(
            "constituent_id" => $constituentId,
            "address_lines" => $event['Intangible/Event.payload']['billing_street'],
            "city" =>  $event['Intangible/Event.payload']['billing_suburb'],
            "state" => $event['Intangible/Event.payload']['billing_state'],
            "country" => $event['Intangible/Event.payload']['billing_country'],
            "do_not_mail" => $event['Intangible/Event.payload']['set_send_mail_for_new_complete_constituent_address'] ?
                false :  !$set_billing_as_primary,
            "postal_code" => $event['Intangible/Event.payload']['billing_postcode'],
            "preferred" => $set_billing_as_primary,
            "type" => $billing_address_type
        );

        if (!empty($event['Intangible/Event.payload']['constituent_address_set_start_date'])) {
            $data->start = date('c');
        }

        //Use the billing_address_information_source for billing addresses
        if (!empty($event['Intangible/Event.payload']['constituent_address_set_information_source'])
            && !empty($event['Intangible/Event.payload']['billing_address_information_source'])
        ) {
            $data->information_source
                = $event['Intangible/Event.payload']['billing_address_information_source'];
        }

        $response = json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                $data
            )
        );

        //If a new primary Gaddress has been set, set the previous addresses as previous
        if (!empty($response->id) && $set_billing_as_primary && !empty($previous_primary_addresses)) {
            foreach ($previous_primary_addresses as $previous_preferred_address) {
                MachinePack::log('Patching previous primary address :: ' . json_encode($previous_primary_addresses));
                $former_address_type = $event['Intangible/Event.payload']['constituent_former_address_type'] ?? false;
                //TODO When trying to set the preferred flag to false from true we are getting:
                //"The address is marked as 'Preferred' and that cannot be changed." error
                //It might be just a RFDS QLD configuration though...
                $address_update_params = [
                    'type' => $former_address_type,
                    'preferred' => false,
                    'end' => date('c'),
                    'do_not_mail' => true
                ];

                if (!$event['Intangible/Event.payload']['constituent_former_address_update_address_type']) {
                    unset($address_update_params['type']);
                }
                if (!$event['Intangible/Event.payload']['constituent_former_address_do_not_mail_set_false']) {
                    unset($address_update_params['do_not_mail']);
                }

                $this->_patchConstituentAddress(
                    $previous_preferred_address,
                    $headers,
                    $address_update_params
                );
            }
        }

        MachinePack::log('_createConstituentBillingAddress response :: ' . json_encode($response));
        MachinePack::log('address data :: ' . json_encode($data));
        MachinePack::log('event data :: ' . json_encode($event));
    }

    private function _patchNewConstituentPrimaryAddress($event, $constituentId, $headers)
    {
        //If the information source (or other createConstituent method unsupported field) is not empty
        //Make a subsequent call to patch the address

        if (!empty($event['Intangible/Event.payload']['constituent_address_set_information_source'])
            && !empty($event['Intangible/Event.payload']['primary_address_information_source'])
        ) {
            $constituentAddresses = $this->_getConstituentAddresses($constituentId, $headers);
            MachinePack::log(
                '_patchNewConstituentAddress $constituentAddresses :: ' . json_encode($constituentAddresses)
            );
            foreach ($constituentAddresses as $constituentAddress) {
                if ($constituentAddress->preferred === true) {
                    $preferredAddress = $constituentAddress;
                    $patchData        = [];

                    $patchData['information_source']
                        = $event['Intangible/Event.payload']['primary_address_information_source'];

                    MachinePack::log(
                        '_patchNewConstituentAddress - $preferredAddress->id :: ' . json_encode($preferredAddress->id)
                    );
                    MachinePack::log('_patchNewConstituentAddress - $patchData :: ' . json_encode($patchData));

                    $this->_patchConstituentAddress($preferredAddress, $headers, $patchData);
                }
            }
        }
    }

    private function _patchConstituentAddress($address, $headers, $data)
    {
        MachinePack::log('Patching previous constituent address $data :: ' . json_encode($data));

        $method = 'PATCH';
        $url    = 'https://api.sky.blackbaud.com/constituent/v1/addresses/' . $address->id;

        $response = json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                $data
            )
        );

        MachinePack::log('_patchConstituentAddress $response :: ' . json_encode($response));
    }

    private function _recordNewConstituentIncompleteAddress($constituentId, $event, $headers)
    {
        $is_mailing_address_empty = $this->_isAddressEmpty($event);
        //If the address is completely empty don't create any address at all
        MachinePack::log(
            '_getConstituentData $is_mailing_address_empty :: ' . json_encode($is_mailing_address_empty)
        );

        if (!$is_mailing_address_empty) {
            //Check whether the incomplete address should be created
            $incomplete_address_type
                = $event['Intangible/Event.payload']['constituent_incomplete_address_type'] ?? false;

            if ($event['Intangible/Event.payload']['constituent_record_incomplete_address'] === true
                && !empty($incomplete_address_type)
            ) {
                $this->_createConstituentIncompleteAddress(
                    $constituentId,
                    $incomplete_address_type,
                    $event,
                    $headers,
                    'address'
                );
            }
        }
    }

    private function _deleteConstituentAddress($address, $headers)
    {
        $method = 'DELETE';
        $url    = 'https://api.sky.blackbaud.com/constituent/v1/addresses/' . $address->id;
        $data   = null;

        $response = json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                $data
            )
        );
    }

    private function _createAcknowledgeeConstituent($event, $token_response)
    {
        $acknowledgee_search_event = [
            'Intangible/Sky.access_token' => $event['Intangible/Sky.access_token'],
            'Intangible/Event.payload' => [
                'email' =>
                    $event['Intangible/Event.payload']['acknowledgee']['email']
            ]
        ];

        MachinePack::log(
            'MP SKY API $acknowledgee_search_event :: '
            . json_encode($acknowledgee_search_event)
        );

        $acknowledgee_search_event_response = $this->_getConstituentSearch(
            $acknowledgee_search_event,
            $token_response,
            'email',
            1
        );

        MachinePack::log(
            'MP SKY API $acknowledgee_search_event_response response :: '
            . json_encode($acknowledgee_search_event_response)
        );

        if (isset($acknowledgee_search_event_response->count)
            && $acknowledgee_search_event_response->count > 0
        ) {
            $acknowledgee_constituent_id
                = $acknowledgee_search_event_response->value[0]->id;
        } else {
            $acknowledgee_full_name    = trim(
                implode(
                    ' ',
                    [
                       $event['Intangible/Event.payload']['acknowledgee']['name'],
                       $event['Intangible/Event.payload']['acknowledgee']['last']
                    ]
                )
            );
            $acknowledgee_create_event = [
                'Intangible/Sky.access_token' => $event['Intangible/Sky.access_token'],
                'constituent_data' => [
                    'name' => $acknowledgee_full_name,
                    'first' => $event['Intangible/Event.payload']['acknowledgee']['name'],
                    'last' => $event['Intangible/Event.payload']['acknowledgee']['last'],
                    "email" => [
                        "address" => $event['Intangible/Event.payload']['acknowledgee']['email'],
                        "do_not_email" => false,
                        "inactive" => false,
                        "primary" => true,
                        "type" => "Email"
                    ],
                    'type' => 'Individual'
                ]
            ];

            if (!empty($event['Intangible/Event.payload']['primary_addressee_configuration_id'])) {
                $acknowledgee_create_event["constituent_data"]["primary_addressee"] = array(
                    "configuration_id" => $event['Intangible/Event.payload']['primary_addressee_configuration_id']
                );
            }

            if (!empty($event['Intangible/Event.payload']['primary_salutation_custom_format'])
                && !empty($event['Intangible/Event.payload']['primary_salutation_formatted_name'])
            ) {
                $acknowledgee_create_event["constituent_data"]["primary_salutation"] = array(
                    "custom_format"  => $event['Intangible/Event.payload']['primary_salutation_custom_format'],
                    "formatted_name" => $event['Intangible/Event.payload']['primary_salutation_formatted_name']
                );
            }

            MachinePack::log(
                'MP SKY API $acknowledgee_create_event :: '
                . json_encode($acknowledgee_create_event)
            );

            $acknowledgee_create_event_response = $this->_createConstituentSimple(
                $acknowledgee_create_event,
                $token_response
            );

            MachinePack::log(
                'MP SKY API $acknowledgee_create_event_response :: '
                . json_encode($acknowledgee_create_event_response)
            );

            $acknowledgee_constituent_id = empty($acknowledgee_create_event_response->id) ?
                false : $acknowledgee_create_event_response->id;

            if (!empty($event['Intangible/Event.payload']['tribute']['acknowledgee_constituent_code'])
                && $acknowledgee_constituent_id
            ) {
                $headers = array(
                    "Content-Type: application/json",
                    "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
                    "Authorization: Bearer " . (
                    empty($token_response['access_token']) ?
                        $event['Intangible/Sky.access_token'] : $token_response['access_token']
                    )
                );

                $acknowledgee_create_constituent_code_response = $this->_createConstituentCode(
                    $acknowledgee_constituent_id,
                    $event['Intangible/Event.payload']['tribute']['acknowledgee_constituent_code'],
                    $headers
                );

                MachinePack::log(
                    'MP SKY API $acknowledgee_create_constituent_code_response data :: '
                    . json_encode($event['Intangible/Event.payload']['tribute']['acknowledgee_constituent_code'])
                );
                MachinePack::log(
                    'MP SKY API $acknowledgee_create_constituent_code_response :: '
                    . json_encode($acknowledgee_create_constituent_code_response)
                );
            }
        }

        return $acknowledgee_constituent_id;
    }

    private function _updateConstituentBillingAddress(
        $constituentId,
        $existing_donor,
        $billing_as_primary_mailing,
        $event,
        $headers
    ) {
        MachinePack::log(
            '_updateConstituentBillingAddress $event :: ' . json_encode($event['Intangible/Event.payload'])
        );
        MachinePack::log('$existing_donor :: ' . json_encode($existing_donor));
        MachinePack::log('$billing_as_primary_mailing :: ' . json_encode($billing_as_primary_mailing));
        MachinePack::log('_isAddressComplete :: ' . json_encode($this->_isAddressComplete($event, 'billing')));

        if ($this->_isAddressComplete($event, 'billing')) {
            //1. Check whether the billing address matches the current preferred address
            $is_billing_address_unique    = false;
            $addresses                    = $this->_getConstituentAddresses($constituentId, $headers);
            $previous_primary_addresses   = [];
            $current_primary_address      = false;
            $primary_matches_billing      = false;
            $non_matching_addresses_count = 0;
            if (!empty($addresses)) {
                foreach ($addresses as $address) {
                    //Check whether the address is a duplicate of the billing address
                    if ($this->_isConstituentAddressDuplicate($address, $event, 'billing')) {
                        $is_billing_address_unique = false;
                        if ($address->preferred === true) {
                            $primary_matches_billing = true;
                        }
                    } else {
                        $non_matching_addresses_count++;
                    }

                    if ($address->preferred === true) {
                        $current_primary_address      = $address;
                        $previous_primary_addresses[] = $address;
                    }
                }

                if ($non_matching_addresses_count == count($addresses)) {
                    $is_billing_address_unique = true;
                }
            }

            if ($billing_as_primary_mailing) {
                $billing_address_type = $event['Intangible/Event.payload']['address_type'];
            } else {
                $billing_address_type = $event['Intangible/Event.payload']['constituent_create_billing_address_type']
                    ?? false;
            }

            MachinePack::log('$billing_address_type :: ' . json_encode($billing_address_type));
            MachinePack::log('$is_billing_address_unique :: ' . json_encode($is_billing_address_unique));
            MachinePack::log('$current_primary_address :: ' . json_encode($current_primary_address));
            MachinePack::log(
                '$this->_previous_preferred_address :: ' . json_encode($this->_previous_preferred_address)
            );
            MachinePack::log('$previous_primary_addresses :: ' . json_encode($previous_primary_addresses));
            MachinePack::log('$primary_matches_billing :: ' . json_encode($primary_matches_billing));


            //Check whether billing address matches the current primary address
            if ($primary_matches_billing && $existing_donor) {
                //Check whether the billing address matches the previous address - the one that has been swapped
                //This should NEVER HAPPEN - if the billing matches the new primary, it can't match the previous primary
                if (!empty($this->_previous_preferred_address)) {
                    $key = 'constituent_create_billing_address_when_matching_previous_primary_address';
                    if ($event['Intangible/Event.payload'][$key] === true) {
                        if ($this->_isConstituentAddressDuplicate(
                            $this->_previous_preferred_address,
                            $event,
                            'billing'
                        )
                        ) {
                            //Create new billing address
                            MachinePack::log(
                                'creating billing address for an existing donor with previous preferred address'
                            );
                            $this->_createConstituentBillingAddress(
                                $constituentId,
                                $billing_address_type,
                                $billing_as_primary_mailing,
                                $previous_primary_addresses,
                                $event,
                                $headers
                            );
                        }
                    }
                }
                //If not, don't create a new billing address even if it's unique
            } else {
                //Billing address needs to be unique & address type provided
                if ($is_billing_address_unique && $billing_address_type) {
                    //Create new billing address
                    MachinePack::log('creating unique billing address for a new donor');
                    $this->_createConstituentBillingAddress(
                        $constituentId,
                        $billing_address_type,
                        $billing_as_primary_mailing,
                        $previous_primary_addresses,
                        $event,
                        $headers
                    );
                }
            }
        } else {
            //If the billing address is not complete, record it as incomplete if it doesn't match
            //the provided mailing address
            $incomplete_address_type
                = $event['Intangible/Event.payload']['constituent_incomplete_address_type'] ?? false;

            if ($event['Intangible/Event.payload']['constituent_record_incomplete_address'] === true
                && !empty($incomplete_address_type)
            ) {
                if (!$this->_isAddressEmpty($event, 'billing')) {
                    $billing_address                = new \stdClass();
                    $billing_address->address_lines = $event['Intangible/Event.payload']['billing_street'] ?? '';
                    $billing_address->city          = $event['Intangible/Event.payload']['billing_suburb'] ?? '';
                    $billing_address->postal_code   = $event['Intangible/Event.payload']['billing_postcode'] ?? '';
                    $billing_address->state         = $event['Intangible/Event.payload']['billing_state'] ?? '';
                    $billing_address->country       = $event['Intangible/Event.payload']['billing_country'] ?? '';

                    MachinePack::log('Comparing non-empty incomplete billing address with provided mailing address');
                    //TODO - What if some of the fields are empty but the rest of the fields are the same?
                    //We should compare available fields only
                    $is_billing_address_unique = !$this->_isConstituentAddressDuplicate($billing_address, $event);
                    MachinePack::log('$is_billing_address_unique :: ' . json_encode($is_billing_address_unique));
                    if ($is_billing_address_unique) {
                        $this->_createConstituentIncompleteAddress(
                            $constituentId,
                            $incomplete_address_type,
                            $event,
                            $headers,
                            'billing'
                        );
                    }
                }
            }
        }
    }

    private function _createRelationship($constituentId, $relationId, $headers, $relationshipData = [])
    {
        $is_organization_contact = !empty($relationshipData['gift_assign_to_organisation']);

        try {
            $method = 'POST';
            $url    = 'https://api.sky.blackbaud.com/constituent/v1/relationships';
            $data   = (object) array(
                "comment" => $event["Intangible/Event.payload"]['relationship_comment'] ?? "",
                "constituent_id" => $constituentId,
                "is_organization_contact" => $is_organization_contact,
                "reciprocal_type" => $event["Intangible/Event.payload"]['reciprocal_type'],
                "start" => [
                    "d" => date('d'),
                    "m" => date('n'),
                    "y" => date('Y')
                ],
                "relation_id" => $relationId,
                "type" => $event["Intangible/Event.payload"]['relationship_type']
            );

            if (!empty($relationshipData['relationship_comment'])) {
                $data->comment = $relationshipData['relationship_comment'];
            }

            if (!empty($relationshipData['reciprocal_type'])) {
                $data->reciprocal_type = $relationshipData['reciprocal_type'];
            }

            if (!empty($relationshipData['relationship_type'])) {
                $data->type = $relationshipData['relationship_type'];
            }

            $response = json_decode(
                $this->_skyApiCall(
                    $url,
                    $method,
                    $headers,
                    $data
                )
            );

            MachinePack::log('SKY API _createRelationship debug $data :: ' . json_encode($data));
            MachinePack::log('SKY API _createRelationship debug $response :: ' . json_encode($response));
        } catch (\Exception $e) {
            MachinePack::log('SKY API _createRelationship Error: ' . $e->getMessage(), 'debug');
        }

        return $response->id ?? false;
    }

    private function _getTributeEvent($event, $honoree_constituent_id)
    {
        $tribute = [
            'tribute_type_id' => $event['Intangible/Event.payload']['tribute']['type'],
            'constituent_record_id' => $honoree_constituent_id,
            'is_active' => true,
            'start_date' => [
                "d" => date('j'),
                "m" => date('n'),
                "y" => date('Y')
            ]
        ];

        if (!empty($event['Intangible/Event.payload']['tribute']['default_fund_id'])) {
            $tribute['default_fund_id']
                = $event['Intangible/Event.payload']['tribute']['default_fund_id'];
        }

        if (!empty($event['Intangible/Event.payload']['tribute']['description'])) {
            $tribute['description'] = $event['Intangible/Event.payload']['tribute']['description'];
        }

        return $tribute;
    }

    private function _createTribute($event, $tribute, $token = false)
    {
        $auth_key = $event['Intangible/Sky.access_token'];
        if (isset($token['access_token'])) {
            $auth_key = $token['access_token'];
        }
        $headers = array(
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        );

        $method = 'POST';
        $url    = 'https://api.sky.blackbaud.com/nxt-data-integration/v1/re/tribute';

        $data            = $tribute;
        $tributeResponse = json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                $data
            )
        );

        return $tributeResponse;
    }

    private function _createGiftTribute($event, $tribute, $token = false)
    {
        $auth_key = $event['Intangible/Sky.access_token'];
        if (isset($token['access_token'])) {
            $auth_key = $token['access_token'];
        }
        $headers = array(
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        );

        $method = 'POST';
        $url    = 'https://api.sky.blackbaud.com/nxt-data-integration/v1/re/gifttribute';

        $data            = $tribute;
        $tributeResponse = json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                $data
            )
        );

        return $tributeResponse;
    }

    private function _createSimpleHonoree($event, $token_response)
    {
        if (!empty($event['Intangible/Event.payload']['tribute']['honoree_secondary_name'])) {
            $honoree_first_name = $event['Intangible/Event.payload']['tribute']['honoree_name'];
            $honoree_last_name  = $event['Intangible/Event.payload']['tribute']['honoree_secondary_name'];
        } else {
            $honoree_name_pieces = explode(
                ' ',
                $event['Intangible/Event.payload']['tribute']['honoree_name']
            );
            $honoree_first_name  = current($honoree_name_pieces);
            $honoree_last_name   = array_pop($honoree_name_pieces);
        }

        $honoree_create_event = [
            'Intangible/Sky.access_token' => $event['Intangible/Sky.access_token'],
            'constituent_data' => [
                'name' => $event['Intangible/Event.payload']['tribute']['honoree_name'],
                'first' => $honoree_first_name,
                'last' => $honoree_last_name,
                'type' => 'Individual',
                'is_memorial' => !empty(
                    $event['Intangible/Event.payload']['tribute']['honoree_is_memorial']
                )
            ]
        ];

        if (!empty($event['Intangible/Event.payload']['tribute']['honoree_email'])) {
            $honoree_create_event['constituent_data']['email']
                = [
                    "address" => $event['Intangible/Event.payload']['tribute']['honoree_email'],
                    "do_not_email" => false,
                    "inactive" => false,
                    "primary" => true,
                    "type" => "Email"
                ];
        }

        if (!empty($event['Intangible/Event.payload']['primary_addressee_configuration_id'])) {
            $honoree_create_event["constituent_data"]["primary_addressee"] = array(
                "configuration_id" => $event['Intangible/Event.payload']['primary_addressee_configuration_id']
            );
        }

        if (!empty($event['Intangible/Event.payload']['primary_salutation_custom_format'])
            && !empty($event['Intangible/Event.payload']['primary_salutation_formatted_name'])
        ) {
            $honoree_create_event["constituent_data"]["primary_salutation"] = array(
                "custom_format"  => $event['Intangible/Event.payload']['primary_salutation_custom_format'],
                "formatted_name" => $event['Intangible/Event.payload']['primary_salutation_formatted_name']
            );
        }

        MachinePack::log(
            'MP SKY API _createConstituentSimple $honoree_create_event :: '
            . json_encode($honoree_create_event)
        );

        $honoree_create_event_response = $this->_createConstituentSimple(
            $honoree_create_event,
            $token_response
        );

        MachinePack::log(
            'MP SKY API $honoree_create_event_response response :: '
            . json_encode($honoree_create_event_response)
        );

        $honoree_constituent_id = $honoree_create_event_response->id ?? false;
        if (!empty($event['Intangible/Event.payload']['tribute']['honoree_constituent_code'])
            && $honoree_constituent_id
        ) {
            $headers = array(
                "Content-Type: application/json",
                "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
                "Authorization: Bearer " . (
                empty($token_response['access_token']) ?
                    $event['Intangible/Sky.access_token'] : $token_response['access_token']
                )
            );
            $honoree_create_constituent_code_response = $this->_createConstituentCode(
                $honoree_constituent_id,
                $event['Intangible/Event.payload']['tribute']['honoree_constituent_code'],
                $headers
            );

            MachinePack::log(
                'MP SKY API $honoree_create_constituent_code data :: '
                . json_encode($event['Intangible/Event.payload']['tribute']['honoree_constituent_code'])
            );
            MachinePack::log(
                'MP SKY API $honoree_create_constituent_code_response response :: '
                . json_encode($honoree_create_constituent_code_response)
            );
        }

        return $honoree_constituent_id;
    }

    private function _createTributeAcknowledgee($event, $tributeAcknowledgee, $token = false)
    {
        $auth_key = $event['Intangible/Sky.access_token'];
        if (isset($token['access_token'])) {
            $auth_key = $token['access_token'];
        }
        $headers = array(
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        );

        $method = 'POST';
        $url    = 'https://api.sky.blackbaud.com/nxt-data-integration/v1/re/tribute/acknowledgee';

        $data            = $tributeAcknowledgee;
        $tributeResponse = json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                $data
            )
        );

        return $tributeResponse;
    }

    /**
     * Create a gift payment
     * @param Crmsync $event
     */

    private function _createGift($event, $constituentId, $batch = false, $token = false)
    {
        try {
            $auth_key = $event['Intangible/Sky.access_token'];
            if (isset($token['access_token'])) {
                $auth_key = $token['access_token'];
            }

            $url = 'https://api.sky.blackbaud.com/gift/v1/gifts';

            $headers = array(
                "Content-Type: application/json",
                "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
                "Authorization: Bearer " . $auth_key
            );

            $payment_method = $event['Intangible/Event.payload']['gift_payment_method'] ?? "Credit Card";

            $data = array(
                'amount' => array(
                    'value' => $event['Intangible/Event.payload']['donation_amount']
                ),
                'constituent_id' => $constituentId,
                'date' => $event['Intangible/Event.payload']['gift_datetime'] ?? date('Y-m-d'),
                'gift_status' => 'Active',
                'is_anonymous' => $event['Intangible/Event.payload']['is_anonymous'] ?? false,
                'payments' => array(
                    array (
                        'payment_method' => $payment_method
                    ),
                ),
                //For recurring gift the only valid status is DoNotPost
                'post_status' => $event['Intangible/Event.payload']['gift_payment_type'] == 'RecurringGift' ?
                    'DoNotPost' : $event['Intangible/Event.payload']['gift_general_ledger_status'],
                'reference' => $event['Intangible/Event.payload']['gift_reference'],
                "type" => $event['Intangible/Event.payload']['gift_payment_type']
            );

            //Inject lookup_id if available
            if (!empty($event['Intangible/Event.payload']['gift_subtype'])) {
                $data['subtype'] = $event['Intangible/Event.payload']['gift_subtype'];
            }

            //Inject lookup_id if available
            if (!empty($event['Intangible/Event.payload']['lookup_id'])) {
                $data['lookup_id'] = $event['Intangible/Event.payload']['lookup_id'];
            }

            //Gift Receipt
            $data['receipts'][] = array(
                'amount' => array(
                    'value' => $event['Intangible/Event.payload']['donation_amount']
                ),
                'date' => $event['Intangible/Event.payload']['gift_receipt_status'] == 'RECEIPTED' ?
                    $event['Intangible/Event.payload']['gift_datetime'] : '',
                'status' => $event['Intangible/Event.payload']['gift_receipt_status'] ?? "DONOTRECEIPT",
            );

            //Gift Splits
            if (!empty($event['Intangible/Event.payload']['gift_splits'])) {
                $data['gift_splits'] = array();
                foreach ($event['Intangible/Event.payload']['gift_splits'] as $item_key => $item) {
                    $data['gift_splits'][] = array(
                        'amount' => array (
                            'value' => $item['amount'],
                        ),
                        'appeal_id' => $item['ids']['appeal_id'],
                        'campaign_id' => $item['ids']['campaign_id'],
                        'fund_id' => $item['ids']['fund_id'],
                    );
                }
            } else {
                $data['gift_splits'] = array(
                    array (
                        'amount' => array (
                            'value' => $event['Intangible/Event.payload']['donation_amount'],
                        ),
                        'appeal_id' => $event['Intangible/Gift.appeal_id'],
                        'campaign_id' => $event['Intangible/Gift.campaign_id'],
                        'fund_id' => $event['Intangible/Gift.fund_id'],
                    ),
                );
            }

            //Inject custom fields
            if (!empty($event['Intangible/Event.payload']['custom_fields_gift'])) {
                $custom_fields = [];
                foreach ($event['Intangible/Event.payload']['custom_fields_gift'] as $key => $value) {
                    $custom_field = [
                        'category' => $key,
                        'value' => $value
                    ];

                    if (!empty($event['Intangible/Event.payload']['gift_datetime'])) {
                        $custom_field['date'] = $event['Intangible/Event.payload']['gift_datetime'];
                    }

                    $custom_fields[] = $custom_field;
                }

                if (!empty($custom_fields)) {
                    $data['custom_fields'] = $custom_fields;
                }
            }


            if ($event['Intangible/Event.payload']['gift_payment_type'] === 'RecurringGift'
                || $event['Intangible/Event.payload']['gift_payment_type'] === 'RecurringGiftPayment'
            ) {
                $recurring_date = isset($event['Intangible/Event.payload']['payment_date']) &&
                $event['Intangible/Event.payload']['payment_date'] ?
                    date("c", strtotime(date($event['Intangible/Event.payload']['payment_date']))) :
                    date("c", strtotime(date("Y-m-d", strtotime(date("Y-m-d")))));

                if (strtolower($payment_method) != 'paypal') {
                    // phpcs:disable
                    $data['payments'][0]['account_token'] = $event['Intangible/Event.payload']['account_token'];
                    $data['payments'][0]['bbps_transaction_id'] = $event['Intangible/Event.payload']['transaction_id'];
                    if (isset($event['Intangible/Event.payload']['payment_configuration_id'])) {
                        $data['payments'][0]['bbps_configuration_id']
                            = $event['Intangible/Event.payload']['payment_configuration_id'];
                    }
                    // phpcs:enable
                }
                if ($event['Intangible/Event.payload']['gift_payment_type'] !== 'RecurringGiftPayment') {
                    $data['recurring_gift_schedule'] = array(
                        'frequency'     => $event['Intangible/Event.payload']['donation_frequency'],
                        'start_date'    => $recurring_date
                    );
                }
            }

            //Add the tribute data here if pushing into batch
            if ($batch) {
                MachinePack::log('MP SKY API Adding tribute to batch');
                $this->_addTributeToBatchGift($data, $event, $auth_key);
                MachinePack::log('MP SKY API Gift data :: ' . json_encode($data));
            }

            $gift = $data;
            if ($batch) {
                if (!isset($event['Intangible/Sky.Batch.identifier'])) {
                    return new Failure(
                        'Processing batches but batch_id not provided.
                        Please ensure your application passes the - Intangible/Sky.Batch.identifier'
                    );
                }
                $gift            = array();
                $url             = 'https://api.sky.blackbaud.com/gift/v1/giftbatches/'.
                    $event['Intangible/Sky.Batch.identifier'] .'/gifts';
                $gift['gifts'][] = $data;
            }

            $response = json_decode(
                $this->_skyApiCall(
                    $url,
                    'POST',
                    $headers,
                    $gift
                )
            );

            return $response;
        } catch (\Exception $ex) {
            $exceptionMsg = $ex->getMessage();
            $returnArray  = array('Errors' => true, 'message' => $exceptionMsg);
            return json_encode($returnArray);
        }
    }

    private function _addTributeToBatchGift(&$data, $event, $token_response)
    {
        if (!empty($event['Intangible/Event.payload']['tribute'])) {
            if (!empty($event['Intangible/Event.payload']['tribute']['type'])
                && !empty($event['Intangible/Event.payload']['tribute']['honoree_name'])
            ) {
                //At first, we need to create the honoree & acknowledgee constituent records
                //Then we need to create the relationship so it can be detected by the Tribute
                //Finally we create the tribute record & tribute acknowledgee

                //1. Create Honoree
                $honoree_constituent_id = $this->_createSimpleHonoree($event, $token_response);
                MachinePack::log('MP SKY API Honoree constituent id :: ' . json_encode($honoree_constituent_id));

                $relationship_id = null;
                //2. Create acknowledgee if needed
                //If tribute acknowledgee is required
                if (!empty($event['Intangible/Event.payload']['acknowledgee'])) {
                    //3. Create Tribute Acknowledgee
                    $acknowledgee_constituent_id = $this->_createAcknowledgeeConstituent($event, $token_response);
                    MachinePack::log(
                        'MP SKY API Acknowledgee constituent id :: ' . json_encode($acknowledgee_constituent_id)
                    );

                    $relationship_data = [
                        'relationship_comment' => 'Acknowledgee of a tribute constituent',
                        'gift_assign_to_organisation' => false,
                    ];

                    //The naming here is missleading, it's the other way around?
                    if (!empty($event['Intangible/Event.payload']['tribute']['relationship']['acknowledgee_honoree'])) {
                        $relationship_data['relationship_type']
                            = $event['Intangible/Event.payload']['tribute']['relationship']['acknowledgee_honoree'];
                    }

                    if (!empty($event['Intangible/Event.payload']['tribute']['relationship']['honoree_acknowledgee'])) {
                        $relationship_data['reciprocal_type']
                            = $event['Intangible/Event.payload']['tribute']['relationship']['honoree_acknowledgee'];
                    }

                    //Create Honoree - Acknowledgee Relationship
                    $relationship_response = $this->_createRelationship(
                        $honoree_constituent_id,
                        $acknowledgee_constituent_id,
                        [
                            "Content-Type: application/json",
                            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
                            "Authorization: Bearer " . (
                            empty($token_response['access_token']) ?
                                $event['Intangible/Sky.access_token'] : $token_response['access_token']
                            )
                        ],
                        $relationship_data
                    );

                    MachinePack::log(
                        'MP SKY API Honoree <-> Acknowledgee Relationship data :: '
                        . json_encode($relationship_data)
                    );
                    MachinePack::log(
                        'MP SKY API Honoree <-> Acknowledgee Relationship response :: '
                        . json_encode($relationship_response)
                    );

                    if (!empty($relationship_response)) {
                        $relationship_id = $relationship_response;
                    }
                }

                //2. Create a (generic) Tribute
                $tribute          = $this->_getTributeEvent($event, $honoree_constituent_id);
                $tribute_response = $this->_createTribute($event, $tribute);

                MachinePack::log('MP SKY API $tribute data :: ' . json_encode($tribute));
                MachinePack::log('MP SKY API $tribute_response response :: ' . json_encode($tribute_response));

                if (empty($tribute_response->id)) {
                    MachinePack::log('MP SKY API _createTribute invalid response :: ' . json_encode($tribute_response));
                } else {
                    $data['tributes'] = [[
                        'id' => $tribute_response->id,
                    ]];

                    //If tribute acknowledgee is required
                    if (!empty($event['Intangible/Event.payload']['acknowledgee'])) {
                        $tribute_acknowledgee_event = [
                            'tribute_id' => $tribute_response->id,
                            //A null relationship ID indicates a self-acknowledgee.
                            'relationship_id' => $relationship_id
                        ];

                        MachinePack::log(
                            'MP SKY API $tribute_acknowledgee_event :: '
                            . json_encode($tribute_acknowledgee_event)
                        );

                        $tribute_acknowledgee_response = $this->_createTributeAcknowledgee(
                            $event,
                            $tribute_acknowledgee_event,
                            $token_response
                        );

                        MachinePack::log(
                            'MP SKY API $tribute_acknowledgee_response response :: '
                            . json_encode($tribute_acknowledgee_response)
                        );

                        if (!empty($tribute_acknowledgee_response->id)) {
                            $tribute_acknowledgee_id = $tribute_acknowledgee_response->id;
                            $data['tributes'][0]['tribute_acknowledgees'] = [[
                                'id' => $tribute_acknowledgee_id
                            ]];
                        }
                    }
                }
            }
        }
    }

    private function _createTributeForNonBatchGift($event, $token_response)
    {
        if (!empty($event['Intangible/Event.payload']['tribute'])) {
            if (!empty($event['Intangible/Event.payload']['tribute']['type'])
                && !empty($event['Intangible/Event.payload']['tribute']['honoree_name'])
            ) {
                $honoree_search_event          = [
                    'Intangible/Sky.access_token' => $event['Intangible/Sky.access_token'],
                    'Intangible/Event.payload' => [
                        'honoree_name' => $event['Intangible/Event.payload']['tribute']['honoree_name']
                    ]
                ];
                $honoree_search_event_response = $this->_getConstituentSearch(
                    $honoree_search_event,
                    $token_response,
                    'honoree_name',
                    1
                );

                MachinePack::log(
                    'MP SKY API $honoree_search_event response :: ' . json_encode($honoree_search_event_response)
                );

                if ($honoree_search_event_response instanceof Failure) {
                    MachinePack::log(
                        'MP SKY API $honoree_search_event failure :: ' .
                        json_encode($honoree_search_event_response)
                    );
                }

                if (isset($honoree_search_event_response->statusCode)) {
                    //Token needs to be refreshed - will be handled in parent call/function
                    MachinePack::log(
                        'MP SKY API $honoree_search_event invalid response :: ' .
                        json_encode($honoree_search_event_response)
                    );
                } else {
                    if (isset($honoree_search_event_response->count) && $honoree_search_event_response->count > 0) {
                        $honoree_constituent_id = $honoree_search_event_response->value[0]->id;
                    } else {
                        $honoree_constituent_id = $this->_createSimpleHonoree($event, $token_response);
                    }

                    if (!empty($honoree_constituent_id)) {
                        $tribute = $this->_getTributeEvent($event, $honoree_constituent_id);

                        MachinePack::log(
                            'MP SKY API _createTribute data :: ' . json_encode($tribute)
                        );

                        $tribute_response = $this->_createTribute($event, $tribute, $token_response);

                        MachinePack::log(
                            'MP SKY API $tribute_response response :: ' . json_encode($tribute_response)
                        );

                        if (empty($tribute_response->id)) {
                            MachinePack::log(
                                'MP SKY API _createTribute invalid response :: ',
                                json_encode($tribute_response)
                            );
                        } else {
                            //If gift tribute is required
                            if (!empty($event['Intangible/Event.payload']['honoree_gift_tribute'])
                                && !empty($gift_id)
                            ) {
                                $gift_tribute_response = $this->_createGiftTribute(
                                    $event,
                                    ['gift_id' => $gift_id, 'tribute_id' => $tribute_response->id],
                                    $token_response
                                );

                                MachinePack::log(
                                    'MP SKY API $gift_tribute_response response :: '
                                    . json_encode($gift_tribute_response)
                                );
                            }

                            //If tribute acknowledgee is required
                            if (!empty($event['Intangible/Event.payload']['acknowledgee'])) {
                                //Create Acknowledgee Constituent
                                $acknowledgee_constituent_id = $this->_createAcknowledgeeConstituent(
                                    $event,
                                    $token_response
                                );

                                if (!empty($acknowledgee_constituent_id)) {
                                    //Create Honoree - Acknowledgee Relationship
                                    $relationship_response = $this->_createRelationship(
                                        $honoree_constituent_id,
                                        $acknowledgee_constituent_id,
                                        [
                                            "Content-Type: application/json",
                                            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
                                            "Authorization: Bearer " . (
                                            empty($token_response['access_token']) ?
                                                $event['Intangible/Sky.access_token'] : $token_response['access_token']
                                            )
                                        ],
                                        [
                                            'description' => 'Acknowledgee of a tribute constituent'
                                        ]
                                    );

                                    if (!empty($relationship_response->id)) {
                                        $tribute_acknowledgee = [
                                            'tribute_id' => $tribute_response->id,
                                            'relationship_id' => $relationship_response->id
                                        ];

                                        $tribute_acknowledgee_response = $this->_createTributeAcknowledgee(
                                            $event,
                                            $tribute_acknowledgee,
                                            $token_response
                                        );

                                        MachinePack::log(
                                            'MP SKY API $tribute_acknowledgee_response response :: '
                                            . json_encode($tribute_acknowledgee_response)
                                        );
                                    } else {
                                        MachinePack::log(
                                            'MP SKY API $relationship_response response :: '
                                            . json_encode($relationship_response)
                                        );
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private function _patchReceipt($giftId, $event)
    {
        try {
            $url = 'https://api.sky.blackbaud.com/gift/v1/giftreceipts/' . $giftId;

            $headers = array(
                "Content-Type: application/json",
                "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
                "Authorization: Bearer " . $event['Intangible/Sky.access_token']
            );

            //Recurring gift status may only be 'DONOTRECEIPT'
            $status = $event['Intangible/Event.payload']['gift_payment_type'] == 'RecurringGift' ?
                'DONOTRECEIPT' : ($event['Intangible/Event.payload']['gift_receipt_status'] ?? 'RECEIPTED');

            $data = array(
                'amount' => array(
                    'value' => number_format($event['Intangible/Event.payload']['donation_amount'])
                ),
                'number' => $event['Intangible/Event.payload']['gift_receipt_id'] ?? $giftId,
                'status' => $status
            );

            if ($status != 'DONOTRECEIPT') {
                $data['date'] = $event['Intangible/Event.payload']['gift_datetime'] ?? date('Y-m-d\TH:i:s');
            }

            $response = $this->_skyApiCall(
                $url,
                'PATCH',
                $headers,
                $data
            );

            return $response;
        } catch (\Exception $ex) {
            $exceptionMsg = $ex->getMessage();
            $returnArray  = array('Errors' => true, 'message' => $exceptionMsg);
            return json_encode($returnArray);
        }
    }

    private function _getGift($giftId, $event)
    {
        try {
            $url = 'https://api.sky.blackbaud.com/gift/v1/gifts/' . $giftId;

            $headers = array(
                "Content-Type: application/json",
                "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
                "Authorization: Bearer " . $event['Intangible/Sky.access_token']
            );

            $data = array();

            $response = $this->_skyApiCall(
                $url,
                'GET',
                $headers,
                $data
            );

            return $response;
        } catch (\Exception $ex) {
            $exceptionMsg = $ex->getMessage();
            $returnArray  = array('Errors' => true, 'message' => $exceptionMsg);
            return json_encode($returnArray);
        }
    }

    private function _getCodeTables($event, $token = false)
    {
        $auth_key = $event['Intangible/Sky.access_token'];
        if (isset($token['access_token'])) {
            $auth_key = $token['access_token'];
        }
        $headers = array(
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        );

        $url    = "https://api.sky.blackbaud.com/nxt-data-integration/v1/re/codetables";
        $method = 'GET';

        return json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                array()
            )
        );
    }

    private function _getCodeTableEntries($event, $table_id, $token = false)
    {
        $auth_key = $event['Intangible/Sky.access_token'];
        if (isset($token['access_token'])) {
            $auth_key = $token['access_token'];
        }
        $headers = array(
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        );

        $url    = "https://api.sky.blackbaud.com/nxt-data-integration/v1/re/codetables/" . $table_id . "/tableentries";
        $method = 'GET';

        return json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                array()
            )
        );
    }

    private function _createAuthURL()
    {
        $auth_data = array(
            'client_id' => $this->_config['client_id'],
            'response_type' => 'code',
            'redirect_uri' => $this->_config['redirect_uri']
        );

        $auth_uri = 'https://app.blackbaud.com/oauth/authorize?' . http_build_query($auth_data);

        return new Success(
            [
                'status' => 'URL Creation',
                'url'    => $auth_uri
            ]
        );
    }


    private function _exchangeCodeForAccessToken($event)
    {
        $body = array(
            'grant_type' => 'authorization_code',
            'redirect_uri' => $this->_config['redirect_uri'],
            'code' => $event['Intangible/Sky.application_code']
        );

        $token = $this->_fetchTokens($body);
        if (!($token instanceof Failure)) {
            return new Success(
                [
                    'token' => $token
                ]
            );
        }
        return $token;
    }

    private function _refreshToken($event, $full_details = false)
    {
        $refresh_token = $event['Intangible/Sky.refresh_token'];
        $body          = array(
            'grant_type' => 'refresh_token',
            'refresh_token' => $refresh_token,
            'preserve_refresh_token' => 'true'
        );

        if (isset($event['Intangible/Sky.preserve_refresh_token'])
            && $event['Intangible/Sky.preserve_refresh_token'] == 'no'
        ) {
            $body['preserve_refresh_token'] = 'false';
        }

        $token_response = $this->_fetchTokens($body);
        if (!$token_response instanceof Failure) {
            if ($full_details) {
                return new Success(
                    $this->_getDefaultSuccessPayload($token_response)
                );
            } else {
                return $token_response;
            }
        } else {
            return $token_response;
        }
    }

    private function _fetchTokens($body = array())
    {
        $headers = array(
            'Content-type: application/x-www-form-urlencoded',
            'Authorization: Basic ' . base64_encode($this->_config['client_id'] . ':' . $this->_config['client_secret'])
        );

        $url = "https://oauth2.sky.blackbaud.com/token";

        $response = $this->_skyApiCall($url, 'POST', $headers, $body, true);
        $token    = json_decode($response, true);

        /*echo "<pre>";
        var_dump('$token', $token);
        die;*/

        if (isset($token['error']) && $token['error']) {
            return new Failure(
                $token['error']
            );
        }

        return $token;
    }

    private function _getDefaultSuccessPayload($response)
    {
        $payload = [
            'Intangible/Sky.refresh_token' => isset($response['refresh_token'])
                ? $response['refresh_token'] : '',
            'Intangible/Sky.refresh_token_expires_in' => isset($response['refresh_token_expires_in'])
                ? $response['refresh_token_expires_in'] : '',
            'Intangible/Sky.access_token' => isset($response['access_token'])
                ? $response['access_token'] : '',
            'Intangible/Sky.access_token_expires_in' => isset($response['expires_in'])
                ? $response['expires_in'] : '',
        ];

        return $payload;
    }

    /**
     * Call the API via curl
     *
     * @param string $method POST|GET|PUT
     * @param string $url
     * @param array $data
     * @return void
     */
    private function _skyApiCall($url, $method, $headers, $data, $auth = false)
    {
        $params = array();

        if ($auth) {
            $params = http_build_query($data);
        } else {
            $params = json_encode($data);
        }

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSLVERSION, 0);

        if ($method == "POST") {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        } elseif ($method == "PUT") {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
        } elseif ($method == "PATCH") {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
            curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        } elseif ($method == "DELETE") {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
        }

        $response = curl_exec($curl);

        if (curl_error($curl)) {
            $error_msg = curl_error($curl);
        }

        curl_close($curl);

        if (isset($error_msg)) {
            throw new \Exception($error_msg);
        }

        if ($response === null && json_last_error() !== JSON_ERROR_NONE) {
            throw new \Exception('Could not parse JSON response');
        }

        return $response;
    }
}
