<?php
namespace MachinePack\Core\Handler\Crmsync;

use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Event\Events\Crmsync;

class Pardot extends Handler
{
    private $_config;
    private $_userKey;
    private $_apiKey;

    public function handleEvent(\MachinePack\Core\Event\Event $event): Result
    {
        if (! $event instanceof Crmsync) {
            return new Ignored;
        }

        //load config from settings
        $this->_config = $this->settings[$this->settings['env']];

        //Api credentials can be either passed as args or loaded from config
        //validate configuration variables
        if (empty($this->_config['email'])
            || empty($this->_config['password'])
            || empty($this->_config['userKey'])
        ) {
            return new Failure(
                'Please provide Email, Password and userKey for Pardot Configuration. Full config should be:
                {
                    config: {
                        env: <someenv>,
                        <someenv>: {
                            email:
                            password:
                            userKey:
                        }
                    }
                }
            '
            );
        }

        return $this->_sendTransactionalEmail($event);
    }

    /**
     * Submit the transaction email event payload to Campaign Monitor
     * @param Crmsync $event
     */
    private function _sendTransactionalEmail(Crmsync $event)
    {
        try {
            $loginResponse = $this->_generateApiKey($event);

            if (!empty($loginResponse)) {
                //convert the result to a JSON object
                $loginResponse = json_decode($loginResponse, true);

                if (isset($loginResponse['api_key'])) {
                    $this->_apiKey = $loginResponse['api_key'];
                } else {
                    return new Failure('Error:: Invalid API response. (login). ' . $loginResponse['err']);
                }
            } else {
                return new Failure('Error:: Invalid API response. (login response empty).');
            }

            if (!isset($event['EmailMessage.recipient.email'])
                || !filter_var($event['EmailMessage.recipient.email'], FILTER_VALIDATE_EMAIL)
            ) {
                return new Failure('Error:: Message - Incorrect recipient email.');
            }

            $user_id = $this->_createProspect($event);
            return $this->_sendEmail($event, $user_id);
        } catch (\Exception $e) {
            return new Failure(
                $e->getMessage()
            );
            //TODO should log stacktrace
        }
    }

    /**
     * Check if prospect exists and create or update prospect account.
     *
     * @return string $prospect_id
     */
    private function _checkProspect($event)
    {
        $checkProspect = $this->_curl(
            'https://pi.pardot.com/api/prospect/version/4/do/read/email/'.$event['EmailMessage.recipient.email'],
            array(
            'user_key' => $this->_config['userKey'],'output' => 'simple'
            ),
            'POST',
            array(
            //create the Authorization header from the user_key and api_key
            "Authorization: Pardot user_key=".$this->_config['userKey'].",api_key=".$this->_apiKey
            )
        );

        $checkProspectXml = simplexml_load_string($checkProspect);

        if ($checkProspectXml === false) {
            return new Failure('Error while parsing XML. (prospect)');
        }

        $checkProspectResponse     = $checkProspectXml['stat'];
        $pardotID                  = isset($checkProspectXml->prospect->id)?$checkProspectXml->prospect->id:'';
        $apiUpdateProspectEndPoint = "https://pi.pardot.com/api/prospect/version/4/do/update/id/";
        $apicreateProspectEndPoint = "https://pi.pardot.com/api/prospect/version/4/do/create/email/";

        if ($checkProspectResponse == 'ok') {
            $apiEndPoint = $apiUpdateProspectEndPoint.$pardotID;
        } else {
            $apiEndPoint = $apicreateProspectEndPoint.$event['EmailMessage.recipient.email'];
        }

        return $apiEndPoint;
    }

    /**
     * Check if prospect exists and create or update prospect account.
     *
     * @return string $prospect_id
     */
    private function _createProspect($event)
    {
        $apiEndPoint = $this->_checkProspect($event);

        $prospectData = array(
            'first_name'                        => $event['EmailMessage.recipient.givenName'],
            'last_name'                         => $event['EmailMessage.recipient.familyName'],
            'campaign_id'                       => $event['Intangible/Pardot.campaign_id'],
            'email_template_id'                 => $event['Intangible/Pardot.email_template_id'],
            'address_one'                       => $event['Intangible/Event.payload']['address_one'],
            'address_two'                       => $event['Intangible/Event.payload']['address_two'],
            'city'                              => $event['Intangible/Event.payload']['city'],
            'state'                             => $event['Intangible/Event.payload']['state'],
            'zip'                               => $event['Intangible/Event.payload']['zip'],
            'country'                           => $event['Intangible/Event.payload']['country'],
            'leafcutter_donation_frequency'     =>
            $event['Intangible/Event.payload']['leafcutter_donation_frequency'],
            'leafcutter_donation_amount'        => $event['Intangible/Event.payload']['leafcutter_donation_amount'],
            'leafcutter_payment_gateway'        => $event['Intangible/Event.payload']['leafcutter_payment_gateway'],
            'leafcutter_payment_type'           => $event['Intangible/Event.payload']['leafcutter_payment_type'],
            'leafcutter_transaction_id'         => $event['Intangible/Event.payload']['leafcutter_transaction_id'],
            'transaction_date'                  => $event['Intangible/Event.payload']['transaction_date'],
            'leafcutter_token'                  => $event['Intangible/Event.payload']['leafcutter_token'],
            'leafcutter_status'                 => $event['Intangible/Event.payload']['leafcutter_status'],
            'leafcutter_on_behalf_of'           =>
            $event['Intangible/Event.payload']['leafcutter_on_behalf_of'],
            'leafcutter_on_behalf_first_name'   =>
            $event['Intangible/Event.payload']['leafcutter_on_behalf_first_name'],
            'leafcutter_on_behalf_last_name'    =>
            $event['Intangible/Event.payload']['leafcutter_on_behalf_last_name'],
            'leafcutter_tribute_type'           => $event['Intangible/Event.payload']['leafcutter_tribute_type'],
            'output'                            => 'simple'
        );

        if (isset($event['Intangible/Event.payload']['phone']) && $event['Intangible/Event.payload']['phone']) {
            $prospectData['phone'] = $event['Intangible/Event.payload']['phone'];
        }

        $addPropect = $this->_curl(
            $apiEndPoint,
            $prospectData,
            'POST',
            array(
                //create the Authorization header from the user_key and api_key
                "Authorization: Pardot user_key=".$this->_config['userKey'].",api_key=".$this->_apiKey
            )
        );

        $addPropect = simplexml_load_string($addPropect);

        if ($addPropect !== false) {
            if (isset($addPropect->err)) {
                return new Failure('Error:: ' . $addPropect->err);
            }
        } else {
            return new Failure('Error while parsing XML. (prospect)');
        }

        return $addPropect->prospect->id;
    }

    /**
     * Send the email to the prospect bsaed off their ID
     *
     * @return array $response
     */
    private function _sendEmail($event, $user_id)
    {
        $pardot_email_vars = array(
                'first_name' => $event['EmailMessage.recipient.givenName'],
                'last_name' => $event['EmailMessage.recipient.familyName'],
                'campaign_id' => $event['Intangible/Pardot.campaign_id'],
                'email_template_id' => $event['Intangible/Pardot.email_template_id'],
                'operational_email' => true,
                'user_key' => $this->_config['userKey'],
                'prospect_id' => $user_id,
        );
        if (isset($event['EmailMessage.subject']) && $event['EmailMessage.subject']) {
            $pardot_email_vars['subject'] = $event['EmailMessage.subject'];
        }

        $sendPardotEmail = $this->_curl(
            'https://pi.pardot.com/api/email/version/4/do/send/prospect_id/' . $user_id,
            $pardot_email_vars,
            'POST',
            array(
                //create the Authorization header from the user_key and api_key
                "Authorization: Pardot user_key=".$this->_config['userKey'].",api_key=".$this->_apiKey
            )
        );

        $sendPardotEmail = simplexml_load_string($sendPardotEmail);

        if ($sendPardotEmail !== false) {
            if (isset($sendPardotEmail->err)) {
                return new Failure('Error:: ' . $sendPardotEmail->err);
            }
        } else {
             return new Failure('Error while parsing XML. (email)');
        }

        return new Success(
            [
                'Successfully sent email'
            ]
        );
    }

    /**
     * Generate an API for Pardot
     *
     * @return array $response
     */
    private function _generateApiKey(Crmsync $event)
    {
        try {
            $params = array(
                'email' => $this->_config['email'],
                'password' => $this->_config['password'],
                'user_key' => $this->_config['userKey'],
                'format' => 'json'
            );

            $json_response = $this->_curl(
                'https://pi.pardot.com/api/login/version/4',
                $params,
                'POST',
                null,
                true
            );

            return $json_response;
        } catch (\Exception $ex) {
            $exceptionMsg = $ex->getMessage();
            $returnArray  = array('Errors' => true, 'message' => $exceptionMsg);
            return json_encode($returnArray);
        }
    }

    /**
     * Make a curl request
     *
     * @param string $method    GET|POST
     * @param string $url       the url to call
     * @param string $bearer    the bearer token for authentication
     * @param array $data       request payload
     * @return void
     */
    //private function _curl($method, $url, $data = array())
    private function _curl($url, $data = array(), $method = 'GET', $headers = null, $login = false)
    {
        // build out the full url, with the query string attached.
        if (!$login) {
            $queryString = http_build_query($data, null, '&');
            if (strpos($url, '?') !== false) {
                $url = $url . '&' . $queryString;
            } else {
                $url = $url . '?' . $queryString;
            }
        }

        $curl_handle = curl_init($url);

        // wait 5 seconds to connect to the Pardot API, and 30
        // total seconds for everything to complete
        curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($curl_handle, CURLOPT_TIMEOUT, 30);

        // https only, please!
        curl_setopt($curl_handle, CURLOPT_PROTOCOLS, CURLPROTO_HTTPS);

        // ALWAYS verify SSL - this should NEVER be changed. 2 = strict verify
        curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, 2);

        // return the result from the server as the return value of curl_exec instead of echoing it
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);

        if (strcasecmp($method, 'POST') === 0) {
            curl_setopt($curl_handle, CURLOPT_POST, true);
            if ($login) {
                curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $data);
            }
        } elseif (strcasecmp($method, 'GET') !== 0) {
            // perhaps a DELETE?
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, strtoupper($method));
        }

        // add any headers that were specified
        if ($headers) {
            curl_setopt($curl_handle, CURLOPT_HTTPHEADER, $headers);
        }

        $pardotApiResponse = curl_exec($curl_handle);
        if ($pardotApiResponse === false) {
            // failure - a timeout or other problem. depending on how you want to handle failures,
            // you may want to modify this code. Some folks might throw an exception here. Some might
            // log the error. May you want to return a value that signifies an error. The choice is yours!

            // let's see what went wrong -- first look at curl
            $humanReadableError = curl_error($curl_handle);

            // you can also get the HTTP response code
            $httpResponseCode = curl_getinfo($curl_handle, CURLINFO_HTTP_CODE);

            // make sure to close your handle before you bug out!
            curl_close($curl_handle);

            throw new Exception(
                "Unable to successfully complete Pardot API call
                 to $url -- curl error: $humanReadableError\,
                 HTTP response code was: $httpResponseCode"
            );
        }

        // make sure to close your handle before you bug out!
        curl_close($curl_handle);

        return $pardotApiResponse;
    }
}
