<?php
namespace MachinePack\Core\Handler\Crmsync;

use MachinePack\Core\Handler\Handler;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Event\Events\Crmsync;

class MicrosoftDynamics extends Handler
{
    private $_config;

    public function handleEvent(\MachinePack\Core\Event\Event $event): Result
    {
        if (! $event instanceof Crmsync) {
            return new Ignored;
        }

        //Api credentials can be either passed as args or loaded from config
        //validate configuration variables
        try {
            $this->_fetchValidateConfig($event);
        } catch (\Exception $e) {
            return new Failure($e->getMessage());
        }

        if (isset($event['Intangible/D365.action'])) {
            switch ($event['Intangible/D365.action']) {
                case 'authenticate':
                    return $this->_authenticate($event);
                case 'process_gift':
                    return $this->_processGift($event);
                case 'power_automate_data_fetch':
                    return $this->_fetchDataFromPowerAutomateEndpoint($event);
                case 'power_automate_data_push':
                    return $this->_pushDataFromPowerAutomateEndpoint($event);
            }
        }

        return new Failure('Invalid event action provided.');
    }

    private function _fetchValidateConfig($event)
    {
        //Check the event payload whether it includes valid configuration
        //phpcs:disable
        switch ($event['Intangible/D365.action']) {
            case 'authenticate':
            $errors = $this->_fetchValidateEventConfig(
            $event,
            ['endpoint_url', 'client_id', 'client_secret']
        );
                break;
            case 'process_gift':
            $errors = $this->_fetchValidateEventConfig(
            $event,
            ['endpoint_url', 'endpoint_env', 'endpoint_app_id', 'endpoint_token', 'auth_token']
        );
                break;
            default:
            $errors = $this->_fetchValidateEventConfig(
            $event,
            ['endpoint_url', 'endpoint_env', 'endpoint_app_id', 'endpoint_token']
        );
                break;
            // phpcs:enable
        }

        if (!empty($errors)) {
            $this->_throwConfigurationException($errors);
        }
    }

    private function _fetchValidateEventConfig($event, $required_fields = [])
    {
        $errors = [];

        foreach ($required_fields as $field) {
            if (!isset($event['Intangible/D365.' . $field])) {
                if (!isset($this->settings[$this->settings['env']][$field])) {
                    $errors[] = $field;
                } else {
                    $this->_config[$field] = $this->settings[$this->settings['env']][$field];
                }
            } else {
                $this->_config[$field] = $event['Intangible/D365.' . $field];
            }
        }

        return $errors;
    }

    private function _throwConfigurationException($errors = [])
    {
        throw new \Exception(
            'Please provide the following configuration fields within your configuration file or the event payload: ' .
            implode(', ', $errors)
        );
    }

    private function _authenticate($event)
    {
        $headers = array(
            "Content-Type: application/x-www-form-urlencoded"
        );

        $url    = $this->_config['endpoint_url'];
        $method = 'POST';
        $data   = [
            'client_id' => $this->_config['client_id'],
            'client_secret' => $this->_config['client_secret'],
            'scope' => 'https://service.flow.microsoft.com//.default',
            'grant_type' => 'client_credentials'
        ];

        $response = json_decode(
            $this->_curlCall(
                $url,
                $method,
                $headers,
                $data,
                true
            )
        );

        if (!empty($response) && !empty($response->access_token) && empty($response->error)) {
            return new Success($response);
        } else {
            return new Failure(
                'Unable to fetch data from authentication endpoint.' .
                implode('. ', [($response->error ?? ''), ($response->error_description ?? '')])
            );
        }
    }

    private function _fetchDataFromPowerAutomateEndpoint($event)
    {
        $headers = array(
            "Content-Type: application/json"
        );

        $url    = $this->_config['endpoint_url'];
        $method = 'POST';
        $data   = [
            'token' => $this->_config['endpoint_token'],
            'environment' => $this->_config['endpoint_env'],
            'app_id' => $this->_config['endpoint_app_id']
        ];

        if (!empty($event['Intangible/Event.payload'])) {
            $data = array_merge($event['Intangible/Event.payload'], $data);
        }

        $response = json_decode(
            $this->_curlCall(
                $url,
                $method,
                $headers,
                $data
            )
        );

        if (!empty($response)) {
            return new Success($response);
        } else {
            return new Failure('Unable to fetch data from power automate endpoint.');
        }
    }

    private function _pushDataFromPowerAutomateEndpoint($event)
    {
        $headers = array(
            "Authorization: " . $event['Intangible/D365.api_auth_token'],
            "Content-Type: application/json",
        );

        $url    = $this->_config['endpoint_url'];
        $method = 'POST';
        $data   = [
            'token' => $this->_config['endpoint_token'],
            'environment' => $this->_config['endpoint_env'],
            'app_id' => $this->_config['endpoint_app_id']
        ];

        if (!empty($event['Intangible/Event.payload'])) {
            $data = array_merge($event['Intangible/Event.payload'], $data);
        }

        $response = json_decode(
            $this->_curlCall(
                $url,
                $method,
                $headers,
                $data
            )
        );

        if (!empty($response)) {
            return new Success($response);
        } else {
            return new Failure('Unable to fetch data from power automate endpoint.');
        }
    }

    private function _processGift($event)
    {
        $headers = array(
            "Content-Type: application/json",
            "Authorization: Bearer " . $this->_config['auth_token']
        );

        $url    = $this->_config['endpoint_url'];
        $method = 'POST';
        $data   = [
            'token' => $this->_config['endpoint_token'],
            'environment' => $this->_config['endpoint_env'],
            'app_id' => $this->_config['endpoint_app_id']
        ];

        if (!empty($event['Intangible/Event.payload'])) {
            $data = array_merge($event['Intangible/Event.payload'], $data);
        }

        $response_data = $this->_curlCall(
            $url,
            $method,
            $headers,
            $data,
            false,
            true
        );

        $response = json_decode($response_data['response']);

        if ($response_data['code'] === 201
            && !empty($response->contact_id)
            && !empty($response->transaction_id)
        ) {
            return new Success(
                [
                    'Intangible/Record.contact_guid' => $response->contact_guid ?? '',
                    'Intangible/Record.contact_id' => $response->contact_id,
                    'Intangible/Record.transaction_id' => $response->transaction_id,
                    'Intangible/Response.code' => $response_data['code']
                ]
            );
        } else {
            return new Failure(
                'Error:: Invalid API response while retrieving gift Id. Response :: '
                . json_encode($response_data)
            );
        }
    }

    /**
     * Call the API via curl
     *
     * @param string $method POST|GET|PUT
     * @param string $url
     * @param array $data
     * @return void
     */
    private function _curlCall($url, $method, $headers, $data, $auth = false, $return_response_code = false)
    {
        $params = array();

        if ($auth) {
            $params = http_build_query($data);
        } else {
            $params = json_encode($data);
        }

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSLVERSION, 0);

        if ($method == "POST") {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        } elseif ($method == "PUT") {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
        } elseif ($method == "PATCH") {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
            curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        } elseif ($method == "DELETE") {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
        }

        $response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if (curl_error($curl)) {
            $error_msg = curl_error($curl);
        }

        curl_close($curl);

        if (isset($error_msg)) {
            throw new \Exception($error_msg);
        }

        if ($response === null && json_last_error() !== JSON_ERROR_NONE) {
            throw new \Exception('Could not parse JSON response');
        }

        if ($return_response_code) {
            return [
                'response' => $response,
                'code' => $httpcode
            ];
        }

        return $response;
    }
}
