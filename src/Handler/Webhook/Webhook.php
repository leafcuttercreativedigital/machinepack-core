<?php
namespace MachinePack\Core\Handler\Webhook;

use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Result;

class Webhook extends Handler
{
	public function handleEvent(\MachinePack\Core\Event\Event $event): Result
	{
		if (random_int(0, 10) > 8) {
			$data = [
				'message' => 'Something went wrong'
			];
			$this->broker->send('webhook.failure', $data);
			return new Failure($data['message']);
		} else {
			$data = [
				'Server' => $event['Intangible/RESTRequest.server'] ?? null,
				'Payload' => $event['Intangible/RESTRequest.data'] ?? null
			];
			$this->broker->send('webhook.success', $data);
			return new Success($data);
		}
	}
}
