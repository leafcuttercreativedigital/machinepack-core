<?php
namespace MachinePack\Core\Handler\Logic;

use MachinePack\Core\Event\Event;
use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Exception\Settings as SettingsException;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Util;

/**
 * Basic event router using templated events and conditions
 */
class Mapper extends Handler
{
    /**
     * Recursive callback
     *
     * @param mixed $item array value
     * @param string|number $key
     * @return void
     */
    private function _processValue(&$value, $key, $event)
    {
        if (!is_string($value)) {
            return;
        }
        $twig  = new Util\Twig(
            [
            'value' => $value
            ]
        );
        $value = trim(
            strval(
                $twig->render(
                    'value',
                    [
                        'data' => $event->getData()
                    ]
                )
            )
        );
    }

    /**
     * Handler impl
     *
     * @param Event $event
     * @return Result
     */
    public function handleEvent(Event $event): Result
    {
        if (empty($this->config['data'])) {
            throw new SettingsException(
                'Nothing to output. Make sure you set what data is being sent out via "data: ...".'
            );
        }

        if (empty($this->config['event'])) {
            throw new SettingsException(
                'No output event path given. Make sure you set "event: your.custom.event".'
            );
        }

        $parsed = $this->config['data'];
        \array_walk_recursive($parsed, [$this, '_processValue'], $event);

        return $this->forward(strval($this->config['event']), $parsed);
    }
}
