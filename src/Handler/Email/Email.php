<?php
namespace MachinePack\Core\Handler\Email;

use MachinePack\Core\Event\Event;
use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Exception\Settings as SettingsException;
use MachinePack\Core\Util;

/**
 * Base email handler using PHP mail().
 * Does both config and templating, subclass for customisation
 */
class Email extends Handler
{
    public function handleEvent(Event $event): Result
    {
        if (empty($this->config['contents']['to'])
            || empty($this->config['contents']['subject'])
            || empty($this->config['contents']['message'])
        ) {
            throw new SettingsException(
                'You must provide at least a to, subject and message values for an email, e.g.:
                    settings:
                    env: development
                    development:
                      contents:
                        to: |
                            {{ $event[\'Person.email\'] }}
                        subject: >
                            Thank you for your order #{{ event[\'Order.identifier\'] }}
                        message: >
                            <h1>Thank you for your order!</h1>
                            <p>Your order #{{ event[\'Order.identifier\'] }} was submitted successfully</p>
                        headers: ~
                        attachments: []
                '
            );
        }

        $twig_data = $this->getTemplateData($event, $this->config);

        $twig = new Util\Twig($this->config['contents']);

        $rendered = [];
        foreach ($this->config['contents'] as $field => $template) {
            $rendered[$field] = $this->renderField($twig, $field, strval($template), $twig_data);
        }

        if (empty($rendered['to'])
            || empty($rendered['subject'])
            || empty($rendered['message'])
        ) {
            return new Ignored('Empty to, subject or message');
        }

        $result = $this->send(
            $rendered['to'],
            $rendered['subject'],
            $rendered['message'],
            $rendered['headers'],
            (array) $this->config['attachments']
        );

        if (!$result) {
            return new Failure('PHP mail() failed');
        } else {
            return new Success($rendered);
        }
    }

    protected function getTemplateData(Event $event, array $config)
    {
        return [
            'event' => $event,
            'settings' => $config
        ];
    }

    /**
     * Automatically add some headers to plain mail
     *
     * @param string $headers
     * @return void
     */
    protected function getMailHeaders(string $headers)
    {
        $email_headers = [];
        if (!preg_match('/MIME-Version: /i', $headers)) {
            $email_headers[] = 'MIME-Version: 1.0';
        }
        if (!preg_match('/Content-Type: /i', $headers)) {
            $email_headers[] = 'Content-type: text/html; charset=UTF-8';
        }

        $header_string =  implode("\r\n", $email_headers);

        return $header_string;
    }

    protected function renderField(\Twig\Environment $twig, string $field, string $template, array $data)
    {
        return trim(strval($twig->render($field, $data)));
    }

    protected function send(string $to, string $subject, string $message, string $headers = '', array $attachments = [])
    {
        if ($this->config['disabled'] === true) {
            return true;
        }

        if (!empty($attachments)) {
            throw new SettingsException('This mailer does not support attachments.');
        }

        return mail($to, $subject, $message, $this->getMailHeaders($headers));
    }
}
