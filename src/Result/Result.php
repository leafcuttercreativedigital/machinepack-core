<?php
namespace MachinePack\Core\Result;

abstract class Result
{
    /**
     * Unique result identifier
     *
     * @var string
     */
    public $uid;

    public function __construct()
    {
        $this->uid = uniqid();
    }

    public function __toString()
    {
        return get_class($this) . '#' . $this->uid;
    }

    public function asHttpResponseData()
    {
        return [
            'type' => (string) $this
        ];
    }

    public function asHttpCode()
    {
        return 200;
    }
}
