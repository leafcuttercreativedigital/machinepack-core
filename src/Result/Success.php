<?php
namespace MachinePack\Core\Result;

class Success extends Result
{
    public $uid;
    public $data;
    public function __construct($data = null)
    {
        parent::__construct();
        $this->data = is_string($data) ? [
            'message' => $data
        ] : $data;
    }

    public function asHttpResponseData()
    {
        $data         = parent::asHttpResponseData();
        $data['data'] = (array) $this->data;
        return $data;
    }
}
