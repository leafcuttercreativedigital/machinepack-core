<?php
namespace MachinePack\Core\Exception;

use MachinePack\Core\Event;

class MissingArguments extends \Exception
{
    public $missing_args;
    public function __construct($event, array $missing_args)
    {
        $this->missing_args = $missing_args;

        parent::__construct(
            sprintf(
                'The event %s requires the following missing data: %s',
                $event,
                implode(', ', $missing_args)
            )
        );
    }
}
