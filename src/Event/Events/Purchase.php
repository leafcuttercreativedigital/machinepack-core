<?php
namespace MachinePack\Core\Event\Events;

use MachinePack\Core\Event\Event;

class Purchase extends Payment
{
    public function __construct(array $event, array $data)
    {
        $this->required_data = array_merge(
            $this->required_data,
            [
                'Product.productId'
            ]
        );

        parent::__construct($event, $data);
    }
}
