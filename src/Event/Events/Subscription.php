<?php
namespace MachinePack\Core\Event\Events;

use MachinePack\Core\Event\Event;

class Subscription extends Purchase
{
    public function __construct(array $event, array $data)
    {
        $this->required_data = array_merge(
            $this->required_data,
            [
                // @see https://schema.org/Duration
                'Product/Subscription.duration'
            ]
        );

        parent::__construct($event, $data);
    }
}
