<?php
namespace MachinePack\Core\Event\Events;

use MachinePack\Core\Event\Event;

class Webhook extends Event
{
    protected $required_data = [
        'Intangible/RESTRequest.server',
        'Intangible/RESTRequest.data',
        'Intangible/RESTRequest.body'
    ];

    public function __toString()
    {
        return 'This is a webhook event. ' . json_encode($this->getData());
    }
}
