<?php
declare(strict_types=1);

namespace MachinePack\Core\Test\Stress;

use PHPUnit\Framework\TestCase;
use MachinePack\Core\Result\Success;
use MachinePack\Core\MachinePack;

final class MachinePackBasicStressTest extends TestCase
{
    /**
     * returns how long should this system take in processing data and how much
     * memory it should take (WIP)
     *
     * first argument (length) means X handlers
     * second argument is how many events get sent
     * second argument is in seconds
     * third argument is in megabytes
     *
     * @return Array<Array<num_handlers:int,num_events:int,max_seconds:int,max_memory:int>>
     */
    public function getAcceptabilityData()
    {
        return [
            'S' => [
                10,
                10,
                1,
                1
            ],
            'M' => [
                100,
                1000,
                1,
                70
            ],
            'L' => [
                1000,
                1000,
                30,
                600
            ],
        ];
    }

    /**
     * basic callback generation for simple stress test
     * @dataProvider getAcceptabilityData
     */
    public function testLotsOfCallbackHandlers($num_handlers, $num_events, $time_limit_seconds, $memory_limit_megabytes)
    {
        $started_ts       = microtime(true);
        $initial_memory   = memory_get_usage(true);
        $data             = [];
        $callback_factory = function ($seed) use (&$data) {
            return function ($event) use ($seed, &$data) {
                $res = new Success(
                    [
                    'id' => "#$seed",
                    'payload' => random_bytes(64),
                    'event' => $event
                    ]
                );
                array_push($data, $res);
                return $res;
            };
        };

        MachinePack::init([]);
        $results = [];
        for ($i = 0; $i < $num_handlers; $i += 1) {
            MachinePack::subscribe('Payment.success', $callback_factory($i));
        }

        for ($i = 0; $i < $num_events; $i += 1) {
            MachinePack::send(
                'Payment.success',
                [
                    'MonetaryAmount.currency' => 'AUD',
                    'MonetaryAmount.value' => '1.00'
                ]
            );
        }

        $time_taken = microtime(true) - $started_ts;
        $this->assertLessThan($time_limit_seconds * 1000, $time_taken);

        $total_memory = memory_get_usage(true) - $initial_memory;

        $this->assertLessThan($memory_limit_megabytes, $total_memory / 1024.0 / 1024.0);
    }
}
