<?php
declare(strict_types=1);

namespace MachinePack\Core\Test\Unit;

use PHPUnit\Framework\TestCase;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Exception\MissingArguments as MissingArgumentsException;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Ignored;

final class MachinePackSkyTest extends TestCase
{
    /**
     * Test : Send transaction smart email
     */
    public function test()
    {
        $firstResult = new Success(true);

        $this->assertInstanceOf(Success::class, $firstResult);
    }
    /* public function testUserDetailsEvent()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createUserDetailsEvent();

        $results = MachinePack::send(
            'crmsync.create.sky',
            $event
        );
        var_dump($results);

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    } */

    private function _createUserDetailsEvent()
    {
        $event = array();
        $event['Intangible/Gift.appeal_id']       = '781';
        $event['Intangible/Gift.campaign_id']     = '417';
        $event['Intangible/Gift.fund_id']         = '62';
        $event['Intangible/Sky.action']           = 'get_custom_field_category_values';
        $event['Intangible/CustomField.category'] = 'Source';

        $event['Intangible/Sky.application_code'] = '9e6cc97fce06428f98ca4c7d1d73d00e';
        $event['Intangible/Sky.refresh_token']    = 'a0ea481a4ee74ce3a77bbd261e2ab24b';
        $event['Intangible/Sky.redirect_uri']     = 'https://av.local/?renxt_for_give_wp_callback=authorize';
        // phpcs:disable
        $event['Intangible/Sky.access_token']  = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IjREVjZzVkxIM0FtU1JTbUZqMk04Wm5wWHU3WSJ9.eyJuYW1laWQiOiIxYmY1ZTFmYS1kYTE1LTRmYTItOTU4Zi1hY2YyZjBkYzBkMjgiLCJhcHBsaWNhdGlvbmlkIjoiYTA1NmNhNmItYTNhOC00YWM3LWIzMjUtOTk3NjY2MzA2ZTUyIiwiZW52aXJvbm1lbnRpZCI6InAtOFpwbk9LLUJ0RXkyZm5vencyZ0ZRUSIsImVudmlyb25tZW50bmFtZSI6IkFuZ2xpY2FyZSBWaWN0b3JpYSBQcm9kdWN0aW9uIEVudmlyb25tZW50IiwibGVnYWxlbnRpdHlpZCI6InAtTDg2cWFseDFVRVdUalAwV29LbzRYZyIsImxlZ2FsZW50aXR5bmFtZSI6IkFuZ2xpY2FyZSBWaWN0b3JpYSIsInpvbmUiOiJwLWF1czAxIiwiaXNzIjoiaHR0cHM6Ly9vYXV0aDIuc2t5LmJsYWNrYmF1ZC5jb20vIiwiYXVkIjoiYmxhY2tiYXVkIiwiZXhwIjoxNjQ5NDE5Mzc5LCJuYmYiOjE2NDk0MTU3Nzl9.afoiWSG1RrguIkJb6hu6cU5KinoTduC_VMGUJC_UNVXW3BZw6-QFLev8fsN2bYBg5P0pPO3mueyCJRORZJ90UWw4kEDdp-zHZkIdk-rs60Hon1iYz1dt6Ca4VkC_Gbc7EV43XhbThP4kdUg1NB-HxCGfPBKZt__uOYOvZKlwph8DXHweizENYrwMMMUAIiw8OYLH-G6kdDqJjGcuJdxECz-wHAbkL4R2TXHFccMu5T0W5mRp1choFpHXmSkj44UZTSQnglBJYpazLVhK-p3IMyG7xeMWtrZ6ZYwYaw2dw_9zKElz8egxHOBIvHRRrnUZLKVwq9Oiaeyqpq9oVfWpHQ';
        // phpcs:enable

        $event['Intangible/Sky.client_id']        = '8358aa7d-3695-413a-bc18-859722470e4d';
        $event['Intangible/Sky.client_secret']    = '0R/4X/l7SrExbdZQKMN/xFf1Q1DHtI53yvrYHs1UvTY=';
        $event['Intangible/Sky.subscription_key'] = 'fe45bd1966ec4a6c908ec80e5860ffdf';

        $event['Intangible/Sky.Batch.identifier'] = '17990';
        $event['Intangible/Sky.Batch.added_by']   = 'integrations@marlincommunications.com';

        $event['Intangible/Event.payload'] = [
            // Type -> Individual or Organisation
            'type'                  => 'Individual',
            'address_first_name'    => 'Marlin',
            'address_last_name'     => 'Test',
            'address_email'         => 'av@marlincommunications.com',
            'address_phone'         => '+610415415555',
            //Phone Type -> Business, Direct, Home, Mobile, Primary Voice, Voice
            'phone_type'            => 'Home',
            'address_street'        => '272 Pacific Highway',
            'address_suburb'        => 'Crows Nest',
            'address_state'         => 'NSW',
            'address_postcode'      => '2065',
            'address_country'       => 'Australia',
            //Installment frequency of the recurring gift to add.
            //Available values are WEEKLY, EVERY_TWO_WEEKS, EVERY_FOUR_WEEKS, MONTHLY, QUARTERLY, ANNUALLY.
            'donation_frequency'    => 'MONTHLY',
            'donation_amount'       => '4.21',
            'reason'                => 'Approved',
            'account_token'         => '26d19e41-ebb8-4750-8f26-077cae67153f',
            'transaction_id'        => 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx',
            // Payment method -> Cash, Cheque, CreditCard, DebitCard, Other
            'gift_payment_method'   => 'CreditCard',
            // Payment Type "Donation" (OneTime), "RecurringGift", and "RecurringGiftPayment"
            // Not working for recurring gifts / Couldn't find any object in API to add CC details
            // Could be issue with Post date and credit card details
            'gift_payment_type'     => 'RecurringGift',
            'gift_reference'        => 'Newly added gift',
            'gift_general_ledger_status' => 'NotPosted',
            'patch_gift_receipt'    => true, //Only if you want to patch the receipt
            'gift_receipt_id'       => '2169',
            'gift_receipt_status'   => 'RECEIPTED',
            'gift_datetime'         => '2021-12-23T23:59:59',
            'payment_date'          => '2022-01-23 23:59:59', //Required only for recurring donations
            'custom_fields'         => ['Donor Source' => 'Online Donation'],
            "update_constituent_name" => true,
            "update_constituent_addresses" => true,
            "constituent_primary_address_type" => "Home",
            "constituent_former_address_type" => "Previous Address",
            "payment_configuration_id" => "e07c6b03-8fe7-4940-a984-6f790924135c",

            "batch_description" => 'This is a test',
            "batch_number" => uniqid(),
            "expected_batch_total" => "",
            "expected_number" => 20,
            "constituent_code" => "Major Donor",
        ];

        return $event;
    }
}
