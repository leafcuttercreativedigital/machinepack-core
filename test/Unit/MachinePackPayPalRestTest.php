<?php


namespace MachinePack\Core\Test\Unit;

use MachinePack\Core\MachinePack;
use MachinePack\Core\Result\Success;
use PHPUnit\Framework\TestCase;

class MachinePackPayPalRestTest extends TestCase
{
    /**
     * Test a PayPal One Off Payment Express Checkout creation
     */
    public function getOrder()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createGetOrderEvent();

        $results     = MachinePack::send('data.create.paypalrest', $event);
        $firstResult = array_pop($results);
        $this->assertInstanceOf(Success::class, $firstResult);
    }

    /**
     * Create a valid payment event
     */
    private function _createGetOrderEvent()
    {
        $event = array();

        $event['Intangible/PaypalRest.action']       = 'get_order';
        $event['Intangible/PaypalRest.clientId']     = '';
        $event['Intangible/PaypalRest.clientSecret'] = '';
        $event['Intangible/PaypalRest.sandbox']      = true;
        $event['Order.identifier']                   = '7N850259D3008430Y';

        return $event;
    }

    /**
     * Test Paypal Product Creation
     */
    public function createProduct()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event       = $this->_createProductEvent();
        $results     = MachinePack::send('subscription.create.paypalrest', $event);
        $firstResult = array_pop($results);
        $this->assertInstanceOf(Success::class, $firstResult);
    }

    private function _createProductEvent()
    {
        $event = array();

        $event['Intangible/PaypalRest.action']       = 'create_product';
        $event['Intangible/PaypalRest.clientId']     = '';
        $event['Intangible/PaypalRest.clientSecret'] = '';
        $event['Intangible/PaypalRest.sandbox']      = true;

        $event['Product.productId']   = 'Test_Product_' . uniqid();
        $event['Product.productName'] = 'Test_Payment_' . time();
        //$event['Intangible/PaypalRest.ProductType'] = 'SERVICE';
        //$event['Intangible/PaypalRest.ProductCategory'] = 'CHARITY';

        $event['MonetaryAmount.value']          = 10;
        $event['MonetaryAmount.currency']       = 'AUD';
        $event['Product/Subscription.duration'] = '';

        return $event;
    }

    /**
     * Test Paypal Plan Creation
     */
    public function createPlan()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event       = $this->_createPlanEvent();
        $results     = MachinePack::send('subscription.create.paypalrest', $event);
        $firstResult = array_pop($results);
        $this->assertInstanceOf(Success::class, $firstResult);
    }

    private function _createPlanEvent()
    {
        $event = array();

        $event['Intangible/PaypalRest.action']       = 'create_plan';
        $event['Intangible/PaypalRest.clientId']     = '';
        $event['Intangible/PaypalRest.clientSecret'] = '';
        $event['Intangible/PaypalRest.sandbox']      = true;

        $event['Intangible/PaypalRest.PlanName']        = 'Donation_Plan_' . time();
        $event['Intangible/PaypalRest.PlanDescription'] = 'Test transaction into PayPal';
        $event['Intangible/PaypalRest.ProductId']       = 'Test_Product_6333d7271ad4a';

        $event['MonetaryAmount.value']    = 10;
        $event['MonetaryAmount.currency'] = 'AUD';
        $event['Product.productId']       = '';

        //$event['MonetaryTax.percentage'] = 0;
        //$event['MonetaryTax.inclusive']  = false;

        $event['Intangible/PaypalRest.PlanFrequencyUnit']     = 'MONTH';
        $event['Intangible/PaypalRest.PlanFrequencyInterval'] = 1;
        //$event['Intangible/PaypalRest.PlanTrialFrequencyUnit']     = 'DAY';
        //$event['Intangible/PaypalRest.PlanTrialFrequencyInterval'] = '10';
        //$event['Intangible/PaypalRest.PlanContinueOnFailure']      = 'CONTINUE';
        //$event['Intangible/PaypalRest.PlanFailureTreshold']        = 3;
        //$event['Intangible/PaypalRest.PlanStatus'] = 'ACTIVE';

        $event['MonetaryAmount.value']          = 10;
        $event['MonetaryAmount.currency']       = 'AUD';
        $event['Product/Subscription.duration'] = '';

        return $event;
    }

    /**
     * Test a PayPal Subscription Payment Express Checkout creation
     */
    public function createSubscription()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createSubscriptionEvent();

        $results     = MachinePack::send('subscription.create.paypalrest', $event);
        $firstResult = array_pop($results);
        $this->assertInstanceOf(Success::class, $firstResult);
    }

    /**
     * Create a valid payment return event
     */
    private function _createSubscriptionEvent()
    {
        $event = array();

        $event['Intangible/PaypalRest.action']       = 'create_subscription';
        $event['Intangible/PaypalRest.clientId']     = '';
        $event['Intangible/PaypalRest.clientSecret'] = '';
        $event['Intangible/PaypalRest.PlanId']       = 'P-2J0399220E5591439MMZ5O2A';
        $event['Intangible/PaypalRest.sandbox']      = true;

        $event['MonetaryAmount.value']    = 10;
        $event['MonetaryAmount.currency'] = 'AUD';
        $event['Product.productId']       = '';

        $event['Person.givenName']  = 'MarlinDev';
        $event['Person.familyName'] = 'MachinePack';
        $event['Person.email']      = 'dev@marlincommunications.com';

        //$event['Intangible/PaypalRest.SubscriptionReturnUrl'] = '';
        //$event['Intangible/PaypalRest.SubscriptionCancelUrl'] = '';

        $event['Product/Subscription.duration'] = '';

        return $event;
    }
}
