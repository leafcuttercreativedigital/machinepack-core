<?php
declare(strict_types=1);

namespace MachinePack\Core\Test\Unit;

use PHPUnit\Framework\TestCase;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Exception\MissingArguments as MissingArgumentsException;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Ignored;

final class MachinePackBamboraTest extends TestCase
{
    public function test()
    {
        $firstResult = new Success(true);

        $this->assertInstanceOf(Success::class, $firstResult);
    }
    /**
     * Test a valid one-off credit card payment
     */
    /* public function testValidOneOffPayment()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidPaymentEvent();

        $results = MachinePack::send(
            'payment.create.bambora',
            $event
        );

        $firstResult = array_pop($results);
        $this->assertInstanceOf(Success::class, $firstResult);
    } */

    /* public function testValidPayPalPayment()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidPaymentEvent();

        $results = MachinePack::send(
            'payment.create.bambora',
            $event
        );

        var_dump($results);

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    } */

    /**
     * Test an invalid one-off credit card payment
     */
    /* public function testInvalidOneOffPayment()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidPaymentEvent();

        $results = MachinePack::send(
            'payment.create.bambora',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Failure::class, $firstResult);
    } */

    /**
     * Test a valid recurring credit card payment
     */
    /*public function testValidSubscription()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidPaymentEvent();
        $event['Product.productId'] = uniqid();

        $results = MachinePack::send(
            'subscription.create.bambora',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    }*/

    /**
     * Creates a valid payment event
     */
    private function _createValidPaymentEvent()
    {
        $event = array();
        $event['MonetaryAmount.value']          = 2;
        $event['MonetaryAmount.currency']       = 'AUD';
        $event['Order.identifier']              = uniqid();
        $event['Person.identifier']             = uniqid();
        $event['Order.description']             = 'Test transaction';
        $event['Person.givenName']              = 'John';
        $event['Person.familyName']             = 'Doe';
        $event['Person.telephone']              = 0404000000;
        $event['Person.email']                  = 'test@leafcutter.com.au';
        $event['PostalAddress.streetAddress']   = '123 Nowhere St';
        $event['PostalAddress.addressLocality'] = 'Crows Nest';
        $event['PostalAddress.addressRegion']   = 'NSW';
        $event['PostalAddress.postalCode']      = '2065';
        $event['PostalAddress.addressCountry']  = 'AU';
        $event['CreditCard/CardDetails.name']   = 'Martin Herko';
        $event['CreditCard/CardDetails.number'] = '4111111111111111';
        $event['CreditCard/CardDetails.expiryMonth'] = '11';
        $event['CreditCard/CardDetails.expiryYear']  = '2023';
        $event['CreditCard/CardDetails.cvn']         = '123';
        $event['CreditCard/CardDetails.type']        = 'visa';
        
        $event['Intangible/Customer.Reference'] = uniqid();
        $event['Intangible/Session.Key']        = uniqid();
        $event['Intangible/Server.URL']         = '/thank-you';
        $event['Intangible/User.URL']           = '/thank-you';

        $event['Order.PayPal']          = false;
        $event['Intangible/PPName']     = 'Test Bambora PayPal';
        $event['Intangible/PPDesc']     = 'This is a PayPal Donation via Bambora';
        $event['Intangible/Number']     = 1;
        $event['Intangible/NoShipping'] = false;
        $event['Intangible/BrandName']  = 'Marlin';

        $event['Intangible/API.tokeniseAlgorithmID'] = 8;
        $event['Intangible/API.username']            = 'Marlin';
        $event['Intangible/API.password']            = 'Marlin';
        $event['Intangible/API.accountNumber']       = 'Marlin';
        $event['Intangible/API.dtsapiURL']           = 'https://demo.bambora.co.nz/interface/api/dts.asmx';
        $event['Intangible/API.sippapiURL']          = 'https://demo.ippayments.com.au/interface/api/sipp.asmx';

        return $event;
    }
}
