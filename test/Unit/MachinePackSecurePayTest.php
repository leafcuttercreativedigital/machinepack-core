<?php
declare(strict_types=1);

namespace MachinePack\Core\Test\Unit;

use PHPUnit\Framework\TestCase;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Exception\MissingArguments as MissingArgumentsException;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Ignored;

final class MachinePackSecurePayTest extends TestCase
{
    public function test()
    {
        $firstResult = new Success(true);

        $this->assertInstanceOf(Success::class, $firstResult);
    }
    /**
     * Test a valid one-off credit card payment
     */
    /* public function testValidOneOffPayment()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidPaymentEvent();

        $results = MachinePack::send(
            'payment.create.securepay',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    } */
 
    /**
     * Test an invalid one-off credit card payment
     */
    /* public function testInvalidOneOffPayment()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidPaymentEvent();
        $event['CreditCard/CardDetails.number'] = '3333333333333333';
        $event['MonetaryAmount.value']          = 151;

        $results = MachinePack::send(
            'payment.create.securepay',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Failure::class, $firstResult);
    } */

    /**
     * Test a valid recurring credit card payment
     */
    /* public function testValidSubscription()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidPaymentEvent();
        $event['Product.productId']             = uniqid();
        $event['Product/Subscription.duration'] = '31/12/2050';

        $results = MachinePack::send(
            'subscription.create.securepay',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    } */

    /**
     * Creates a valid payment event
     */
    private function _createValidPaymentEvent()
    {
        $event = array();
        
        $event['MonetaryAmount.value']       = 10.08;
        $event['MonetaryAmount.currency']    = 'AUD';
        $event['Intangible/SecurePay.Token'] = '5932040440109590';

        $event['Intangible/SecurePay.auth_url']      = 'https://welcome.api2.sandbox.auspost.com.au/oauth/token';
        $event['Intangible/SecurePay.merchant_code'] = '5AR0055';
        $event['Intangible/SecurePay.client_id']     = '0oaxb9i8P9vQdXTsn3l5';
        $event['Intangible/SecurePay.client_secret'] = '0aBsGU3x1bc-UIF_vDBA2JzjpCPHjoCP7oI6jisp';
        $event['Intangible/SecurePay.ApiUrl']        = 'https://payments-stest.npe.auspost.zone';
        $event['Intangible/SecurePay.access_token']  = '';
        $event['Intangible/SecurePay.customer_code'] = "614c5e88da3ee";
        
        $event['Order.identifier'] = uniqid();
        $event['Person.ip']        = '127.0.0.1';
        //$event['Person.givenName']              = 'John';
        //$event['Person.familyName']             = 'Doe';
        
        return $event;
    }
}
