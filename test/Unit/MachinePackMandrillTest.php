<?php
declare(strict_types=1);

namespace MachinePack\Core\Test\Unit;

use PHPUnit\Framework\TestCase;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Exception\MissingArguments as MissingArgumentsException;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Ignored;

final class MachinePackMandrill extends TestCase
{
    public function test()
    {
        $firstResult = new Success(true);

        $this->assertInstanceOf(Success::class, $firstResult);
    }
    /**
     * Test : Send transaction smart email
     */
    /* public function testTransactionSmartEmail()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createTransactionSmartEmail();

        $results = MachinePack::send(
            'crmsync.create.mandrill',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    } */

    /**
     * Test : Send transaction smart email
     */
    /* public function testTemplatesList()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createTransactionSmartEmail();
        $event['Intangible/get_templates'] = true;

        $results = MachinePack::send(
            'crmsync.create.mandrill',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    }
    */
    private function _createTransactionSmartEmail()
    {
        $event = array();
        $event['Intangible/Mandrill.ApiKey'] = 'k8zR-vrGdAd5ax1AvUKj0A';
        $event['EmailMessage.template']      = 'test-template';
        $event['EmailMessage.recipient.to']  =  array(
                                                    array(
                                                        'email' => 'adrian@marlincommunications.com',
                                                        'name' => 'Adrian Alwishewa'
                                                    ),
                                                    array(
                                                        'email' => 'bct@marlincommunications.com',
                                                        'name' => 'John Doe 2'
                                                    )
                                                );

        $event['EmailMessage.cc']           = 'bct@marlincommunications.com';
        $event['EmailMessage.cc.name']      = 'CC Name';
        $event['EmailMessage.bcc']          = 'bct@marlincommunications.com';
        $event['EmailMessage.bcc.name']     = 'BCC Name';
        $event['EmailMessage.subject']      = 'Thank you for your donation';
        $event['EmailMessage.sender.email'] = 'noreply@marlincommunications.com';
        $event['EmailMessage.replyto']      = 'bct@marlincommunications.com';
        $event['EmailMessage.sender.name']  = 'Marlin Communications';
        
        $event['Intangible/Event.payload'] = [
            'first_name' => 'Test',
            'last_name' => 'User',
            'amount'=>'550'
        ];

        return $event;
    }
}
