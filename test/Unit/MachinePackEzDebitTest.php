<?php
declare(strict_types=1);

namespace MachinePack\Core\Test\Unit;

use PHPUnit\Framework\TestCase;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Exception\MissingArguments as MissingArgumentsException;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Ignored;

final class MachinePackEzDebitTest extends TestCase
{
    /**
     * Test a valid one-off credit card payment
     */
    public function testValidOneOffPayment()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');
        $event = $this->_createValidPaymentEvent();
        
        $results = MachinePack::send(
            'payment.create.ezdebit',
            $event
        );

        $firstResult = array_pop($results);
        $this->assertInstanceOf(Success::class, $firstResult);
    }
    /**
     * Test an invalid one-off credit card payment
     */
    /* public function testInvalidOneOffPayment()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidPaymentEvent();
        $event['CreditCard/CardDetails.number'] = '41';

        $results = MachinePack::send(
            'payment.create.ezdebit',
            $event
        );

        $firstResult = array_pop($results);
        $this->assertInstanceOf(Failure::class, $firstResult);
    } */

    /**
     * Test a valid recurring credit card payment
     */
    /* public function testValidSubscription()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidPaymentEvent();
        $event['Product/Subscription.duration'] = '31/12/2050';
        $event['Product.productId']             = 'RGMonth';

        $results     = MachinePack::send(
            'subscription.create.ezdebit',
            $event
        );
        $firstResult = array_pop($results);
        $this->assertInstanceOf(Success::class, $firstResult);
    } */

    /**
     * Creates a valid payment event
     */
    private function _createValidPaymentEvent()
    {
        $event = array();
        $event['MonetaryAmount.value']          = 12.00;
        $event['MonetaryAmount.currency']       = 'aud';
        $event['Order.identifier']              = uniqid();
        $event['Order.recurring']               = 'MONTHLY';
        $event['Person.givenName']              = 'Adrian';
        $event['Person.familyName']             = 'Alwishewa';
        $event['Person.telephone']              = '0425132242';
        $event['Person.email']                  = 'adrian2@marlincommunications.com';
        $event['PostalAddress.streetAddress']   = '272 Pacific Highway';
        $event['PostalAddress.addressLocality'] = 'Crows Nest';
        $event['PostalAddress.addressRegion']   = 'NSW';
        $event['PostalAddress.postalCode']      = '2065';
        $event['PostalAddress.addressCountry']  = 'AU';
        $event['CreditCard/CardDetails.name']   = 'John Doe';
        $event['CreditCard/CardDetails.number'] = '4111111111111111';
        $event['CreditCard/CardDetails.expiryMonth']  = '11';
        $event['CreditCard/CardDetails.expiryYear']   = '2022';
        $event['CreditCard/CardDetails.cvn']          = '123';
        $event['CreditCard/CardDetails.type']         = 'Visa';
        $event['Product.productId']                   = uniqid();
        $event['Intangible/EzDebit.systemReference']  = '663302e53d6a9';
        $event['Intangible/EzDebit.generalReference'] = 'general reference';
        $event['Intangible/EzDebit.smsReminder']      = 'YES';
        $event['Intangible/EzDebit.smsFailed']        = 'YES';
        $event['Intangible/EzDebit.smsExpired']       = 'YES';
        $event['Intangible/EzDebit.debitDate']        = date('Y-m-d');
        $event['Intangible/EzDebit.digitalKey']       = '3875E460-81A6-4D26-C616-E6D0C1183D23';
        $event['Intangible/EzDebit.isProductionMode'] = false;
        //$event['Intangible/EzDebit.action']           = 'get_customer_details';

        //$event = array();
        //$event['Intangible/EzDebit.action']          = "get_customer_details";
        /* $event['Intangible/EzDebit.digitalKey']          = "3875E460-81A6-4D26-C616-E6D0C1183D23";
        $event['Intangible/EzDebit.systemReference']          = "663302e53d6a9";
        $event['MonetaryAmount.value']          = 12.00;
        $event['MonetaryAmount.currency']          = 'aud'; */


        return $event;
    }
}
