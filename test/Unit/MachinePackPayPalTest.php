<?php


namespace MachinePack\Core\Test\Unit;

use MachinePack\Core\MachinePack;
use MachinePack\Core\Result\Success;
use PHPUnit\Framework\TestCase;

class MachinePackPayPalTest extends TestCase
{
    /**
     * Test a PayPal Subscription Payment Express Checkout creation
     */
    public function testSubscription()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidPaymentEvent();

        $results     = MachinePack::send('subscription.create.paypal', $event);
        $firstResult = array_pop($results);
        $this->assertInstanceOf(Success::class, $firstResult);
    }

    /**
     * Test a PayPal One Off Payment Express Checkout creation
     */
    public function testOneTimePayment()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidPaymentEvent();

        $results     = MachinePack::send('payment.create.paypal', $event);
        $firstResult = array_pop($results);
        $this->assertInstanceOf(Success::class, $firstResult);
    }

    /**
     * Test PayPal payment return handling
     */

    /*
    //NOTE - Change Order.token within  _createValidReturnEvent to test.
     public function testReturnPayment()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidReturnEvent();

        $results     = MachinePack::send('payment.return.paypal', $event);
        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    }
    */

    /**
     * Create a valid payment event
     */
    private function _createValidPaymentEvent()
    {
        $event = array();

        $event['Order.identifier']  = 11111111;
        $event['Order.description'] = 'Test transaction into PayPal';
        $event['Order.name']        = 'Donation';

        $event['Brand.name'] = 'Leukaemia';

        $event['Intangible/PayPal.returnUrl'] = 'http://leafcutter.com.au/';
        $event['Intangible/PayPal.cancelUrl'] = 'http://leafcutter.com.au/';
        $event['Intangible/PayPal.shipping']  = 1;
        $event['Intangible/PayPal.note']      = false;

        $event['Person.givenName']  = 'John';
        $event['Person.familyName'] = 'Doe';
        $event['Person.telephone']  = 0404000000;
        $event['Person.email']      = 'test@leafcutter.com.au';
        $event['Person.password']   = 'test@leafcutter.com.au';

        $event['PostalAddress.streetAddress']   = '123 Nowhere St';
        $event['PostalAddress.addressLocality'] = 'Crows Nest';
        $event['PostalAddress.addressRegion']   = 'NSW';
        $event['PostalAddress.postalCode']      = '2065';
        $event['PostalAddress.addressCountry']  = 'AU';

        $event['CreditCard/CardDetails.name']        = 'John Doe';
        $event['CreditCard/CardDetails.number']      = '4444333322221111';
        $event['CreditCard/CardDetails.expiryMonth'] = '12';
        $event['CreditCard/CardDetails.expiryYear']  = '25';
        $event['CreditCard/CardDetails.cvn']         = '123';

        $event['MonetaryAmount.value']    = 10.00;
        $event['MonetaryAmount.currency'] = 'AUD';

        $event['Product/Subscription.duration'] = 2;
        $event['Product.productId']             = 147258369;
        return $event;
    }

    /**
     * Create a valid payment return event
     */
    private function _createValidReturnEvent()
    {
        $event = array();

        $event['Order.token']       = 'EC-92X76869CL127580E';
        $event['Order.description'] = 'Test transaction into PayPal';

        $event['Payment.identifier'] = 'GQXY43RAHM5QU';
        $event['Payment.type']       = 'recurring';

        $event['Billing.frequency'] = 1;
        $event['Billing.period']    = 'Month';

        $event['MonetaryAmount.value']    = 10.00;
        $event['MonetaryAmount.currency'] = 'AUD';

        return $event;
    }
}
