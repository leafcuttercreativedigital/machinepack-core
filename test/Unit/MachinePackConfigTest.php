<?php
declare(strict_types=1);

namespace MachinePack\Core\Test\Unit;

use PHPUnit\Framework\TestCase;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Exception\Settings as SettingsException;
use MachinePack\Core\Event\Events\Payment;

final class MachinePackConfigTest extends TestCase
{
    /**
     * invalid settings throw a nice exception
     */
    public function testConfigFromMissingFile()
    {
        $this->expectException(SettingsException::class);
        MachinePack::init(__DIR__ . '/not_found');
    }

    /**
     * Init handles a defined constant
     */
    public function testLoggerConfigFromConstant()
    {
        MachinePack::init(__DIR__ . '/MachinePackConfigTest.logger.yml');
        $this->_paymentAssertions();
    }

    /**
     * Init handles a file name
     */
    public function testLoggerConfigFromFilename()
    {
        MachinePack::init(__DIR__ . '/MachinePackConfigTest.logger.yml');
        $this->_paymentAssertions();
    }

    /**
     * Init handles a file with environment constants
     */
    public function testLoggerConfigWithConstants()
    {
        MachinePack::init(__DIR__ . '/MachinePackConfigTest.env_extensions.yml');
        $this->_paymentAssertions();
    }

    /**
     * basic Logger captures events and logs event data
     */
    private function _paymentAssertions()
    {
        $payment_data = [
            'MonetaryAmount.currency' => 'AUD',
            'MonetaryAmount.value' => '1.00'
        ];

        ob_start();
        MachinePack::send('payment.one-off.success', $payment_data);
        $logger_out = ob_get_contents();
        ob_end_clean();

        $this->assertRegexp('/' . preg_quote(Payment::class) . '.*1.00 AUD/', $logger_out);

        ob_start();
        MachinePack::send('payment.one-off.failure', $payment_data);
        $logger_out = ob_get_contents();
        ob_end_clean();

        foreach (explode(PHP_EOL, $logger_out) as $log_row) {
            $this->assertRegexp('/' . preg_quote(Payment::class) . '.*1.00 AUD/', $logger_out);
        }
    }
}
