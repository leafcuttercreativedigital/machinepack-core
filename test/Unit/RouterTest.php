<?php
declare(strict_types=1);

namespace MachinePack\Core\Test\Unit;

use PHPUnit\Framework\TestCase;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Collection as ResultCollection;

final class RouterTest extends TestCase
{
    public function testSimpleRouting()
    {
        MachinePack::init(__DIR__ . '/RouterTest.yml');
        list($results) = MachinePack::send(
            'payment.create',
            [
                'Intangible/Gateway.name' => 'GateWay1',
                'MonetaryAmount.currency' => 'AUD',
                'MonetaryAmount.value' => '1'
            ]
        );

        $result = array_shift($results->data);

        $this->assertInstanceOf(Success::class, $result);
        $this->assertContains('gateway1', $result->data['message']);
        $this->assertContains('1 AUD', $result->data['message']);
    }

    public function testComplexRouting()
    {
        MachinePack::init(__DIR__ . '/RouterTest.yml');
        list($results) = MachinePack::send(
            'payment.custom1',
            [
                'Order.identifier' => uniqid(),
                'PostalAddress.addressRegion' => 'NSW',
                'PostalAddress.addressCountry' => 'AU',
                'CreditCard/CardDetails.type' => 'visa',
                'MonetaryAmount.currency' => 'AUD',
                'MonetaryAmount.value' => '1'
            ]
        );

        $this->assertInstanceOf(ResultCollection::class, $results['payment.nsw']);
        $this->assertInstanceOf(Ignored::class, $results['payment.default']);

        $result_1 = $results['payment.nsw']['payment.nsw_visa'];
        $this->assertInstanceOf(Success::class, $result_1);
        $this->assertContains('nsw_visa', $result_1->data['message']);
        $this->assertContains('1 AUD', $result_1->data['message']);

        $result_2 = $results['payment.nsw']['payment.nsw_default'];
        $this->assertInstanceOf(Ignored::class, $result_2);
    }
}
