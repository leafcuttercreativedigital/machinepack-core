<?php
declare(strict_types=1);

namespace MachinePack\Core\Test\Unit;

use PHPUnit\Framework\TestCase;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Ignored;

/**
 * Basic emailer tests
 */
final class EmailTest extends TestCase
{
    protected function setUp()
    {
        MachinePack::init(__DIR__ . '/EmailTest.yml');
    }

    /**
     * Validates email templating works, with mail() disabled
     *
     * @return void
     */
    public function testTemplating()
    {
        list(
            $customer_result,
            $admin_result
        ) = MachinePack::send(
            'payment.create',
            [
                'Person.name' => 'Pat Doe',
                'Person.email' => 'pat.doe@example.com',
                'MonetaryAmount.value' => 1,
                'MonetaryAmount.currency' => 'AUD',
                'Order.identifier' => uniqid()
            ]
        );

        $this->assertEquals('<Pat Doe> pat.doe@example.com', $customer_result->data['to']);
        $this->assertEquals('my.shop.admin@example.com', $admin_result->data['to']);
        $this->assertContains('CC: my.other.shop.admin@example.com', $admin_result->data['headers']);
        $this->assertContains('<h1>New order ', $admin_result->data['message']);
    }

    /**
     * Dummy test checks if wordpress gets called
     *
     * @return void
     */
    public function testWordpress()
    {
        $error = '';
        try {
            list(
                $customer_result
            ) = MachinePack::send(
                'payment.create.wordpress',
                [
                    'Person.name' => 'Pat Doe',
                    'Person.email' => 'pat.doe@example.com',
                    'MonetaryAmount.value' => 1,
                    'MonetaryAmount.currency' => 'AUD',
                    'Order.identifier' => uniqid()
                ]
            );
        } catch (\Error $e) {
            $error = $e->getMessage();
        }
        $this->assertEquals('Call to undefined function wp_mail()', $error);
    }

    /**
     * Simple example with raw shopify data
     *
     * @return void
     */
    public function testShopifyData()
    {
        $error           = '';
        $shopify_body    = file_get_contents(__DIR__ . '/EmailTest_shopify_body.json');
        $shopify_headers = [
            'REMOTE_ADDR' => '127.0.0.1',
            'REMOTE_PORT' => '38696',
            'SERVER_PROTOCOL' => 'HTTP/1.1',
            'REQUEST_URI' => '/',
            'REQUEST_METHOD' => 'GET',
            'PHP_SELF' => '/index.php',
            'HTTP_HOST' => '127.0.0.1:8080',
            'HTTP_ACCEPT' => '*/*',
            'HTTP_X_SHOPIFY_TOPIC' => 'orders/create',
            'HTTP_X_SHOPIFY_HMAC_SHA256' => 'XWmrwMey6OsLMeiZKwP4FppHH3cmAiiJJAweH5Jo4bM=',
            'HTTP_X_SHOPIFY_SHOP_DOMAIN' => 'johns-apparel.myshopify.com',
            'HTTP_X_SHOPIFY_API_VERSION' => '2019-04'
        ];

        // First hook, purchase
        list(
            $result,
            $log
        ) = MachinePack::send(
            'webhook.shopify',
            [
                'Intangible/RESTRequest.server' => $shopify_headers,
                'Intangible/RESTRequest.data' => [],
                'Intangible/RESTRequest.body' => $shopify_body
            ]
        );

        $this->assertContains(
            '<p>Thank you for your order John!<p> <p>Your 403.00 USD',
            $result->data['message']
        );

        // Second hook, refund
        $shopify_headers['HTTP_X_SHOPIFY_TOPIC'] = 'refunds/create';
        list(
            $result,
            $log
        ) = MachinePack::send(
            'webhook.shopify',
            [
                'Intangible/RESTRequest.server' => $shopify_headers,
                'Intangible/RESTRequest.data' => [],
                'Intangible/RESTRequest.body' => $shopify_body
            ]
        );

        $this->assertContains(
            '<p>Your refund has been sent.</p>',
            $result->data['message']
        );

        // Third hook, ignored
        $shopify_headers['HTTP_X_SHOPIFY_TOPIC'] = 'themes/create';
        list(
            $result,
            $log
        ) = MachinePack::send(
            'webhook.shopify',
            [
                'Intangible/RESTRequest.server' => $shopify_headers,
                'Intangible/RESTRequest.data' => [],
                'Intangible/RESTRequest.body' => $shopify_body
            ]
        );

        $this->assertContains('Shopify hook: themes/create #820982911946154508', $log->data['message']);
        $this->assertInstanceOf(Ignored::class, $result);
    }
}
