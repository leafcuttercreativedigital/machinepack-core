<?php
declare(strict_types=1);

namespace MachinePack\Core\Test\Unit;

use PHPUnit\Framework\TestCase;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Exception\MissingArguments as MissingArgumentsException;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Ignored;

final class MachinePackCampaignMonitorTest extends TestCase
{
    public function testClientLists()
    {
        MachinePack::init(__DIR__ . '/MachinePackConfigTest.crm.yml');

        $event = $this->_createGetClientListsEvent();

        $results = MachinePack::send(
            'crmsync.create.campaignmonitor',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    }

    /**
     * Test : Import contacts to subscriber list
     */
    public function testSubscribersImport()
    {
        MachinePack::init(__DIR__ . '/MachinePackConfigTest.crm.yml');

        $event = $this->_createSubscribersListEvent();

        $results = MachinePack::send(
            'crmsync.create.campaignmonitor',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    }

    /**
     * Test : Send transaction smart email
     */
    public function testTransactionSmartEmail()
    {
        MachinePack::init(__DIR__ . '/MachinePackConfigTest.crm.yml');

        $event = $this->_createTransactionSmartEmail();

        $results = MachinePack::send(
            'crmsync.create.campaignmonitor',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    }

    /**
     * Test : Creating new subscriber to list
     */
    public function testCreateSubscriber()
    {
        MachinePack::init(__DIR__ . '/MachinePackConfigTest.crm.yml');

        $rand = uniqid();
        
        $event = $this->_createSubscriberEvent($rand);

        $results = MachinePack::send(
            'crmsync.create.campaignmonitor',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    }

    /**
     * Test : Updating subscriber in list
     */
    public function testUpdateSubscriber()
    {
        MachinePack::init(__DIR__ . '/MachinePackConfigTest.crm.yml');

        $rand = uniqid();
        
        $event = $this->_createSubscriberEvent($rand);

        $results = MachinePack::send(
            'crmsync.create.campaignmonitor',
            $event
        );

        $updateEvent = $this->_updateSubscriberEvent($rand);

        $updateResults = MachinePack::send(
            'crmsync.create.campaignmonitor',
            $updateEvent
        );

        $firstResult = array_pop($updateResults);

        $this->assertInstanceOf(Success::class, $firstResult);
    }

    /**
     * Test : Delete subscriber from list
     */
    public function testDeleteSubscriber()
    {
        MachinePack::init(__DIR__ . '/MachinePackConfigTest.crm.yml');

        $rand = uniqid();
        
        $event = $this->_createSubscriberEvent($rand);

        $results = MachinePack::send(
            'crmsync.create.campaignmonitor',
            $event
        );

        $deleteEvent = $this->_deleteSubscriberEvent($rand);

        $deleteResults = MachinePack::send(
            'crmsync.create.campaignmonitor',
            $deleteEvent
        );

        $firstResult = array_pop($deleteResults);

        $this->assertInstanceOf(Success::class, $firstResult);
    }

    /**
     * Test : Get custom fields list
     */
    public function testCustomfieldList()
    {
        MachinePack::init(__DIR__ . '/MachinePackConfigTest.crm.yml');
        
        $event = $this->_customfieldListEvent();

        $results = MachinePack::send(
            'crmsync.create.campaignmonitor',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    }

    /**
     * Test : Create custom field
     */
    /*  public function testCreateCustomfield()
    {
        MachinePack::init(__DIR__ . '/MachinePackConfigTest.crm.yml');

        $event = $this->_createCustomfieldEvent();

        $results = MachinePack::send(
            'crmsync.create.campaignmonitor',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    } */

    private function _createGetClientListsEvent()
    {
        $event = array();
        $event['Intangible/CampaignMonitor.api_key']   = 'f30a882c3d4bb3b3128397a5fa65c1c4f2440a65bbd78613';
        $event['Intangible/CampaignMonitor.client_id'] = 'b342e3317aa0ea39cfb060c836455796';
        $event['Intangible/Event.action']              = 'clientLists';
        $event['Intangible/Event.payload']             = [];

        return $event;
    }

    /**
     * Creates a valid contact event
     */
    private function _createSubscribersListEvent()
    {
        $event = array();
        $event['Intangible/CampaignMonitor.api_key']     = 'f30a882c3d4bb3b3128397a5fa65c1c4f2440a65bbd78613';
        $event['Intangible/CampaignMonitor.client_id']   = 'b342e3317aa0ea39cfb060c836455796';
        $event['Intangible/Event.action']                = 'subscribersList';
        $event['Intangible/Event.batch_limit']           = 100;
        $event['Intangible/CampaignMonitor.list_id']     = '015104ecc31473d8482e13cb5a8f9bb9';
        $event['Intangible/CampaignMonitor.resubscribe'] = false;

        $testSubscribersList = [];
        for ($i = 1; $i <= 4; $i++) {
            $testSubscribersList[] = [
                'EmailAddress' => 'mptest' . $i . '@leafcutter.com.au',
                'Name' => 'Leafcutter Test ' . $i,
                'CustomFields' => [
                    ['Key' => 'testDate', 'Value' => date('d/m/Y H:i:s')]
                ],
                'ConsentToTrack' => 'Yes',
                'Resubscribe' => true
            ];
        }

        $event['Intangible/CampaignMonitor.subscribers'] = $testSubscribersList;
        $event['Intangible/Event.payload']               = [];

        return $event;
    }

    private function _createTransactionSmartEmail()
    {
        $event = array();
        $event['Intangible/CampaignMonitor.api_key']   = 'f30a882c3d4bb3b3128397a5fa65c1c4f2440a65bbd78613';
        $event['Intangible/CampaignMonitor.client_id'] = 'b342e3317aa0ea39cfb060c836455796';
        $event['Intangible/CampaignMonitor.smart_id']  = '2679fdab-5988-4a3a-bd78-f183a79ec4f5';
        $event['Intangible/Event.action']              = 'transactionSmartEmail';
        $event['EmailMessage.recipient.email']         = 'machinepack@leafcutter.com.au';
        $event['EmailMessage.cc']          = 'ccmachinepack@gmail.com';
        $event['EmailMessage.bcc']         = 'bccmachinepack@gmail.com';
        $event['Intangible/Event.payload'] = [
            'emailTitle'=>'Machine Pack Test Email'
        ];

        return $event;
    }

    private function _createSubscriberEvent($rand)
    {
        $event = array();
        $event['Intangible/CampaignMonitor.api_key']     = 'f30a882c3d4bb3b3128397a5fa65c1c4f2440a65bbd78613';
        $event['Intangible/CampaignMonitor.client_id']   = 'b342e3317aa0ea39cfb060c836455796';
        $event['Intangible/Event.action']                = 'createSubscriber';
        $event['Intangible/Event.batch_limit']           = 100;
        $event['Intangible/CampaignMonitor.list_id']     = '583ec013959fd09cee804fad24431ab9';
        $event['Intangible/CampaignMonitor.resubscribe'] = false;

        $testSubscriber = [
            'EmailAddress' => 'mptest'.$rand.'test@leafcutter.com.au',
            'Name' => 'Leafcutter Test ' . $rand,
            'CustomFields' => [
                [
                    'Key' => 'PURL_ID',
                    'Value' => $rand
                ]
            ],
            'ConsentToTrack' => 'Yes',
            'Resubscribe' => true
        ];
        

        $event['Intangible/CampaignMonitor.subscriber'] = $testSubscriber;
        $event['Intangible/Event.payload']              = [];

        return $event;
    }

    private function _updateSubscriberEvent($rand)
    {
        $event = array();
        $event['Intangible/CampaignMonitor.api_key']     = 'f30a882c3d4bb3b3128397a5fa65c1c4f2440a65bbd78613';
        $event['Intangible/CampaignMonitor.client_id']   = 'b342e3317aa0ea39cfb060c836455796';
        $event['Intangible/Event.action']                = 'updateSubscriber';
        $event['Intangible/Event.batch_limit']           = 100;
        $event['Intangible/CampaignMonitor.list_id']     = '583ec013959fd09cee804fad24431ab9';
        $event['Intangible/CampaignMonitor.resubscribe'] = false;
        $event['Intangible/CampaignMonitor.email']       = 'mptest'.$rand.'test@leafcutter.com.au';


        $new_rand = uniqid();

        $testSubscriber = [
            'EmailAddress' => 'mptest'.$new_rand.'test@leafcutter.com.au',
            'Name' => 'Updated subscriber Test ' . $rand,
            'CustomFields' => [
                [
                    'Key' => 'PURL_ID',
                    'Value' => $rand
                ]
            ],
            'ConsentToTrack' => 'Yes',
            'Resubscribe' => true
        ];
        

        $event['Intangible/CampaignMonitor.subscriber'] = $testSubscriber;
        $event['Intangible/Event.payload']              = [];

        return $event;
    }

    private function _deleteSubscriberEvent($rand)
    {
        $event = array();
        $event['Intangible/CampaignMonitor.api_key']     = 'f30a882c3d4bb3b3128397a5fa65c1c4f2440a65bbd78613';
        $event['Intangible/CampaignMonitor.client_id']   = 'b342e3317aa0ea39cfb060c836455796';
        $event['Intangible/Event.action']                = 'deleteSubscriber';
        $event['Intangible/Event.batch_limit']           = 100;
        $event['Intangible/CampaignMonitor.list_id']     = '583ec013959fd09cee804fad24431ab9';
        $event['Intangible/CampaignMonitor.resubscribe'] = false;
        $event['Intangible/CampaignMonitor.email']       = 'mptest'.$rand.'test@leafcutter.com.au';
        $event['Intangible/Event.payload']               = [];

        return $event;
    }

    private function _customfieldListEvent()
    {
        $event = array();
        $event['Intangible/CampaignMonitor.api_key']   = 'f30a882c3d4bb3b3128397a5fa65c1c4f2440a65bbd78613';
        $event['Intangible/CampaignMonitor.client_id'] = 'b342e3317aa0ea39cfb060c836455796';
        $event['Intangible/Event.action']              = 'customfieldList';
        $event['Intangible/CampaignMonitor.list_id']   = '583ec013959fd09cee804fad24431ab9';
        $event['Intangible/Event.payload']             = [];

        return $event;
    }

    private function _createCustomfieldEvent()
    {
        $event = array();
        $event['Intangible/CampaignMonitor.api_key']   = 'f30a882c3d4bb3b3128397a5fa65c1c4f2440a65bbd78613';
        $event['Intangible/CampaignMonitor.client_id'] = 'b342e3317aa0ea39cfb060c836455796';
        $event['Intangible/Event.action']              = 'createCustomfield';
        $event['Intangible/Event.batch_limit']         = 100;
        $event['Intangible/CampaignMonitor.list_id']   = '583ec013959fd09cee804fad24431ab9';

        $rand = uniqid();

        $custom_fields = [
                'FieldName' => 'Custom field test ' . $rand,
                'DataType' => 'Text'
        ];

        $event['Intangible/CampaignMonitor.fields'] = $custom_fields;
        $event['Intangible/Event.payload']          = [];

        return $event;
    }
}
