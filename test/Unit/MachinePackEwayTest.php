<?php
declare(strict_types=1);

namespace MachinePack\Core\Test\Unit;

use PHPUnit\Framework\TestCase;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Exception\MissingArguments as MissingArgumentsException;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Ignored;

final class MachinePackEwayTest extends TestCase
{
    public function test()
    {
        $firstResult = new Success(true);

        $this->assertInstanceOf(Success::class, $firstResult);
    }
    /**
     * Test a valid one-off credit card payment
     */
    /* public function testValidOneOffPayment()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidPaymentEvent();

        $results = MachinePack::send(
            'payment.create.eway',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    }
    */
    /**
     * Test a invalid one-off credit card payment (based on wrong card number)
     */
    /* public function testInvalidOneOffPayment()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidPaymentEvent();
        $event['CreditCard/CardDetails.number'] = '333322221111'; //set invalid card number

        $results = MachinePack::send(
            'payment.create.eway',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Failure::class, $firstResult);
    } */

    /**
     * Test a valid recurring credit card payment
     */
    /* public function testValidSubscription()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidPaymentEvent();
        $event['Product.productId']             = 'DONONLINE_RECURRING';
        $event['Product/Subscription.duration'] = '31/12/2050';

        $results = MachinePack::send(
            'subscription.create.eway',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    } */

    /**
     * Test a valid recurring credit card payment
     */
    /* public function testValidCustomerCreation()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidPaymentEvent();
        $event['Product.productId']             = 'DONONLINE_RECURRING';
        $event['Product/Subscription.duration'] = '31/12/2050';
        $event['action'] = 'customer_token';

        $results = MachinePack::send(
            'subscription.create.eway',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    } */

    /**
     * Test a valid update customer token
     */
    /* public function testValidUpdateCustomerToken()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidPaymentEvent();
        $event['Product.productId']             = 'DONONLINE_RECURRING';
        $event['Product/Subscription.duration'] = '31/12/2050';
        $event['MonetaryAmount.value']          = 0;
        $event['action'] = 'customer_token';

        $results = MachinePack::send(
            'subscription.create.eway',
            $event
        );

        $firstResult = array_pop($results);

        $event['action']          = 'update_customer_token';
        $event['TokenCustomerID'] = $firstResult->data['TokenCustomerID'];

        $results = MachinePack::send(
            'subscription.create.eway',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    }
    */
    /**
     * Test a valid recurring credit card payment
     */
    /* public function testValidCustomerTokenCharge()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidPaymentEvent();
        $event['Product.productId']             = 'DONONLINE_RECURRING';
        $event['Product/Subscription.duration'] = '31/12/2050';
        $event['action'] = 'customer_token_charge';

        $results = MachinePack::send(
            'subscription.create.eway',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    } */

    /**
     * Creates a valid payment event
     */
    private function _createValidPaymentEvent()
    {
        $event = array();
        $event['Order.identifier']            = 11111111;
        $event['Order.description']           = 'Test transaction into eWay';
        $event['Person.givenName']            = 'John';
        $event['Person.familyName']           = 'Doe';
        $event['Person.telephone']            = 0404000000;
        $event['Person.email']                = 'test@leafcutter.com.au';
        $event['Person.IP']                   = '192.0.0.1';
        $event['PostalAddress.streetAddress'] = '123 Nowhere St';
        $event['PostalAddress.addressLocality']      = 'Crows Nest';
        $event['PostalAddress.addressRegion']        = 'NSW';
        $event['PostalAddress.postalCode']           = '2065';
        $event['PostalAddress.addressCountry']       = 'au';
        $event['CreditCard/CardDetails.name']        = 'John Smith';
        $event['CreditCard/CardDetails.number']      = '4444333322221111';
        $event['CreditCard/CardDetails.expiryMonth'] = '12';
        $event['CreditCard/CardDetails.expiryYear']  = '25';
        $event['CreditCard/CardDetails.cvn']         = '123';
        $event['MonetaryAmount.value']               = 10.00;
        $event['MonetaryAmount.currency']            = 'AUD';
        $event['Intangible/eWAY.CustomerTokenID']    = '914212964040';

        $event['Intangible/eWAY.apiKey']            = '914212964040';
        $event['Intangible/eWAY.apiPassword']       = '914212964040';
        $event['Intangible/eWAY.recurringUrl']      = '914212964040';
        $event['Intangible/eWAY.recurringKey']      = '914212964040';
        $event['Intangible/eWAY.recurringPassword'] = '914212964040';
        $event['Intangible/eWAY.isProductionMode']  = 'true';

        return $event;
    }
}
