<?php
declare(strict_types=1);

namespace MachinePack\Core\Test\Unit;

use PHPUnit\Framework\TestCase;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Exception\MissingArguments as MissingArgumentsException;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Ignored;

final class MachinePackPardotTest extends TestCase
{

    public function test()
    {
        $firstResult = new Success(true);

        $this->assertInstanceOf(Success::class, $firstResult);
    }
    /**
     * Test : Send transaction smart email
     */
    /* public function testTransactionSmartEmail()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createTransactionSmartEmail();

        $results = MachinePack::send(
            'crmsync.create.pardot',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    } */

    private function _createTransactionSmartEmail()
    {
        $event = array();
        $event['Intangible/Pardot.campaign_id']       = '52163';
        $event['Intangible/Pardot.email_template_id'] = '34867';

        $event['EmailMessage.recipient.givenName']  = 'John';
        $event['EmailMessage.recipient.familyName'] = 'Doe';
        $event['EmailMessage.recipient.email']      = 'adrian@leafcutter.com.au';
        $event['EmailMessage.subject']              = 'Thank you for your donation';
        
        $event['Intangible/Event.payload'] = [
            'phone'                             => '0400000000',
            'address_one'                       => 'Unit 1',
            'address_two'                       => '12 Pitt St',
            'city'                              => 'Sydney',
            'state'                             => 'NSW',
            'zip'                               => '2000',
            'country'                           => 'Australia',
            'leafcutter_donation_frequency'     => 'Once',
            'leafcutter_donation_amount'        => '$100.00',
            'leafcutter_payment_gateway'        => 'Credit Card',
            'leafcutter_payment_type'           => 'Donation',
            'leafcutter_transaction_id'         => '370251649',
            'leafcutter_token'                  => 'TOKEN',
            'leafcutter_status'                 => 'active',
            'leafcutter_on_behalf_of'           => 'demo on behalf',
            'leafcutter_on_behalf_first_name'   => 'John',
            'leafcutter_on_behalf_last_name'    => 'Doe',
            'leafcutter_tribute_type'           => 'Demo tribute type value',
            'transaction_date'                  => '27 Feb, 2020 07:01 AM'
        ];

        return $event;
    }
}
