<?php
declare(strict_types=1);

namespace MachinePack\Core\Test\Unit;

use PHPUnit\Framework\TestCase;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Exception\MissingArguments as MissingArgumentsException;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Ignored;

final class MachinePackBraintreeTest extends TestCase
{
    public function test()
    {
        $firstResult = new Success(true);

        $this->assertInstanceOf(Success::class, $firstResult);
    }
    /**
     * Test a valid one-off credit card payment
     */
    /* public function testValidOneOffPayment()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidPaymentEvent();

        $results = MachinePack::send(
            'payment.create.braintree',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    } */

    /**
     * Test a invalid one-off credit card payment (based on wrong card number)
     */
    /* public function testInvalidOneOffPayment()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidPaymentEvent();
        $event['CreditCard/CardDetails.number'] = '333322221111'; //set invalid card number

        $results = MachinePack::send(
            'payment.create.braintree',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Failure::class, $firstResult);
    } */

    /**
     * Test a valid recurring credit card payment
     */
    /* public function testValidSubscription()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidPaymentEvent();
        $event['Product.productId']             = 'sandbox_PLAN';
        $event['Product/Subscription.duration'] = '31/12/2050';

        $results = MachinePack::send(
            'subscription.create.braintree',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    } */

    /**
     * Creates a valid payment event
     */
    private function _createValidPaymentEvent()
    {
        $event = array();
        $event['Person.givenName']              = 'John recurring';
        $event['Person.familyName']             = 'Doe';
        $event['Person.telephone']              = 0404000000;
        $event['Person.email']                  = 'test@leafcutter.com.au';
        $event['PostalAddress.streetAddress']   = '123 Nowhere St';
        $event['PostalAddress.addressLocality'] = 'Crows Nest';
        $event['PostalAddress.addressRegion']   = 'NSW';
        $event['PostalAddress.postalCode']      = '2065';
        $event['PostalAddress.addressCountry']  = 'au';
        $event['CreditCard/CardDetails.name']   = 'John Smith';
        $event['CreditCard/CardDetails.number'] = '4111111111111111';
        $event['CreditCard/CardDetails.expiryMonth'] = '12';
        $event['CreditCard/CardDetails.expiryYear']  = '25';
        $event['CreditCard/CardDetails.cvn']         = '123';
        $event['MonetaryAmount.value']               = 10.00;
        $event['MonetaryAmount.currency']            = 'AUD';
        return $event;
    }
}
