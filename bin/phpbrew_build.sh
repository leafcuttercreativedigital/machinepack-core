#!/bin/bash

source $HOME/.phpbrew/bashrc

phpbrew install "${1}" +curl +filter +soap +zip +bz2 +json +ctype +hash +iconv 

BUILD_CURRENT_PHP=$(phpbrew list | grep "${1}" | tr -d "*")

source $HOME/.phpbrew/bashrc

phpbrew switch $BUILD_CURRENT_PHP

for extension in yaml openssl mbstring
do
		phpbrew ext install $extension
		phpbrew ext enable $extension
done

phpbrew clean $BUILD_CURRENT_PHP
