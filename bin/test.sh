#!/bin/bash

set +u
source /root/.bashrc

BUILD_CURRENT_PHP=$(phpbrew list | grep "${1}" | tr -d "*")

phpbrew switch $BUILD_CURRENT_PHP

set -ex
cp test/Unit/MachinePackTest.env.yml test/Unit/MachinePackTest.yml 
mkdir -p ./test-reports || true
composer install
vendor/bin/phpcs --report-junit=./test-reports/phpcs_report_${1}.xml
vendor/bin/phpunit --log-junit=./test-reports/phpunit_report_${1}.xml
